.SUFFIXES:
.PHONY: default all clean mrproper indent zip

COMPILER		:= python3

MAIN			:= card.py
DEMO			:= demo.py

PY_FILES		:= $(wildcard *.py)
CACHE_FILES		:= $(wildcard __pycache__/ */__pycache__/ *.pyc)

###########################################################

default: demo

main: $(MAIN)
	@echo "Lauching $<..."
	@$(COMPILER) $<

demo: $(DEMO)
	@echo "Lauching $<..."
	@$(COMPILER) $<	

clean:
	@echo "Cleaning cache files..."
	@-rm -rf $(CACHE_FILES)
	@echo "Cleaning done! :)"