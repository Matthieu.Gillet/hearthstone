from numpy.random import randint

import structures

from structures import expansions
from structures.expansions import Expansion, CURRENT_CORE_YEAR
from structures.rarities import group, Rarity
from structures.classes import Class
from structures.runes import Rune
from structures.tribes import Tribe
from structures.types import Type

from tools import text_tools
from tools.text_tools import Color
from tools import normalization
from tools import description

from card import card, database, card_list, collection, pack, deck
from display import window, display_card, pack_opening, display_collection, menu

DEFAULT = False	

def transition(s: str):
	n = len(s)
	line = '\n' + '=' * (n + 6) + '\n'
	print("{}== {} =={}".format(line, text_tools.colorize_text(s, Color.BLUE, True), line))

def draw_line():
	print(text_tools.colorize_text("="*10, Color.BLACK, True))

##Constants
if DEFAULT:
	transition("Constants")
	print(Expansion.EXPERT1)
	print(Expansion.EXPERT1.value, Expansion.BASIC.value, Expansion.DUSKLAND.value)
	print(Expansion.EXPERT1 == Expansion.BASIC)
	print(expansions.SYMBOL_DICT[Expansion.RASTAKHAN])
	print(Expansion(3))

##TextTools
if DEFAULT:
	transition("TextTools")
	print(text_tools.colorize_text('Hello', Color.RED, True)
		+ " I'm "
		+ text_tools.colorize_text('FRIENDLY', Color.YELLOW, True))
	print(text_tools.colorize_text('Hi', Color.RED, True)
		+ " "
		+ text_tools.colorize_text('FRIENDLY', Color.YELLOW, True)
		+ " I'm "
		+ text_tools.colorize_text("DAD", Color.BLUE, True))

##Normalization
if DEFAULT:
	transition("Normalization")
	print(normalization.normalize_apostrophe("Zul'jin"))
	print(normalization.normalize_expansion("LBM"))
	try:
		print(normalization.normalize_expansion("glouborg"))
	except KeyError:
		print("I can't find my glouborg! :(")
	print(normalization.normalize_description(
		"$Charge$. $Battlecry:$ Summon two 1/1 Whelps for your opponent."))
	print(normalization.title_to_filename("Zul’jin"))
	print(normalization.title_to_filename("Power Word: Shield"))

##Card
if DEFAULT:
	transition("Card")
	a = card.Card(Expansion.GOBLINS_VS_GNOMES, "Dr Boom", "Dr Boom", 7, Class.ANY, Rarity.LEGENDARY,
		"$Battlecry:$ Summon two 1/1 Boom Bots.", False, True, [])
	print(a)
	n = a.get_name()
	print(n)

	draw_line()
	print("Does copy work well?")
	
	b = a.copy()
	print(b)
	a.add_related_card(b)
	a.add_related_card(b)
	print("A related cards: {}".format(a.get_related_cards()))
	print("B related cards: {}".format(b.get_related_cards()))
	print(a.__dict__)

	draw_line()
	print("Testing Minions")

	c = card.Minion(Expansion.GOBLINS_VS_GNOMES, "Dr Boom", "Dr Boom", 7, 7, 7, Tribe.NONE,
		Class.ANY, Rarity.LEGENDARY, "$Battlecry:$ Summon two 1/1 Boom Bots.", False, True, [])
	print(c)
	d = card.Minion(Expansion.HALL_OF_FAME, "Azure Drake", "Azure Drake", 5, 4, 4, Tribe.DRAGON,
		Class.ANY, Rarity.RARE, "$Spell Damage +1$. $Battlecry:$ Draw a card.", True, True, [])
	print(d)
	print("card == minion? {}, and also {}".format(a == c, c == a))
	print(d.copy())

	draw_line()
	print("Testing other card types.")
	print(card.Spell(Expansion.BASIC, "Fireball", "Fireball", 4, Class.MAGE, Rarity.BASIC,
		"Deal 6 damage.", False, True, []))
	print(card.Weapon(Expansion.RASTAKHAN, "Sul'thraze", "Sul'thraze", 6, 4, 4, Class.WARRIOR,
		Rarity.EPIC, "$Overkill:$ You may attack again.", False, True, []))
	print(card.HeroPower(Expansion.BASIC, "Life Tap", "Life Tap", 2, Class.WARLOCK, Rarity.BASIC,
		"$Hero Power$. Take 2 damage and draw a card.", False, True, []))
	print(card.Hero(Expansion.FROZEN_THRONE, "Deathstalker Rexxar", "Deathstalker Rexxar", 6, 5,
		Class.HUNTER, Rarity.LEGENDARY, "$Battlecry: Deal 2 damage to all enemy minions.",
		False, True, []))

##Database (1)
if DEFAULT:
	transition("Database (1)")
	a = 'Mot de pouvoir : Réplication;BOOM;Mot de pouvoir : Réplication;5;;;Sort;;Prêtre;Épique;Vous choisissez un serviteur allié. En invoque une copie 5/5.;;9;5;Prêtre;VRAI'
	print(a)
	ca = database.line_to_card(a)
	print(ca)
	print(ca.__dict__)

	draw_line()
	b = 'Buveuse de vie;LBM;Buveuse de vie;4;3;3;Serviteur;Bête;Neutre;Rare;$Cri de guerre :$ inflige 3 points de dégâts au héros adverse. Rend 3 PV à votre héros.;;26;4;Neutre;VRAI'
	print(b)
	cb = database.line_to_card(b)
	print(cb)
	print(cb.__dict__)

	draw_line()
	c = 'Deuillegivre;CTG;Deuillegivre;7;5;3;Arme;;Chevalier de la mort;Basique-D;$Râle d’agonie :$ invoque chaque serviteur tué par cette arme.;"Arfus;Le roi-liche";8;7;Chevalier de la mort;FAUX'
	print(c)
	cc = database.line_to_card(c)
	print(cc)
	print(cc.__dict__)

	draw_line()
	d = 'Uther de la Lame d’ébène;CTG;Uther de la Lame d’ébène;9;;5;Héros;;Paladin;Légendaire;$Cri de guerre :$ vous équipe d’une arme 5/3 avec $Vol de vie$.;;16;9;Paladin;VRAI'
	print(d)
	cd = database.line_to_card(d)
	print(cd)
	print(cd.__dict__)

	draw_line()
	e = 'Construct-o-bêtes;CTG;Construct-o-bêtes;2;;;Pouvoir héroïque;;Chasseur;Basique;$Pouvoir héroïque$. Crée une bête zombifiée.;Rexxar le nécrotraqueur;2;2;Chasseur;FAUX'
	print(e)
	ce = database.line_to_card(e)
	print(ce)
	print(ce.__dict__)

##Database (2)
if DEFAULT:
	transition("Database (2)")
	database = database.get_database()
	draw_line()
	for i in [450, 2149, 2442, 911, 1492]:
		print(database[i])
		print(database[i].__dict__)
		draw_line()

print("Building Card List...")
card_list.init_card_list()
##Card List
if DEFAULT:
	transition("Card List")
	print(card_list.CARD_LIST[-10:])

	draw_line()
	print(card_list.COMPLETE_CARD_LIST[-10:])

	draw_line()
	if DEFAULT:
		print(card_list.CARD_LIST_EXPANSION_RARITY) #Works fine, but takes some place. ^^
	print("Duplicate keys:", card_list.get_duplicate_keys())

	draw_line()
	print("Testing the search functions")
	a = card_list.search_with_key("Ver tunnelier")
	print(a)
	print(card_list.search_with_key("Feu follet (DTA)"))
	print(card_list.search_with_key("Feu follet (DTA)").__dict__)
	print(card_list.search_with_key("L'Épouvantueur"))
	try:
		print(card_list.search_with_key("glouborg"))
	except ValueError:
		print("Still can't find my glouborg :(")
	
	draw_line()
	print("Searching the Corridor Creeper...")
	i = card_list.search_indix(a, card_list.COMPLETE_CARD_LIST)
	print("Indix found:", i)
	print("list[i]:", card_list.COMPLETE_CARD_LIST[i])
	print("Yay! Found it! :D (it was in the corridor)")


print("Linking Tokens and Golden Versions...")
card_list.token_linking()

print("Building Collection...")
collection.init_collection()
##Collection
if DEFAULT:
	transition("Collection")
	print("Testing on a small collection first.")
	a = card_list.search_with_key("Ver tunnelier")
	golden_a = a.golden_copy()
	test_collection = collection.Collection(0, 0, {a: (1, False), golden_a: (1, True)})
	print(test_collection.__dict__)
	print(test_collection)

	draw_line()
	print("Global collection.")
	print(collection.COLLECTION)
	if DEFAULT:
		print(collection.COLLECTION.__dict__) #Works fine, but takes some place. ^^

	draw_line()
	collection.add_card_to_collection(a)
	print(collection.COLLECTION)

	draw_line()
	collection.add_card_list_to_collection([a, a, a, golden_a, a])
	print(collection.COLLECTION)

##Pack
if DEFAULT:
	transition("Pack")
	#print(collection.COLLECTION._collection)
	#draw_line()
	for i in range(5):
		print("Any card:      ", pack.generate_pack_card(Expansion.ALL))
		print("Rastakhan card:", pack.generate_pack_card(Expansion.RASTAKHAN))

	exps = [Expansion.CLASSIC, Expansion.RASTAKHAN, Expansion.TAVERNS_OF_TIME,
		Expansion.BLACKROCK_MOUNTAIN, Expansion.ALL]
	for i in range(5):
		draw_line()
		print(exps[i])
		p = pack.generate_pack(exps[i])
		for j in range(5):
			print(p[j])

##Deck
if False:
	transition("Deck")
	deck.new_deck("Druide", Class.DRUID)
	#deck.new_deck("Prêtre", Class.PRIEST)
	deck.new_deck("Chasseur", Class.HUNTER)
	deck.new_deck("Mage", Class.MAGE)
	d = deck.new_deck("Paladin classique", Class.PALADIN)
	deck.new_deck("Voleur", Class.ROGUE)
	print(d)

	e1 = card_list.search_with_key("Tirion Fordring").golden_copy()
	e2 = card_list.search_with_key("Élémentaire d'air")
	e3 = card_list.search_with_key("Quatre murlocs et un enterrement")
	e4 = card_list.search_with_key("Quatre murlocs et un enterrement")
	e5 = card_list.search_with_key("Élémentaire d'air").golden_copy()
	p = [e1, e2, e3, e4, e5]
	draw_line()
	d2 = deck.card_list_to_deck(p, "Le test avec des cartes", Class.SHAMAN)
	print(d2)
	draw_line()
	print("Remove")
	d2.remove_card(e4)
	print(d2)
	draw_line()
	print("Remove again")
	d2.remove_card(e4)
	print(d2)
	d2.add_card(e3)
	d2.add_card(card_list.search_with_key("Âme des Murlocs"))
	d2.add_card(card_list.search_with_key("Géant de lave"))
	d2.add_card(card_list.search_with_key("Géant des montagnes"))
	d2.add_card(card_list.search_with_key("Géant de lave"))
	d2.add_card(card_list.search_with_key("Shirvallah, le tigre"))
	d2.add_card(card_list.search_with_key("Parchemin changeant"))
	draw_line()
	print("Deck List")
	print(deck.DECK_LIST)
	draw_line()
	print("Now with lots of cards")
	deck.new_deck("Guerrier", Class.WARRIOR)
	d3 = deck.new_deck("Reno Brouillecaboche", Class.WARLOCK)
	l=["Protecteur d'Argent", "Protecteur d'Argent", "Garde-paix de l'Aldor", "Garde-paix de l'Aldor", "Bénédiction des rois", "Bénédiction des rois", "Championne en vrai-argent", "Championne en vrai-argent", "Consécration", "Consécration", "Égalité", "Égalité", "Gardien des rois", "Imposition des mains", "Tirion Fordring", "Crocilisque des rivières", "Crocilisque des rivières", "Limon des marais acide", "Limon des marais acide", "Clerc du Soleil brisé", "Clerc du Soleil brisé", "Golem des moissons", "Golem des moissons", "Maître-bouclier de Sen'jin", "Maître-bouclier de Sen'jin", "Champion de la Main d'argent", "Champion de la Main d'argent", "Ogre rochepoing", "Ogre rochepoing", "Champion de Hurlevent"]
	for i in l:
		if randint(20) <= 3:
			d.add_card(card_list.search_with_key(i).golden_copy())
		else:
			d.add_card(card_list.search_with_key(i))
	l=["Pacte sacrificiel", "Puissance accablante", "Voile de mort", "Chef du gang des diablotins", "Conseiller de Sombre-Comté", "Courroux démoniaque", "Détection des démons", "Trait de l'ombre", "Destrier de l'effroi", "Destrier de l'effroi", "Flammes infernales", "Ombreflamme", "Potion de cristal explosif", "Écraseur du Vide", "Siphonner l'âme", "Néant distordu", "Sir Finley Mrrgglton", "Auspice funeste", "Limon des marais acide", "Prophète du Cercle terrestre", "Vendeur de rafraîchissements", "Videur de l'arrière-salle", "Champion de Mukla", "Cogneur médiocre", "Reno Jackson", "Sylvanas Coursevent", "Champion de Hurlevent", "Maire Brouillecaboche", "Nozdormu", "N'Zoth le corrupteur"]
	for i in l:
		if randint(20) <= 3:
			d3.add_card(card_list.search_with_key(i).golden_copy())
		else:
			d3.add_card(card_list.search_with_key(i))
	print(deck.DECK_LIST)

##Description
if DEFAULT:
	transition("Description")
	example1 = card_list.search_with_key("Zilliax")
	example2 = card_list.search_with_key("Tempête de foudre")
	for a in [example1, example2]:
		draw_line()
		print(a)
		d = description.description(a.get_description())
		print(d)
	for i in range(3):
		draw_line()
		c = card_list.COMPLETE_CARD_LIST[randint(len(card_list.COMPLETE_CARD_LIST))]
		print(c)
		print(description.description(c.get_description()))

##Display
if DEFAULT:
	transition("Card Display")
	e1 = card_list.search_with_key("Tirion Fordring")
	e2 = card_list.search_with_key("Élémentaire d'air").golden_copy()
	e3 = card_list.search_with_key("Quatre murlocs et un enterrement")
	e4 = card_list.search_with_key("Hurlesang")
	e5 = card_list.search_with_key("Anduin le faucheur noir")

	for c in [e1, e2, e3, e4, e5]:
		draw_line()
		w = window.init_window(207, 277)
		print("Displaying {}...".format(c.get_name()))
		display_card.add_card_to_window(w, c, 4, 4, 0)
		w.display()

if DEFAULT:
	transition("Pack Display")
	if DEFAULT:
		w = window.init_window(1362, 701)
		e1 = card_list.search_with_key("Tirion Fordring")
		e2 = card_list.search_with_key("Élémentaire d'air").golden_copy()
		e3 = card_list.search_with_key("Quatre murlocs et un enterrement")
		e4 = card_list.search_with_key("Hurlesang")
		e5 = card_list.search_with_key("Anduin le faucheur noir")
		p = [e1, e2, e3, e4, e5]
		pack_opening.display_pack(w, p, Expansion.ALL, True)
		print("Displaying predefined pack...")
		w.display()
	w = window.init_window(1362, 701)
	pack_opening.display_opening(w, Expansion.ALL, True)
	print("Displaying random pack...")
	w.display()

if DEFAULT:
	transition("Collection Display")
	w = window.init_window(1362, 701)
	display_collection.display_collection(w)
	w.display()



def givecard(s: str):
	collection.COLLECTION.add_card(card_list.search_with_key(s))


if DEFAULT:
	transition("Menu")
	# c = card_list.search_with_key("Tirion Fordring")
	# collection.COLLECTION.add_card(c)
	# collection.COLLECTION.add_card(c)
	# collection.COLLECTION.add_card(c)
	w = window.init_window(1362, 701)
	menu.menu(w)
	w.display()

if False:
	transition("Expansion Art Test")

	expansion = Expansion.MARCH_OF_THE_LICH_KING
	n = expansions.to_int(expansion)
	l = []
	# for i in range(n,n+1):
	# 	l += card_list.COMPLETE_CARD_LIST_EXPANSION[i]
	l = card_list.COMPLETE_CARD_LIST_EXPANSION[n]
	for c in l:
		if not c.is_collectable():
			c = c.golden_copy()
		collection.COLLECTION.add_card(c)
		collection.COLLECTION.add_card(c)
		collection.COLLECTION.set_new(c, False)
	w = window.init_window(1362, 701)
	menu.menu(w)
	w.display()

#Checking if every card has a different name
if False:
	print("Duplicate keys:")
	for i in card_list.get_duplicate_keys():
		print(i[0], i[2])
		print(i[1], i[3])
		print("")

if False:
	expansion_list = [expansions.to_int(i) for i in [Expansion.CLASSIC, Expansion.WITCHWOOD, Expansion.BOOMSDAY, Expansion.RASTAKHAN, Expansion.RISE_OF_SHADOWS]]
	l = []
	for i in expansion_list:
		l += card_list.COMPLETE_CARD_LIST_EXPANSION[i]
	for c in l:
		if c.get_rarity() is Rarity.LEGENDARY and c.get_type() is Type.MINION and c.is_collectable():
			collection.COLLECTION.add_card(c)
	w = window.init_window(1362, 701)
	menu.menu(w)
	w.display()

if True:
	transition("IS YOUR COLLECTION ALL GOLDEN?")

	for c in card_list.COMPLETE_CARD_LIST:
		
		# if False:
		# if c.get_related_cards() != [] and (c.get_tavern() is not None or c.get_expansion() is Expansion.BATTLEGROUNDS) and c.get_expansion() is not CURRENT_CORE_YEAR:
		if c.is_collectable() and c.get_expansion() in [CURRENT_CORE_YEAR]:
		# if c.get_expansion() in [Expansion.TITANS]:
		# if c.get_expansion() in [Expansion.MARCH_OF_THE_LICH_KING] or c.get_class() == [Class.DEATH_KNIGHT]:
		# if c.get_related_cards() != []:
		# if c.get_type() is Type.MINION and len(c.get_tribes()) >= 2:
		# if c.get_type() is Type.MINION and (Tribe.GOBLIN in c.get_tribes()):
		# if len(c.get_description()) >= 110:
		# if '. $' in c.get_description() and not "$. $" in c.get_description():# or "-" in c.get_description():
		# if c.is_collectable() and not c.get_expansion() in [Expansion.DUSKLAND, Expansion.ARENA, Expansion.TAVERNS_OF_TIME]:

			collection.add_card_to_collection(c)

			if not group(c.get_rarity()) is Rarity.LEGENDARY:
				collection.add_card_to_collection(c)
			# collection.add_card_to_collection(c.golden_copy())

			collection.COLLECTION.set_new(c, False)
			collection.COLLECTION.set_new(c.golden_copy(), False)
	w = window.init_window(1362, 701)
	menu.menu(w)
	w.display()