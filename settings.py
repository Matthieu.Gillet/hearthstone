from structures.rarities import Rarity

TAVERN_MODE = False
MORE_SUBTYPES = False
AUTO_LINEBREAK = False
REMOVE_LINEBREAK = False

def get_tavern_mode() -> bool:
	return TAVERN_MODE
def set_tavern_mode(b: bool):
	TAVERN_MODE = b

#Drop rate of each rarity
STAT_COMMON = 888
STAT_RARE = STAT_COMMON + 57
STAT_EPIC = STAT_RARE + 44
STAT_LEGENDARY = STAT_EPIC + 11

#Percentage rate of the golden version of each rarity.
STAT_GOLDEN_COMMON = 980
STAT_GOLDEN_RARE = 935
STAT_GOLDEN_EPIC = 948
STAT_GOLDEN_LEGENDARY = 909

STAT_GOLDEN_DICT = {
	Rarity.BASIC:		STAT_GOLDEN_COMMON, 
	Rarity.BASIC_D:		STAT_GOLDEN_LEGENDARY, 
	Rarity.COMMON:		STAT_GOLDEN_COMMON, 
	Rarity.RARE:		STAT_GOLDEN_RARE, 
	Rarity.EPIC:		STAT_GOLDEN_EPIC, 
	Rarity.LEGENDARY:	STAT_GOLDEN_LEGENDARY
}

CRAFTING_VALUES = {
	(Rarity.BASIC, False):		40, 
	(Rarity.BASIC, True):		400, 
	(Rarity.BASIC_D, False):	1600, 
	(Rarity.BASIC_D, True):		3200, 
	(Rarity.COMMON, False):		40, 
	(Rarity.COMMON, True):		400, 
	(Rarity.RARE, False):		100, 
	(Rarity.RARE, True):		800, 
	(Rarity.EPIC, False):		400, 
	(Rarity.EPIC, True):		1600, 
	(Rarity.LEGENDARY, False):	1600, 
	(Rarity.LEGENDARY, True):	3200
}

DISENCHANT_VALUES = {
	(Rarity.BASIC, False):		5, 
	(Rarity.BASIC, True):		50, 
	(Rarity.BASIC_D, False):	400, 
	(Rarity.BASIC_D, True):		1600, 
	(Rarity.COMMON, False):		5, 
	(Rarity.COMMON, True):		50, 
	(Rarity.RARE, False):		20, 
	(Rarity.RARE, True):		100, 
	(Rarity.EPIC, False):		100, 
	(Rarity.EPIC, True):		400, 
	(Rarity.LEGENDARY, False):	400, 
	(Rarity.LEGENDARY, True):	1600
}

def get_crafting_value(r: Rarity, is_golden: bool) -> int:
	return CRAFTING_VALUES[r, is_golden]

def get_disenchant_value(r: Rarity, is_golden: bool) -> int:
	return DISENCHANT_VALUES[r, is_golden]