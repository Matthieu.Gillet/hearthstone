from enum import Enum, auto

class Zone(Enum):
	BATTLEFIELD		= auto()
	HAND			= auto()
	DECK			= auto()
	GRAVEYARD		= auto()