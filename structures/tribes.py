from enum import Enum, auto

class Tribe(Enum):
	NONE		= auto()
	BEAST		= auto()
	DEMON		= auto()
	DRAENEI		= auto()
	DRAGON		= auto()
	ELEMENTAL	= auto()
	MECH		= auto()
	MURLOC		= auto()
	NAGA		= auto()
	PIRATE		= auto()
	QUILBOAR	= auto()
	TOTEM		= auto()
	UNDEAD		= auto()
	ALL			= auto()

	ARCANE		= auto()
	FEL			= auto()
	FIRE		= auto()
	FROST		= auto()
	NATURE		= auto()
	HOLY		= auto()
	SHADOW		= auto()

	SPELLCRAFT	= auto()
	TAVERN		= auto()
	UPGRADE		= auto()

	LESSER		= auto()
	GREATER		= auto()

	ANKOAN		= auto()
	ARAKKOA		= auto()
	BLOOD_ELF	= auto()
	BROKER		= auto()
	CELESTIAL	= auto()
	CENTAUR		= auto()
	DEVOURER	= auto()
	DREDGER		= auto()
	DWARF		= auto()
	EGG			= auto()
	ETERNAL		= auto()
	ETHEREAL	= auto()
	FACELESS	= auto()
	FLAMEWAKER	= auto()
	FURBOLG		= auto()
	GNOLL		= auto()
	GNOME		= auto()
	GOBLIN		= auto()
	GOLEM		= auto()
	GOREN		= auto()
	GRONN		= auto()
	HARPY		= auto()
	HIGH_ELF	= auto()
	HOZEN		= auto()
	HUMAN		= auto()
	JAILER		= auto()
	JINYU		= auto()
	KOBOLD		= auto()
	KYRIAN		= auto()
	LOBSTROK	= auto()
	MANTID		= auto()
	MAWSWORN	= auto()
	MOGU		= auto()
	MOONKIN		= auto()
	MUCKLING	= auto()
	NAARU		= auto()
	NERUBIAN	= auto()
	NIGHTBORNE	= auto()
	NIGHT_ELF	= auto()
	OBJECT		= auto()
	OGRE		= auto()
	OLD_GOD		= auto()
	OOZE		= auto()
	ORC			= auto()
	PANDAREN	= auto()
	PLANT		= auto()
	PROTOSS	 	= auto()
	PYGMY		= auto()
	QIRAJI		= auto()
	SAUROK		= auto()
	SETHRAK		= auto()
	SHA			= auto()
	SPORELING	= auto()
	STEWARD		= auto()
	STONEBORN	= auto()
	TAUREN		= auto()
	TITAN		= auto()
	TITAN_FORGED= auto()
	TOLVIR		= auto()
	TORTOLLAN	= auto()
	TROGG		= auto()
	TROLL		= auto()
	TUSKARR		= auto()
	VENTHYR		= auto()
	VIRMEN		= auto()
	VOID_ELF	= auto()
	VRYKUL		= auto()
	VULPERA		= auto()
	WORGEN		= auto()
	YETI		= auto()
	ZERG		= auto()

TRIBE_DICT = {
	"":						Tribe.NONE,
	"Aucune":				Tribe.NONE,
	"None":					Tribe.NONE,
	"Bête":					Tribe.BEAST,
	"Beast":				Tribe.BEAST,
	"Démon":				Tribe.DEMON,
	"Demon":				Tribe.DEMON,
	"Draeneï":				Tribe.DRAENEI,
	"Draenei":				Tribe.DRAENEI,
	"Dragon":				Tribe.DRAGON,
	"Élémentaire":			Tribe.ELEMENTAL,
	"Elemental":			Tribe.ELEMENTAL,
	"Méca":					Tribe.MECH,
	"Mech":					Tribe.MECH,
	"Murloc":				Tribe.MURLOC,
	"Naga":					Tribe.NAGA,
	"Pirate":				Tribe.PIRATE,
	"Huran":				Tribe.QUILBOAR,
	"Quilboar":				Tribe.QUILBOAR,
	"Totem":				Tribe.TOTEM,
	"Mort-vivant":			Tribe.UNDEAD,
	"Undead":				Tribe.UNDEAD,
	"Tout":					Tribe.ALL,
	"All":					Tribe.ALL,

	"Arcanes":				Tribe.ARCANE,
	"Arcane":				Tribe.ARCANE,
	"Fiel":					Tribe.FEL,
	"Fel":					Tribe.FEL,
	"Feu":					Tribe.FIRE,
	"Fire":					Tribe.FIRE,
	"Givre":				Tribe.FROST,
	"Frost":				Tribe.FROST,
	"Nature":				Tribe.NATURE,
	"Sacré":				Tribe.HOLY,
	"Holy":					Tribe.HOLY,
	"Ombre":				Tribe.SHADOW,
	"Shadow":				Tribe.SHADOW,

	"Sorcellerie":			Tribe.SPELLCRAFT,
	"Spellcraft":			Tribe.SPELLCRAFT,
	"Taverne":				Tribe.TAVERN,
	"Tavern":				Tribe.TAVERN,
	"Amélioration":			Tribe.UPGRADE,
	"Upgrade":				Tribe.UPGRADE,

	"Inférieur":			Tribe.LESSER,
	"Lesser":				Tribe.LESSER,
	"Supérieur":			Tribe.GREATER,
	"Greater":				Tribe.GREATER,

	"Ankoïen":				Tribe.ANKOAN,
	"Arakkoa":				Tribe.ARAKKOA,
	"Elfe de sang":			Tribe.BLOOD_ELF,
	"Briseur":				Tribe.BROKER,
	"Astre":				Tribe.CELESTIAL,
	"Centaure":				Tribe.CENTAUR,
	"Dévoreur":				Tribe.DEVOURER,
	"Purotin":				Tribe.DREDGER,
	"Nain":					Tribe.DWARF,
	"Œuf":					Tribe.EGG,
	"Éternel":				Tribe.ETERNAL,
	"Éthérien":				Tribe.ETHEREAL,
	"Sans-visage":			Tribe.FACELESS,
	"Attise-flammes":		Tribe.FLAMEWAKER,
	"Furbolg":				Tribe.FURBOLG,
	"Gnoll":				Tribe.GNOLL,
	"Gnome":				Tribe.GNOME,
	"Gobelin":				Tribe.GOBLIN,
	"Golem":				Tribe.GOLEM,
	"Goren":				Tribe.GOREN,
	"Gronn":				Tribe.GRONN,
	"Harpie":				Tribe.HARPY,
	"Haut-elfe":			Tribe.HIGH_ELF,
	"Hozen":				Tribe.HOZEN,
	"Humain":				Tribe.HUMAN,
	"Geôlier":				Tribe.JAILER,
	"Jinyu":				Tribe.JINYU,
	"Kobold":				Tribe.KOBOLD,
	"Kyrian":				Tribe.KYRIAN,
	"Homstrok":				Tribe.LOBSTROK,
	"Mantide":				Tribe.MANTID,
	"Antrelige":			Tribe.MAWSWORN,
	"Mogu":					Tribe.MOGU,
	"Sélénien":				Tribe.MOONKIN,
	"Boueux":				Tribe.MUCKLING,
	"Naaru":				Tribe.NAARU,
	"Nérubien":				Tribe.NERUBIAN,
	"Sacrenuit":			Tribe.NIGHTBORNE,
	"Elfe de la nuit":		Tribe.NIGHT_ELF,
	"Objet":				Tribe.OBJECT,
	"Ogre":					Tribe.OGRE,
	"Dieu très ancien":		Tribe.OLD_GOD,
	"Limon":				Tribe.OOZE,
	"Orc":					Tribe.ORC,
	"Pandaren":				Tribe.PANDAREN,
	"Plante":				Tribe.PLANT,
	"Protoss":				Tribe.PROTOSS,
	"Pygmée":				Tribe.PYGMY,
	"Qiraji":				Tribe.QIRAJI,
	"Saurok":				Tribe.SAUROK,
	"Sethrak":				Tribe.SETHRAK,
	"Sha":					Tribe.SHA,
	"Sporelin":				Tribe.SPORELING,
	"Régisseur":			Tribe.STEWARD,
	"Vive-pierre":			Tribe.STONEBORN,
	"Tauren":				Tribe.TAUREN,
	"Titan":				Tribe.TITAN,
	"Forgé par les Titans":	Tribe.TITAN_FORGED,
	"Tol’vir":				Tribe.TOLVIR,
	"Tortollan":			Tribe.TORTOLLAN,
	"Trogg":				Tribe.TROGG,
	"Troll":				Tribe.TROLL,
	"Rohart":				Tribe.TUSKARR,
	"Venthyr":				Tribe.VENTHYR,
	"Virmen":				Tribe.VIRMEN,
	"Elfe du Vide":			Tribe.VOID_ELF,
	"Vrykul":				Tribe.VRYKUL,
	"Vulpérin":				Tribe.VULPERA,
	"Worgen":				Tribe.WORGEN,
	"Yéti":					Tribe.YETI,
	"Zerg":					Tribe.ZERG
}

TRIBE_REPR = {
	Tribe.NONE:			"",
	Tribe.BEAST:		"Bête",
	Tribe.DEMON:		"Démon",
	Tribe.DRAENEI:		"Draeneï",
	Tribe.DRAGON:		"Dragon",
	Tribe.ELEMENTAL:	"Élémentaire",
	Tribe.MECH:			"Méca",
	Tribe.MURLOC:		"Murloc",
	Tribe.NAGA:			"Naga",
	Tribe.PIRATE:		"Pirate",
	Tribe.QUILBOAR:		"Huran",
	Tribe.TOTEM:		"Totem",
	Tribe.UNDEAD:		"Mort-vivant",
	Tribe.ALL:			"Tout",

	Tribe.ARCANE:		"Arcanes",
	Tribe.FEL:			"Fiel",
	Tribe.FIRE:			"Feu",
	Tribe.FROST:		"Givre",
	Tribe.NATURE:		"Nature",
	Tribe.HOLY:			"Sacré",
	Tribe.SHADOW:		"Ombre",

	Tribe.SPELLCRAFT:	"Sorcellerie",
	Tribe.TAVERN:		"Taverne",
	Tribe.UPGRADE:		"Amélioration",

	Tribe.LESSER:		"Inférieur",
	Tribe.GREATER:		"Supérieur",

	Tribe.ANKOAN:		"Ankoïen",
	Tribe.ARAKKOA:		"Arakkoa",
	Tribe.BLOOD_ELF:	"Elfe de sang",
	Tribe.BROKER:		"Briseur",
	Tribe.CELESTIAL:	"Astre",
	Tribe.CENTAUR:		"Centaure",
	Tribe.DEVOURER:		"Dévoreur",
	Tribe.DREDGER:		"Purotin",
	Tribe.DWARF:		"Nain",
	Tribe.FURBOLG:		"Furbolg",
	Tribe.EGG:			"Œuf",
	Tribe.ETERNAL:		"Éternel",
	Tribe.ETHEREAL:		"Éthérien",
	Tribe.FACELESS:		"Sans-visage",
	Tribe.FLAMEWAKER:	"Attise-flammes",
	Tribe.FURBOLG:		"Furbolg",
	Tribe.GNOLL:		"Gnoll",
	Tribe.GNOME:		"Gnome",
	Tribe.GOBLIN:		"Gobelin",
	Tribe.GOLEM:		"Golem",
	Tribe.GOREN:		"Goren",
	Tribe.GRONN:		"Gronn",
	Tribe.HARPY:		"Harpie",
	Tribe.HIGH_ELF:		"Haut-elfe",
	Tribe.HOZEN:		"Hozen",
	Tribe.HUMAN:		"Humain",
	Tribe.JAILER:		"Geôlier",
	Tribe.JINYU:		"Jinyu",
	Tribe.KOBOLD:		"Kobold",
	Tribe.KYRIAN:		"Kyrian",
	Tribe.LOBSTROK:		"Homstrok",
	Tribe.MANTID:		"Mantide",
	Tribe.MAWSWORN:		"Antrelige",
	Tribe.MOGU:			"Mogu",
	Tribe.MOONKIN:		"Sélénien",
	Tribe.MUCKLING:		"Boueux",
	Tribe.NAARU:		"Naaru",
	Tribe.NERUBIAN:		"Nérubien",
	Tribe.NIGHTBORNE:	"Sacrenuit",
	Tribe.NIGHT_ELF:	"Elfe de la nuit",
	Tribe.OBJECT:		"Objet",
	Tribe.OGRE:			"Ogre",
	Tribe.OLD_GOD:		"Dieu très ancien",
	Tribe.OOZE:			"Limon",
	Tribe.ORC:			"Orc",
	Tribe.PANDAREN:		"Pandaren",
	Tribe.PLANT:		"Plante",
	Tribe.PROTOSS:		"Protoss",
	Tribe.PYGMY:		"Pygmée",
	Tribe.QIRAJI:		"Qiraji",
	Tribe.SAUROK:		"Saurok",
	Tribe.SETHRAK:		"Sethrak",
	Tribe.SHA:			"Sha",
	Tribe.SPORELING:	"Sporelin",
	Tribe.STEWARD:		"Régisseur",
	Tribe.STONEBORN:	"Vive-pierre",
	Tribe.TAUREN:		"Tauren",
	Tribe.TITAN:		"Titan",
	Tribe.TITAN_FORGED:	"Forgé par les Titans",
	Tribe.TOLVIR:		"Tol’vir",
	Tribe.TORTOLLAN:	"Tortollan",
	Tribe.TROGG:		"Trogg",
	Tribe.TROLL:		"Troll",
	Tribe.TUSKARR:		"Rohart",
	Tribe.VENTHYR:		"Venthyr",
	Tribe.VIRMEN:		"Virmen",
	Tribe.VOID_ELF:		"Elfe du Vide",
	Tribe.VRYKUL:		"Vrykul",
	Tribe.VULPERA:		"Vulpérin",
	Tribe.WORGEN:		"Worgen",
	Tribe.YETI:			"Yéti",
	Tribe.ZERG:			"Zerg"
}
def to_string(tribe: Tribe) -> str:
	return TRIBE_REPR[tribe]