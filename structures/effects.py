from enum import Enum, auto

class Effect(Enum):
	NONE		= auto()
	DEAL_DAMAGE	= auto()