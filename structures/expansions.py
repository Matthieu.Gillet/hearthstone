from enum import Enum, auto

class Expansion(Enum):
	ALL						= auto()
	GRYPHON					= auto()
	HYDRA					= auto()
	WOLF					= auto()
	PEGASUS					= auto()
	BASIC					= auto()
	LEGACY					= auto()
	DEMON_HUNTER_INITIATE	= auto()
	PATH_OF_ARTHAS			= auto()
	EXPERT1					= auto()
	ARENA					= auto()
	TAVERNS_OF_TIME			= auto()
	BATTLEGROUNDS			= auto()
	BATTLEGROUNDS_DUOS		= auto()
	DUELS					= auto()
	TWIST					= auto()
	NAXXRAMAS				= auto()
	GOBLINS_VS_GNOMES		= auto()
	BLACKROCK_MOUNTAIN		= auto()
	THE_GRAND_TOURNAMENT	= auto()
	LEAGUE_OF_EXPLORERS		= auto()
	OLD_GODS				= auto()
	KARAZHAN				= auto()
	GADGETZAN				= auto()
	UNGORO					= auto()
	FROZEN_THRONE			= auto()
	KOBOLDS_AND_CATACOMBS	= auto()
	WITCHWOOD				= auto()
	BOOMSDAY				= auto()
	RASTAKHAN				= auto()
	RISE_OF_SHADOWS			= auto()
	SAVIORS_OF_ULDUM		= auto()
	DESCENT_OF_DRAGONS		= auto()
	GALAKRONDS_AWAKENING	= auto()
	ASHES_OF_OUTLAND		= auto()
	SCHOLOMANCE_ACADEMY		= auto()
	DARKMOON_FAIRE			= auto()
	DARKMOON_RACES			= auto()
	FORGED_IN_THE_BARRENS	= auto()
	WAILING_CAVERNS			= auto()
	UNITED_IN_STORMWIND		= auto()
	DEADMINES				= auto()
	FRACTURED_IN_ALTERAC	= auto()
	ONYXIAS_LAIR			= auto()
	SUNKEN_CITY				= auto()
	THRONE_OF_THE_TIDES		= auto()
	MURDER_AT_CASTLE_NATHRIA= auto()
	MAW_AND_DISORDER		= auto()
	MARCH_OF_THE_LICH_KING	= auto()
	RETURN_TO_NAXXRAMAS		= auto()
	FESTIVAL_OF_LEGENDS		= auto()
	AUDIOPOCALYPSE			= auto()
	TITANS					= auto()
	FALL_OF_ULDUAR			= auto()
	CAVERNS_OF_TIME			= auto()
	SHOWDOWN_IN_THE_BADLANDS= auto()
	DELVE_INTO_DEEPHOLM		= auto()
	EVENT					= auto()
	WHIZBANG_WORKSHOP		= auto()
	DR_BOOMS_INVENTIONS		= auto()
	PERILS_IN_PARADISE		= auto()
	TRAVELING_TRAVEL_AGENCY	= auto()
	THE_GREAT_DARK_BEYOND	= auto()
	HEROES_OF_STARCRAFT		= auto()
	INTO_THE_EMERALD_DREAM	= auto()
	DUSKLAND				= auto()
	HEARTHCARDS				= auto()
	NEW						= auto()

MAX_EXPANSION = 39

EXPANSION_DICT = {
	"All":							Expansion.ALL,
	0:								Expansion.ALL,

	"Griffon":						Expansion.GRYPHON,
	"Gryphon":						Expansion.GRYPHON,
	"Hydre":						Expansion.HYDRA,
	"Hydra":						Expansion.HYDRA,
	"Loup":							Expansion.WOLF,
	"Wolf":							Expansion.WOLF,

	"Core":							Expansion.PEGASUS,
	"CORE":							Expansion.PEGASUS,
	"Pégase":						Expansion.PEGASUS,
	"Pegasus":						Expansion.PEGASUS,
	1:								Expansion.PEGASUS,

	"Basic":						Expansion.BASIC,
	"BAS":							Expansion.BASIC,
	2:								Expansion.BASIC,

	"Legacy":						Expansion.LEGACY,
	"LEG":							Expansion.LEGACY,

	"Demon Hunter Initiate":		Expansion.DEMON_HUNTER_INITIATE,
	"DHI":							Expansion.DEMON_HUNTER_INITIATE,

	"Path of Arthas":				Expansion.PATH_OF_ARTHAS,
	"PoA":							Expansion.PATH_OF_ARTHAS,

	"Expert1":						Expansion.EXPERT1,
	"EXP1":							Expansion.EXPERT1,
	3:								Expansion.EXPERT1,

	"Arena":						Expansion.ARENA,
	"ARN":							Expansion.ARENA,
	4:								Expansion.ARENA,

	"Taverns of Time":				Expansion.TAVERNS_OF_TIME,
	"ToT":							Expansion.TAVERNS_OF_TIME,
	
	"Battlegrounds":				Expansion.BATTLEGROUNDS,
	"BG":							Expansion.BATTLEGROUNDS,
	5:								Expansion.BATTLEGROUNDS,

	"Battlegrounds Duos":			Expansion.BATTLEGROUNDS_DUOS,
	"BGD":							Expansion.BATTLEGROUNDS_DUOS,

	"Duels":						Expansion.DUELS,

	"Twist":						Expansion.TWIST,

	"Naxxramas":					Expansion.NAXXRAMAS,
	"NAXX":							Expansion.NAXXRAMAS,
	6:								Expansion.NAXXRAMAS,

	"Gobelins vs Gnomes":			Expansion.GOBLINS_VS_GNOMES,
	"GvG":							Expansion.GOBLINS_VS_GNOMES,
	7:								Expansion.GOBLINS_VS_GNOMES,

	"Blackrock Mountain":			Expansion.BLACKROCK_MOUNTAIN,
	"BRM":							Expansion.BLACKROCK_MOUNTAIN,
	8:								Expansion.BLACKROCK_MOUNTAIN,

	"The Grand Tournament":			Expansion.THE_GRAND_TOURNAMENT,
	"TGT":							Expansion.THE_GRAND_TOURNAMENT,
	9:								Expansion.THE_GRAND_TOURNAMENT,

	"League of Explorers":			Expansion.LEAGUE_OF_EXPLORERS,
	"LoE":							Expansion.LEAGUE_OF_EXPLORERS,
	10:								Expansion.LEAGUE_OF_EXPLORERS,

	"Old Gods":						Expansion.OLD_GODS,
	"DTA":							Expansion.OLD_GODS,
	11:								Expansion.OLD_GODS,

	"Karazhan":						Expansion.KARAZHAN,
	"KARA":							Expansion.KARAZHAN,
	12:								Expansion.KARAZHAN,

	"Gadgetzan":					Expansion.GADGETZAN,
	"MSoG":							Expansion.GADGETZAN,
	13:								Expansion.GADGETZAN,

	"Un'Goro":						Expansion.UNGORO,
	"UNG":							Expansion.UNGORO,
	14: 							Expansion.UNGORO,

	"Knights of the Frozen Throne":	Expansion.FROZEN_THRONE,
	"CTG":							Expansion.FROZEN_THRONE,
	15:								Expansion.FROZEN_THRONE,

	"Kobolds & Catacombs":			Expansion.KOBOLDS_AND_CATACOMBS,
	"K&C":							Expansion.KOBOLDS_AND_CATACOMBS,
	16:								Expansion.KOBOLDS_AND_CATACOMBS,

	"Witchwood":					Expansion.WITCHWOOD,
	"LBM":							Expansion.WITCHWOOD,
	17:								Expansion.WITCHWOOD,

	"Boomsday Project":				Expansion.BOOMSDAY,
	"BOOM":							Expansion.BOOMSDAY,
	18:								Expansion.BOOMSDAY,

	"Rastakhan":					Expansion.RASTAKHAN,
	"JDR":							Expansion.RASTAKHAN,
	19:								Expansion.RASTAKHAN,

	"Rise of Shadows":				Expansion.RISE_OF_SHADOWS,
	"ÉdO":							Expansion.RISE_OF_SHADOWS,
	20:								Expansion.RISE_OF_SHADOWS,

	"Saviors of Uldum":				Expansion.SAVIORS_OF_ULDUM,
	"SoU":							Expansion.SAVIORS_OF_ULDUM,
	21:								Expansion.SAVIORS_OF_ULDUM,

	"Descent of Dragons":			Expansion.DESCENT_OF_DRAGONS,
	"DoD":							Expansion.DESCENT_OF_DRAGONS,
	22:								Expansion.DESCENT_OF_DRAGONS,

	"Galakrond's Awakening":		Expansion.GALAKRONDS_AWAKENING,
	"DoDA":							Expansion.GALAKRONDS_AWAKENING,

	"Ashes of Outland":				Expansion.ASHES_OF_OUTLAND,
	"AoO":							Expansion.ASHES_OF_OUTLAND,
	23:								Expansion.ASHES_OF_OUTLAND,

	"Scholomance Academy":			Expansion.SCHOLOMANCE_ACADEMY,
	"SMA":							Expansion.SCHOLOMANCE_ACADEMY,
	24:								Expansion.SCHOLOMANCE_ACADEMY,

	"Darkmoon Faire":				Expansion.DARKMOON_FAIRE,
	"FJS":							Expansion.DARKMOON_FAIRE,
	25:								Expansion.DARKMOON_FAIRE,

	"Darkmoon Races":				Expansion.DARKMOON_RACES,
	"FJSM":							Expansion.DARKMOON_RACES,

	"Forged in the Barrens":		Expansion.FORGED_IN_THE_BARRENS,
	"FitB":							Expansion.FORGED_IN_THE_BARRENS,
	26:								Expansion.FORGED_IN_THE_BARRENS,

	"Wailing Caverns":				Expansion.WAILING_CAVERNS,
	"FitBM":						Expansion.WAILING_CAVERNS,

	"United in Stormwind":			Expansion.UNITED_IN_STORMWIND,
	"UiS":							Expansion.UNITED_IN_STORMWIND,
	27:								Expansion.UNITED_IN_STORMWIND,

	"Deadmines":					Expansion.DEADMINES,
	"UiSM":							Expansion.DEADMINES,

	"Fractured in Alterac Valley":	Expansion.FRACTURED_IN_ALTERAC,
	"FiAV":							Expansion.FRACTURED_IN_ALTERAC,
	28:								Expansion.FRACTURED_IN_ALTERAC,

	"Onyxia's Lair":				Expansion.ONYXIAS_LAIR,
	"FiAVM":						Expansion.ONYXIAS_LAIR,

	"Voyage to the Sunken City":	Expansion.SUNKEN_CITY,
	"VSC":							Expansion.SUNKEN_CITY,
	29:								Expansion.SUNKEN_CITY,

	"Throne of the Tides":			Expansion.THRONE_OF_THE_TIDES,
	"VSCM":							Expansion.THRONE_OF_THE_TIDES,

	"Murder at Castle Nathria":		Expansion.MURDER_AT_CASTLE_NATHRIA,
	"MCN":							Expansion.MURDER_AT_CASTLE_NATHRIA,
	30:								Expansion.MURDER_AT_CASTLE_NATHRIA,

	"Maw and Disorder":				Expansion.MAW_AND_DISORDER,
	"MCNM":							Expansion.MAW_AND_DISORDER,

	"March of the Lich King":		Expansion.MARCH_OF_THE_LICH_KING,
	"MLK":							Expansion.MARCH_OF_THE_LICH_KING,
	31:								Expansion.MARCH_OF_THE_LICH_KING,

	"Return to Naxxramas":			Expansion.RETURN_TO_NAXXRAMAS,
	"MLKM":							Expansion.RETURN_TO_NAXXRAMAS,

	"Festival of Legends":			Expansion.FESTIVAL_OF_LEGENDS,
	"FoL":							Expansion.FESTIVAL_OF_LEGENDS,
	32:								Expansion.FESTIVAL_OF_LEGENDS,

	"Audiopocalypse":				Expansion.AUDIOPOCALYPSE,
	"FoLM":							Expansion.AUDIOPOCALYPSE,

	"TITANS":						Expansion.TITANS,
	"TTN":							Expansion.TITANS,
	33:								Expansion.TITANS,

	"Fall of Ulduar":				Expansion.FALL_OF_ULDUAR,
	"TTNM":							Expansion.FALL_OF_ULDUAR,

	"Caverns of Time":				Expansion.CAVERNS_OF_TIME,
	"CoT":							Expansion.CAVERNS_OF_TIME,
	34:								Expansion.CAVERNS_OF_TIME,

	"Showdown in the Badlands":		Expansion.SHOWDOWN_IN_THE_BADLANDS,
	"SitB":							Expansion.SHOWDOWN_IN_THE_BADLANDS,
	35:								Expansion.SHOWDOWN_IN_THE_BADLANDS,

	"Delve into Deepholm":			Expansion.DELVE_INTO_DEEPHOLM,
	"SitBM":						Expansion.DELVE_INTO_DEEPHOLM,

	"Event":						Expansion.EVENT,
	"EVENT":						Expansion.EVENT,

	"Whizbang's Workshop":			Expansion.WHIZBANG_WORKSHOP,
	"AdM":							Expansion.WHIZBANG_WORKSHOP,
	36:								Expansion.WHIZBANG_WORKSHOP,

	"Dr. Boom's Incredible Inventions":	Expansion.DR_BOOMS_INVENTIONS,
	"AdMM":							Expansion.DR_BOOMS_INVENTIONS,

	"Perils in Paradise":			Expansion.PERILS_IN_PARADISE,
	"PiP":							Expansion.PERILS_IN_PARADISE,
	37:								Expansion.PERILS_IN_PARADISE,

	"The Traveling Travel Agency":	Expansion.TRAVELING_TRAVEL_AGENCY,
	"PiPM":							Expansion.TRAVELING_TRAVEL_AGENCY,

	"The Great Dark Beyond":		Expansion.THE_GREAT_DARK_BEYOND,
	"GDB":							Expansion.THE_GREAT_DARK_BEYOND,
	38:								Expansion.THE_GREAT_DARK_BEYOND,

	"Heroes of Starcraft":			Expansion.HEROES_OF_STARCRAFT,
	"GDBM":							Expansion.HEROES_OF_STARCRAFT,

	"Into the Emerald Dream":		Expansion.INTO_THE_EMERALD_DREAM,
	"IED":							Expansion.INTO_THE_EMERALD_DREAM,
	39:								Expansion.INTO_THE_EMERALD_DREAM,

	"Dusland's Sorcellery":			Expansion.DUSKLAND,
	"SST":							Expansion.DUSKLAND,
	MAX_EXPANSION + 1:				Expansion.DUSKLAND,

	"Hearthcards":					Expansion.HEARTHCARDS,
	"HSC":							Expansion.HEARTHCARDS,
	MAX_EXPANSION + 2:				Expansion.HEARTHCARDS,

	"NEW":							Expansion.NEW
}

CURRENT_CORE_YEAR = EXPANSION_DICT["CORE"]

EXPANSION_TO_INT_DICT = {
	Expansion.ALL:						0,
	Expansion.GRYPHON:					0,
	Expansion.HYDRA:					0,
	Expansion.WOLF:						0,
	Expansion.PEGASUS:					1,
	Expansion.BASIC:					2,
	Expansion.LEGACY:					2,
	Expansion.DEMON_HUNTER_INITIATE:	2,
	Expansion.PATH_OF_ARTHAS:			2,
	Expansion.EXPERT1:					3,
	Expansion.ARENA:					4,
	Expansion.TAVERNS_OF_TIME:			4,
	Expansion.DUELS:					4,
	Expansion.TWIST:					4,
	Expansion.BATTLEGROUNDS:			5,
	Expansion.BATTLEGROUNDS_DUOS:		5,
	Expansion.NAXXRAMAS:				6,
	Expansion.GOBLINS_VS_GNOMES:		7,
	Expansion.BLACKROCK_MOUNTAIN:		8,
	Expansion.THE_GRAND_TOURNAMENT:		9,
	Expansion.LEAGUE_OF_EXPLORERS:		10,
	Expansion.OLD_GODS:					11,
	Expansion.KARAZHAN:					12,
	Expansion.GADGETZAN:				13,
	Expansion.UNGORO:					14,
	Expansion.FROZEN_THRONE:			15,
	Expansion.KOBOLDS_AND_CATACOMBS:	16,
	Expansion.WITCHWOOD:				17,
	Expansion.BOOMSDAY:					18,
	Expansion.RASTAKHAN:				19,
	Expansion.RISE_OF_SHADOWS:			20,
	Expansion.SAVIORS_OF_ULDUM:			21,
	Expansion.DESCENT_OF_DRAGONS:		22,
	Expansion.GALAKRONDS_AWAKENING:		22,
	Expansion.ASHES_OF_OUTLAND:			23,
	Expansion.SCHOLOMANCE_ACADEMY:		24,
	Expansion.DARKMOON_FAIRE:			25,
	Expansion.DARKMOON_RACES:			25,
	Expansion.FORGED_IN_THE_BARRENS:	26,
	Expansion.WAILING_CAVERNS:			26,
	Expansion.UNITED_IN_STORMWIND:		27,
	Expansion.DEADMINES:				27,
	Expansion.FRACTURED_IN_ALTERAC:		28,
	Expansion.ONYXIAS_LAIR:				28,
	Expansion.SUNKEN_CITY:				29,
	Expansion.THRONE_OF_THE_TIDES:		29,
	Expansion.MURDER_AT_CASTLE_NATHRIA:	30,
	Expansion.MAW_AND_DISORDER:			30,
	Expansion.MARCH_OF_THE_LICH_KING:	31,
	Expansion.RETURN_TO_NAXXRAMAS:		31,
	Expansion.FESTIVAL_OF_LEGENDS:		32,
	Expansion.AUDIOPOCALYPSE:			32,
	Expansion.TITANS:					33,
	Expansion.CAVERNS_OF_TIME:			34,
	Expansion.FALL_OF_ULDUAR:			33,
	Expansion.SHOWDOWN_IN_THE_BADLANDS:	35,
	Expansion.DELVE_INTO_DEEPHOLM:		35,
	Expansion.EVENT:					2,
	Expansion.WHIZBANG_WORKSHOP:		36,
	Expansion.DR_BOOMS_INVENTIONS:		36,
	Expansion.PERILS_IN_PARADISE:		37,
	Expansion.TRAVELING_TRAVEL_AGENCY:	37,
	Expansion.THE_GREAT_DARK_BEYOND:	38,
	Expansion.HEROES_OF_STARCRAFT:		38,
	Expansion.INTO_THE_EMERALD_DREAM:	39,
	Expansion.DUSKLAND:					MAX_EXPANSION + 1,
	Expansion.HEARTHCARDS:				MAX_EXPANSION + 2,
	Expansion.NEW:						0
}

NUMBER_EXPANSIONS = max([EXPANSION_TO_INT_DICT[e] for e in Expansion]) + 1

def to_int(expansion: Expansion) -> int:
	return EXPANSION_TO_INT_DICT[expansion]

EXPANSION_REPR = {
	Expansion.ALL:						"All",
	Expansion.GRYPHON:					"Gryphon",
	Expansion.HYDRA:					"Hydra",
	Expansion.WOLF:						"Wolf",
	Expansion.PEGASUS:					"Pegasus",
	Expansion.BASIC:					"Basic",
	Expansion.LEGACY:					"Legacy",
	Expansion.DEMON_HUNTER_INITIATE:	"Demon Hunter Initiate",
	Expansion.PATH_OF_ARTHAS:			"Path of Arthas",
	Expansion.EXPERT1:					"Expert1",
	Expansion.ARENA:					"Arena",
	Expansion.TAVERNS_OF_TIME:			"Taverns of Time",
	Expansion.BATTLEGROUNDS:			"Battlegrounds",
	Expansion.BATTLEGROUNDS_DUOS:		"Battlegrounds Duos",
	Expansion.DUELS:					"Duels",
	Expansion.TWIST:					"Twist",
	Expansion.NAXXRAMAS:				"Naxxramas",
	Expansion.GOBLINS_VS_GNOMES:		"Goblins vs Gnomes",
	Expansion.BLACKROCK_MOUNTAIN:		"Blackrock Mountain",
	Expansion.THE_GRAND_TOURNAMENT:		"The Grand Tournament",
	Expansion.LEAGUE_OF_EXPLORERS:		"League of Explorers",
	Expansion.OLD_GODS:					"Whispers of the Old Gods",
	Expansion.KARAZHAN:					"Karazhan",
	Expansion.GADGETZAN:				"Gadgetzan",
	Expansion.UNGORO:					"Un'Goro",
	Expansion.FROZEN_THRONE:			"Knights of the Frozen Throne",
	Expansion.KOBOLDS_AND_CATACOMBS:	"Kobolds & Catacombs",
	Expansion.WITCHWOOD:				"Witchwood",
	Expansion.BOOMSDAY:					"Boomsday Project",
	Expansion.RASTAKHAN:				"Rastakhan",
	Expansion.RISE_OF_SHADOWS:			"Rise of Shadows",
	Expansion.SAVIORS_OF_ULDUM:			"Saviors of Uldum",
	Expansion.DESCENT_OF_DRAGONS:		"Descent of Dragons",
	Expansion.GALAKRONDS_AWAKENING:		"Galakrond's Awakening",
	Expansion.ASHES_OF_OUTLAND:			"Ashes of Outland",
	Expansion.SCHOLOMANCE_ACADEMY:		"Scholomance Academy",
	Expansion.DARKMOON_FAIRE:			"Darkmoon Faire",
	Expansion.DARKMOON_RACES:			"Darkmoon Races",
	Expansion.FORGED_IN_THE_BARRENS:	"Forged in the Barrens",
	Expansion.WAILING_CAVERNS:			"Wailing Caverns",
	Expansion.UNITED_IN_STORMWIND:		"United in Stormwind",
	Expansion.DEADMINES:				"Deadmines",
	Expansion.FRACTURED_IN_ALTERAC:		"Fractured in Alterac Valley",
	Expansion.ONYXIAS_LAIR:				"Onyxia's Lair",
	Expansion.SUNKEN_CITY:				"Voyage to the Sunken City",
	Expansion.THRONE_OF_THE_TIDES:		"Throne of the Tides",
	Expansion.MURDER_AT_CASTLE_NATHRIA:	"Murder at Castle Nathria",
	Expansion.MAW_AND_DISORDER:			"Maw and Disorder",
	Expansion.MARCH_OF_THE_LICH_KING:	"March of the Lich King",
	Expansion.RETURN_TO_NAXXRAMAS:		"Return to Naxxramas",
	Expansion.FESTIVAL_OF_LEGENDS:		"Festival of Legends",
	Expansion.AUDIOPOCALYPSE:			"Audiopocalypse",
	Expansion.TITANS:					"TITANS",
	Expansion.CAVERNS_OF_TIME:			"Caverns of Time",
	Expansion.FALL_OF_ULDUAR:			"Fall of Ulduar",
	Expansion.SHOWDOWN_IN_THE_BADLANDS:	"Showdown in the Badlands",
	Expansion.DELVE_INTO_DEEPHOLM:		"Delve into Deepholm",
	Expansion.DUSKLAND:					"Duskland's Sorcellery",
	Expansion.EVENT:					"Event",
	Expansion.WHIZBANG_WORKSHOP:		"Whizbang's Workshop",
	Expansion.DR_BOOMS_INVENTIONS:		"Dr. Boom's Incredible Inventions",
	Expansion.PERILS_IN_PARADISE:		"Perils in Paradise",
	Expansion.TRAVELING_TRAVEL_AGENCY:	"The Traveling Travel Agency",
	Expansion.THE_GREAT_DARK_BEYOND:	"The Great Dark Beyond",
	Expansion.HEROES_OF_STARCRAFT:		"Heroes of Starcraft",
	Expansion.INTO_THE_EMERALD_DREAM:	"Into the Emerald Dream",
	Expansion.HEARTHCARDS:				"Hearthcards",
	Expansion.NEW:						"New"
}

def to_string(expansion: Expansion) -> str:
	return EXPANSION_REPR[expansion]

SYMBOL_DICT = {
	Expansion.ALL:						"ALL",
	Expansion.GRYPHON:					"CORE_GRYPHON",
	Expansion.HYDRA:					"CORE_HYDRA",
	Expansion.WOLF:						"CORE_WOLF",
	Expansion.PEGASUS:					"CORE",
	Expansion.BASIC:					"BAS",
	Expansion.LEGACY:					"LEG",
	Expansion.DEMON_HUNTER_INITIATE:	"DHI",
	Expansion.PATH_OF_ARTHAS:			"PoA",
	Expansion.EXPERT1:					"EXP1",
	Expansion.ARENA:					"ARN",
	Expansion.TAVERNS_OF_TIME:			"ToT",
	Expansion.BATTLEGROUNDS:			"BG",
	Expansion.BATTLEGROUNDS_DUOS:		"BGD",
	Expansion.DUELS:					"Duels",
	Expansion.TWIST:					"Twist",
	Expansion.NAXXRAMAS:				"NAXX",
	Expansion.GOBLINS_VS_GNOMES:		"GvG",
	Expansion.BLACKROCK_MOUNTAIN:		"BRM",
	Expansion.THE_GRAND_TOURNAMENT:		"TGT",
	Expansion.LEAGUE_OF_EXPLORERS:		"LoE",
	Expansion.OLD_GODS:					"DTA",
	Expansion.KARAZHAN:					"KARA",
	Expansion.GADGETZAN:				"MSoG",
	Expansion.UNGORO:					"UNG",
	Expansion.FROZEN_THRONE:			"CTG",
	Expansion.KOBOLDS_AND_CATACOMBS:	"K&C",
	Expansion.WITCHWOOD:				"LBM",
	Expansion.BOOMSDAY:					"BOOM",
	Expansion.RASTAKHAN:				"JDR",
	Expansion.RISE_OF_SHADOWS:			"ÉdO",
	Expansion.SAVIORS_OF_ULDUM:			"SoU",
	Expansion.DESCENT_OF_DRAGONS:		"DoD",
	Expansion.GALAKRONDS_AWAKENING: 	"DoDA",
	Expansion.ASHES_OF_OUTLAND:			"AoO",
	Expansion.SCHOLOMANCE_ACADEMY:		"SMA",
	Expansion.DARKMOON_FAIRE:			"FJS",
	Expansion.DARKMOON_RACES:			"FJSM",
	Expansion.FORGED_IN_THE_BARRENS:	"FitB",
	Expansion.WAILING_CAVERNS:			"FitBM",
	Expansion.UNITED_IN_STORMWIND:		"UiS",
	Expansion.DEADMINES:				"UiSM",
	Expansion.FRACTURED_IN_ALTERAC:		"FiAV",
	Expansion.ONYXIAS_LAIR:				"FiAVM",
	Expansion.SUNKEN_CITY:				"VSC",
	Expansion.THRONE_OF_THE_TIDES:		"VSCM",
	Expansion.MURDER_AT_CASTLE_NATHRIA:	"MCN",
	Expansion.MAW_AND_DISORDER:			"MCNM",
	Expansion.MARCH_OF_THE_LICH_KING:	"MLK",
	Expansion.RETURN_TO_NAXXRAMAS:		"MLKM",
	Expansion.FESTIVAL_OF_LEGENDS:		"FoL",
	Expansion.AUDIOPOCALYPSE:			"FoLM",
	Expansion.TITANS:					"TTN",
	Expansion.CAVERNS_OF_TIME:			"CoT",
	Expansion.FALL_OF_ULDUAR:			"TTNM",
	Expansion.SHOWDOWN_IN_THE_BADLANDS:	"SitB",
	Expansion.DELVE_INTO_DEEPHOLM:		"SitBM",
	Expansion.EVENT:					"EVENT",
	Expansion.WHIZBANG_WORKSHOP:		"AdM",
	Expansion.DR_BOOMS_INVENTIONS:		"AdMM",
	Expansion.PERILS_IN_PARADISE:		"PiP",
	Expansion.TRAVELING_TRAVEL_AGENCY:	"PiPM",
	Expansion.THE_GREAT_DARK_BEYOND:	"GDB",
	Expansion.HEROES_OF_STARCRAFT:		"GDBM",
	Expansion.INTO_THE_EMERALD_DREAM:	"IED",
	Expansion.DUSKLAND:					"SST",
	Expansion.HEARTHCARDS:				"HSC",
	Expansion.NEW:						"NEW"
}

def to_symbol(expansion: Expansion) -> str:
	return SYMBOL_DICT[expansion]