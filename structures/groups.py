from enum import Enum, auto

class Group(Enum):
	NONE				= auto()
	ADAPT				= auto()
	ADVENTURER			= auto()
	ANOMALY				= auto()
	AURA				= auto()
	BALLER				= auto()
	BASIC_HERO_POWER	= auto()
	BASIC_TOTEM			= auto()
	BUDDY				= auto()
	CAT					= auto()
	COIN				= auto()
	CONCOCTION			= auto()
	CREWMATE			= auto()
	DARK_GIFT			= auto()
	DRINK				= auto()
	EUDORA_LOOT			= auto()
	EXCAVATE_TREASURE	= auto()
	FOLLOWER_OF_CTHUN	= auto()
	GALAKROND			= auto()
	HORDE_WARRIOR		= auto()
	IMBUED_HERO_POWER	= auto()
	IMP					= auto()
	KAZAKUSAN_TREASURE	= auto()
	LACKEY				= auto()
	LIBRAM				= auto()
	POISON				= auto()
	PROTOSS				= auto()
	RELIC				= auto()
	RIFF				= auto()
	SI_7				= auto()
	SILVER_HAND_RECRUIT	= auto()
	SPARE_PART			= auto()
	SPY_GIZMO			= auto()
	STARSHIP			= auto()
	TERRAN				= auto()
	TREANT				= auto()
	UPGRADED_HERO_POWER	= auto()
	WATCHPOST			= auto()
	WHELP				= auto()
	WILD_GOD 			= auto()
	ZERG				= auto()

GROUP_DICT = {
	"":								Group.NONE,
	"Aucun":						Group.NONE,
	"None":							Group.NONE,
	"Adaptation":					Group.ADAPT,
	"Adapt":						Group.ADAPT,
	"Aventurier":					Group.ADVENTURER,
	"Adventurer":					Group.ADVENTURER,
	"Anomalie":						Group.ANOMALY,
	"Anomaly":						Group.ANOMALY,
	"Aura":							Group.AURA,
	"Modeleur":						Group.BALLER,
	"Baller":						Group.BALLER,
	"Pouvoir héroïque de base":		Group.BASIC_HERO_POWER,
	"Base Hero Power":				Group.BASIC_HERO_POWER,
	"Totem de base":				Group.BASIC_TOTEM,
	"Basic Totem":					Group.BASIC_TOTEM,
	"Compagnon":					Group.BUDDY,
	"Buddy":						Group.BUDDY,
	"Chat":							Group.CAT,
	"Cat":							Group.CAT,
	"Pièce":						Group.COIN,
	"Coin":							Group.COIN,
	"Décoction":					Group.CONCOCTION,
	"Concoction":					Group.CONCOCTION,
	"Membre d’équipage":			Group.CREWMATE,
	"Crewmate":						Group.CREWMATE,
	"Sombre don":					Group.DARK_GIFT,
	"Dark Gift":					Group.DARK_GIFT,
	"Boisson":						Group.DRINK,
	"Drink":						Group.DRINK,
	"Butin d’Eudora":				Group.EUDORA_LOOT,
	"Eudoras's Loot":				Group.EUDORA_LOOT,
	"Trésor à déterrer":			Group.EXCAVATE_TREASURE,
	"Excavate Treasure":			Group.EXCAVATE_TREASURE,
	"Disciple de C’Thun":			Group.FOLLOWER_OF_CTHUN,
	"Follower of C'Thun":			Group.FOLLOWER_OF_CTHUN,
	"Galakrond":					Group.GALAKROND,
	"Guerrier de la Horde":			Group.HORDE_WARRIOR,
	"Horde Warrior":				Group.HORDE_WARRIOR,
	"Pouvoir héroïque empreint":	Group.IMBUED_HERO_POWER,
	"Imbued Hero Power":			Group.IMBUED_HERO_POWER,
	"Diablotin":					Group.IMP,
	"Imp":							Group.IMP,
	"Trésor de Kazakusan":			Group.KAZAKUSAN_TREASURE,
	"Kazakusan's Treasure":			Group.KAZAKUSAN_TREASURE,
	"Laquais":						Group.LACKEY,
	"Lackey":						Group.LACKEY,
	"Libram":						Group.LIBRAM,
	"Poison":						Group.POISON,
	"Protoss":						Group.PROTOSS,
	"Relique":						Group.RELIC,
	"Relic":						Group.RELIC,
	"Riff":							Group.RIFF,
	"SI:7":							Group.SI_7,
	"Recrue de la Main d’argent":	Group.SILVER_HAND_RECRUIT,
	"Silver Hand Recruit":			Group.SILVER_HAND_RECRUIT,
	"Pièce détachée":				Group.SPARE_PART,
	"Spare Part":					Group.SPARE_PART,
	"Gadget d’espion":				Group.SPY_GIZMO,
	"Spy Gizmo":					Group.SPY_GIZMO,
	"Vaisseau":						Group.STARSHIP,
	"Starship":						Group.STARSHIP,
	"Terran":						Group.TERRAN,
	"Tréant":						Group.TREANT,
	"Treant":						Group.TREANT,
	"Pouvoir héroïque amélioré":	Group.UPGRADED_HERO_POWER,
	"Upgraded Hero Power":			Group.UPGRADED_HERO_POWER,
	"Poste de guerre":				Group.WATCHPOST,
	"Watchpost":					Group.WATCHPOST,
	"Dragonnet":					Group.WHELP,
	"Whelp":						Group.WHELP,
	"Dieu sauvage":					Group.WILD_GOD,
	"Wild God":						Group.WILD_GOD,
	"Zerg":							Group.ZERG,
}

def to_group(s: str) -> Group:
	return GROUP_DICT[s]

GROUP_REPR = {
	Group.NONE:					"",
	Group.ADAPT:				"Adaptation",
	Group.ADVENTURER:			"Aventurier",
	Group.ANOMALY:				"Anomalie",
	Group.AURA:					"Aura",
	Group.BALLER:				"Modeleur",
	Group.BASIC_HERO_POWER:		"Pouvoir héroïque de base",
	Group.BASIC_TOTEM:			"Totem de base",
	Group.BUDDY:				"Compagnon",
	Group.CAT:					"Chat",
	Group.COIN:					"Pièce",
	Group.CONCOCTION:			"Décoction",
	Group.CREWMATE:				"Membre d’équipage",
	Group.DRINK:				"Boisson",
	Group.EUDORA_LOOT:			"Butin d’Eudora",
	Group.EXCAVATE_TREASURE:	"Trésor à déterrer",
	Group.FOLLOWER_OF_CTHUN:	"Disciple de C’Thun",
	Group.GALAKROND:			"Galakrond",
	Group.HORDE_WARRIOR:		"Guerrier de la Horde",
	Group.IMP:					"Diablotin",
	Group.KAZAKUSAN_TREASURE:	"Trésor de Kazakusan",
	Group.LACKEY:				"Laquais",
	Group.LIBRAM:				"Libram",
	Group.POISON:				"Poison",
	Group.PROTOSS:				"Protoss",
	Group.RELIC:				"Relique",
	Group.RIFF:					"Riff",
	Group.SI_7:					"SI:7",
	Group.SILVER_HAND_RECRUIT:	"Recrue de la Main d’argent",
	Group.SPARE_PART:			"Pièce détachée",
	Group.SPY_GIZMO:			"Gadget d’espion",
	Group.STARSHIP:				"Vaisseau",
	Group.TREANT:				"Tréant",
	Group.UPGRADED_HERO_POWER:	"Pouvoir héroïque amélioré",
	Group.WATCHPOST:			"Poste de guerre",
	Group.WHELP:				"Dragonnet",
	Group.ZERG:					"Zerg"
}

def to_string(group: Group) -> str:
	return GROUP_REPR[group]