from enum import Enum, auto

class Item(Enum):
	ARMOR					= auto()
	ARTWORK					= auto()
	ATTACK					= auto()
	BACK_BUTTON				= auto()
	BACKGROUND				= auto()
	BLUR					= auto()
	CARD_BACK				= auto()
	CARD_BACKGROUND			= auto()
	CARPET					= auto()
	CARPET2					= auto()
	CHANGEPACK_BUTTON		= auto()
	CLASS					= auto()
	CLASS_ACCESS			= auto()
	CONTINUE_BUTTON			= auto()
	COST					= auto()
	COST2					= auto()
	COST_FILTER				= auto()
	DECK_HOLDER				= auto()
	DESCRIPTION_BACKGROUND	= auto()
	DISENCHANTMENT_BUTTON	= auto()
	DRAGON					= auto()
	DURABILITY				= auto()
	EXPANSION				= auto()
	FRAME					= auto()
	GEM						= auto()
	GOLDEN					= auto()
	GOLDEN_FILTER			= auto()
	HEALTH					= auto()
	HITBOX_IN				= auto()
	HITBOX_OUT				= auto()
	HOVER_LIGHT				= auto()
	LEAVES_LEFT				= auto()
	LEAVES_RIGHT			= auto()
	LEFT_ARROW				= auto()
	LOGO					= auto()
	MINIATURE_BACKGROUND	= auto()
	MINIATURE_COST			= auto()
	NAME_BANNER				= auto()
	NUMBER_POSSESSER		= auto()
	PORTRAIT				= auto()
	RIGHT_ARROW				= auto()
	RUNE_HOLDER				= auto()
	RUNE1					= auto()
	RUNE2					= auto()
	RUNE3					= auto()
	SIDE_BANNER				= auto()
	TRIBE_BANNER			= auto()
	TRIPLE					= auto()
	UNDERMANA				= auto()
	WATERMARK				= auto()

is_in_card_display_dict = {
	Item.ARMOR:					True,
	Item.ARTWORK:				True,
	Item.ATTACK:				True,
	Item.BACK_BUTTON:			False,
	Item.BACKGROUND:			False,
	Item.BLUR:					False,
	Item.CARD_BACK:				False,
	Item.CARD_BACKGROUND:		True,
	Item.CARPET:				True,
	Item.CARPET2:				True,
	Item.CHANGEPACK_BUTTON:		False,
	Item.CLASS:					True,
	Item.CLASS_ACCESS:			False,
	Item.CONTINUE_BUTTON:		False,
	Item.COST:					True,
	Item.COST2:					True,
	Item.COST_FILTER:			False,
	Item.DECK_HOLDER:			False,
	Item.DESCRIPTION_BACKGROUND:True,
	Item.DISENCHANTMENT_BUTTON:	False,
	Item.DRAGON:				True,
	Item.DURABILITY:			True,
	Item.EXPANSION:				True,
	Item.FRAME:					True,
	Item.GEM:					True,
	Item.GOLDEN:				True,
	Item.GOLDEN_FILTER:			False,
	Item.HEALTH:				True,
	Item.HITBOX_IN:				False,
	Item.HITBOX_OUT:			False,
	Item.HOVER_LIGHT:			False,
	Item.LEFT_ARROW:			False,
	Item.LEAVES_LEFT:			True,
	Item.LEAVES_RIGHT:			True,
	Item.LOGO:					True,
	Item.MINIATURE_BACKGROUND:	False,
	Item.MINIATURE_COST:		False,
	Item.NAME_BANNER:			True,
	Item.NUMBER_POSSESSER:		False,
	Item.PORTRAIT:				False,
	Item.RIGHT_ARROW:			False,
	Item.RUNE_HOLDER:			True,
	Item.RUNE1:					True,
	Item.RUNE2:					True,
	Item.RUNE3:					True,
	Item.SIDE_BANNER:			True,
	Item.TRIBE_BANNER:			True,
	Item.TRIPLE:				False,
	Item.UNDERMANA:				True,
	Item.WATERMARK:				True
}

def is_in_card_display(item: Item) -> bool:
	return is_in_card_display_dict[item]