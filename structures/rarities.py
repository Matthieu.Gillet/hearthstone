from enum import Enum, auto

class Rarity(Enum):
	BASIC		= auto()
	BASIC_D		= auto()
	COMMON		= auto()
	RARE		= auto()
	EPIC		= auto()
	LEGENDARY	= auto()

RARITY_DICT = {
	"Basique":		Rarity.BASIC, 
	"Basic":		Rarity.BASIC, 
	"Basique-D":	Rarity.BASIC_D, 
	"Basic-D":		Rarity.BASIC_D, 
	"Commune":		Rarity.COMMON,
	"Common":		Rarity.COMMON,  
	"Rare":			Rarity.RARE, 
	"Épique":		Rarity.EPIC, 
	"Epic":			Rarity.EPIC, 
	"Légendaire":	Rarity.LEGENDARY, 
	"Legendary":	Rarity.LEGENDARY
}

RARITY_GROUPING_DICT = {
	Rarity.BASIC:		Rarity.COMMON, 
	Rarity.BASIC_D:		Rarity.LEGENDARY, 
	Rarity.COMMON:		Rarity.COMMON, 
	Rarity.RARE:		Rarity.RARE, 
	Rarity.EPIC:		Rarity.EPIC, 
	Rarity.LEGENDARY:	Rarity.LEGENDARY
}

def group(rarity: Rarity) -> Rarity:
	return RARITY_GROUPING_DICT[rarity]

RARITY_TO_INT_DICT = {
	Rarity.BASIC:		0, 
	Rarity.BASIC_D:		3, 
	Rarity.COMMON:		0, 
	Rarity.RARE:		1, 
	Rarity.EPIC:		2, 
	Rarity.LEGENDARY:	3
}

NUMBER_RARITIES = 4

def to_int(rarity: Rarity) -> int:
	return RARITY_TO_INT_DICT[rarity]

RARITY_REPR = {
	Rarity.BASIC:		"Basic", 
	Rarity.BASIC_D:		"Basic-D", 
	Rarity.COMMON:		"Common", 
	Rarity.RARE:		"Rare", 
	Rarity.EPIC:		"Epic", 
	Rarity.LEGENDARY:	"Legendary"
}

def to_string(rarity: Rarity) -> str:
	return RARITY_REPR[rarity]