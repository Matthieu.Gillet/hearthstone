from enum import Enum, auto

def group(c):
	return CLASS_GROUPING_DICT[c]
class Class(Enum):
	DEATH_KNIGHT	= auto()
	DEMON_HUNTER	= auto()
	DRUID			= auto()
	HUNTER			= auto()
	MAGE			= auto()
	PALADIN			= auto()
	PRIEST			= auto()
	ROGUE			= auto()
	SHAMAN			= auto()
	WARLOCK			= auto()
	WARRIOR			= auto()
	
	ANY				= auto()
	DREAM			= auto()

	GOONS			= auto()
	JADE			= auto()
	KABAL			= auto()

	PROTOSS			= auto()
	TERRAN			= auto()
	ZERG			= auto()
	

	def __eq__(self,other):
		return (group(self).value) == (group(other).value)
	def __ne__(self,other):
		return (group(self).value) != (group(other).value)
	def __lt__(self, other):
		return (group(self).value) < (group(other).value)
	def __le__(self, other):
		return (group(self).value) <= (group(other).value)
	def __gt__(self, other):
		return (group(self).value) > (group(other).value)
	def __ge__(self, other):
		return (group(self).value) >= (group(other).value)
	def __hash__(self):
		return ((self.value))

CLASS_DICT = {
	"Death Knight":			Class.DEATH_KNIGHT,
	"Chevalier de la mort":	Class.DEATH_KNIGHT,
	"Demon Hunter":			Class.DEMON_HUNTER,
	"Chasseur de démons":	Class.DEMON_HUNTER,
	"Druid":				Class.DRUID,
	"Druide":				Class.DRUID,
	"Hunter":				Class.HUNTER,
	"Chasseur":				Class.HUNTER,
	"Mage":					Class.MAGE,
	"Paladin":				Class.PALADIN,
	"Priest":				Class.PRIEST,
	"Prêtre":				Class.PRIEST,
	"Rogue":				Class.ROGUE,
	"Voleur":				Class.ROGUE,
	"Shaman":				Class.SHAMAN,
	"Chaman":				Class.SHAMAN,
	"Warrior":				Class.WARRIOR,
	"Guerrier":				Class.WARRIOR,
	"Warlock":				Class.WARLOCK,
	"Démoniste":			Class.WARLOCK,
	"Any":					Class.ANY,
	"Neutre":				Class.ANY,
	"Dream":				Class.DREAM,
	"Rêve":					Class.DREAM,

	"Goons":				Class.GOONS,
	"Dessoudeurs":			Class.GOONS,
	"Jade":					Class.JADE,
	"Kabal":				Class.KABAL,
	"Kabale":				Class.KABAL,

	"Protoss":				Class.PROTOSS,
	"Terran":				Class.TERRAN,
	"Zerg":					Class.ZERG
}

CLASS_GROUPING_DICT = {
	Class.DEATH_KNIGHT:	Class.DEATH_KNIGHT,
	Class.DEMON_HUNTER:	Class.DEMON_HUNTER,
	Class.DRUID:		Class.DRUID,
	Class.HUNTER:		Class.HUNTER,
	Class.MAGE:			Class.MAGE,
	Class.PALADIN:		Class.PALADIN,
	Class.PRIEST:		Class.PRIEST,
	Class.ROGUE:		Class.ROGUE,
	Class.SHAMAN:		Class.SHAMAN,
	Class.WARRIOR:		Class.WARRIOR,
	Class.WARLOCK:		Class.WARLOCK,
	Class.ANY:			Class.ANY,
	Class.DREAM:		Class.DREAM,

	Class.GOONS:		Class.ANY,
	Class.JADE:			Class.ANY,
	Class.KABAL:		Class.ANY,

	Class.PROTOSS:		Class.ANY,
	Class.TERRAN:		Class.ANY,
	Class.ZERG:			Class.ANY
}
# Defined above, because it is used in the Class function.
def group(c: Class) -> Class:
	return CLASS_GROUPING_DICT[c]

CLASS_REPR = {
	Class.DEATH_KNIGHT:	"Death Knight",
	Class.DEMON_HUNTER:	"Demon Hunter",
	Class.DRUID:		"Druid",
	Class.HUNTER:		"Hunter",
	Class.MAGE:			"Mage",
	Class.PALADIN:		"Paladin",
	Class.PRIEST:		"Priest",
	Class.ROGUE:		"Rogue",
	Class.SHAMAN:		"Shaman",
	Class.WARRIOR:		"Warrior",
	Class.WARLOCK:		"Warlock",
	Class.ANY:			"Any",
	Class.DREAM:		"Dream",

	Class.GOONS:		"Goons",
	Class.JADE:			"Jade",
	Class.KABAL:		"Kabal",

	Class.PROTOSS:		"Protoss",
	Class.TERRAN:		"Terran",
	Class.ZERG:			"Zerg"
}

CLASS_REPR_FR = {
	Class.DEATH_KNIGHT:	"Chevalier de la mort",
	Class.DRUID:		"Druide",
	Class.HUNTER:		"Chasseur",
	Class.MAGE:			"Mage",
	Class.PALADIN:		"Paladin",
	Class.PRIEST:		"Prêtre",
	Class.ROGUE:		"Voleur",
	Class.SHAMAN:		"Chaman",
	Class.WARRIOR:		"Guerrier",
	Class.WARLOCK:		"Démoniste",
	Class.ANY:			"Neutre",
	Class.DREAM:		"Rêve",

	Class.GOONS:		"Dessoudeurs",
	Class.JADE:			"Jade",
	Class.KABAL:		"Kabale",

	Class.PROTOSS:		"Protoss",
	Class.TERRAN:		"Terran",
	Class.ZERG:			"Zerg"
}

def to_string(c: Class):
	return CLASS_REPR[c]

CLASS_SHORTCUT_DICT = {
	Class.GOONS:		[Class.HUNTER, Class.PALADIN, Class.WARRIOR],
	Class.JADE:			[Class.DRUID, Class.ROGUE, Class.SHAMAN],
	Class.KABAL:		[Class.MAGE, Class.PRIEST, Class.WARLOCK],

	Class.PROTOSS:		[Class.DRUID, Class.MAGE, Class.PRIEST, Class.ROGUE],
	Class.TERRAN:		[Class.PALADIN, Class.SHAMAN, Class.WARRIOR],
	Class.ZERG:			[Class.DEATH_KNIGHT, Class.DEMON_HUNTER, Class.HUNTER, Class.WARLOCK]
}

def expand(c: Class):
	try:
		return CLASS_SHORTCUT_DICT[c]
	except KeyError:
		return [c]