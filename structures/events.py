from enum import Enum, auto

class Event(Enum):
	CLICK		= auto()
	HOVER		= auto()
	LEFT_ARROW	= auto()
	RIGHT_ARROW	= auto()
	RIGHT_CLICK	= auto()

EVENT_TO_TAG_DICT = {
	Event.CLICK:		"<ButtonPress-1>", 
	Event.HOVER:		"<Enter>", 
	Event.LEFT_ARROW:	"<Left>",
	Event.RIGHT_ARROW:	"<Right>",
	Event.RIGHT_CLICK:	"<ButtonPress-3>"
}
def to_tag(event: Event) -> str:
	return EVENT_TO_TAG_DICT[event]