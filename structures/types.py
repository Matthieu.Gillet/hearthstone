from enum import Enum, auto

class Type(Enum):
	NONE		= auto()
	DEBUG		= auto()
	MINION		= auto()
	SPELL		= auto()
	WEAPON		= auto()
	HERO_POWER	= auto()
	HERO		= auto()
	PORTRAIT	= auto()
	LOCATION	= auto()
	TRINKET		= auto()
	GLYPH		= auto()

TYPE_DICT = {
	"Debug":			Type.DEBUG,
	"Serviteur":		Type.MINION,
	"Minion":			Type.MINION,
	"Sort":				Type.SPELL,
	"Spell":			Type.SPELL,
	"Arme":				Type.WEAPON,
	"Weapon":			Type.WEAPON,
	"Pouvoir héroïque":	Type.HERO_POWER,
	"Hero Power":		Type.HERO_POWER,
	"Héros":			Type.HERO,
	"Hero":				Type.HERO,
	"Portrait":			Type.PORTRAIT,
	"Lieu":				Type.LOCATION,
	"Location":			Type.LOCATION,
	"Bibelot":			Type.TRINKET,
	"Trinket":			Type.TRINKET,
	"Glyphe":			Type.GLYPH,
	"Glyph":			Type.GLYPH
}

TYPE_REPR = {
	Type.NONE:		"",
	Type.DEBUG:		"",
	Type.MINION:	"Serviteur",
	Type.SPELL:		"Sort",
	Type.WEAPON:	"Arme",
	Type.HERO_POWER:"Pouvoir héroïque",
	Type.HERO:		"Héros",
	Type.PORTRAIT:	"Portrait",
	Type.LOCATION:	"Lieu",
	Type.TRINKET:	"Bibelot",
	Type.GLYPH:		"Glyphe"
}

def to_string(t: Type) -> str:
	return TYPE_REPR[t]