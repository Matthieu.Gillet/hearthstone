from enum import Enum, auto

class Rune(Enum):
	NONE		= auto()
	BLOOD		= auto()
	FROST		= auto()
	UNHOLY		= auto()

RUNE_DICT = {
	"":				Rune.NONE,
	"Blood":		Rune.BLOOD,
	"Sang":			Rune.BLOOD,
	"S":			Rune.BLOOD,
	"Frost":		Rune.FROST,
	"Givre":		Rune.FROST,
	"G":			Rune.FROST,
	"Unholy":		Rune.UNHOLY,
	"Impie":		Rune.UNHOLY,
	"I":			Rune.UNHOLY
}
def to_rune(s: str) -> Rune:
	return RUNE_DICT[s]

RUNE_REPR = {
	Rune.NONE:		"",
	Rune.BLOOD:		"Sang",
	Rune.FROST:		"Givre",
	Rune.UNHOLY:	"Impie"
}
def to_string(rune: Rune) -> str:
	return RUNE_REPR[rune]

RUNE_FILENAMES = {
	Rune.NONE:		"",
	Rune.BLOOD:		"rune_blood.png",
	Rune.FROST:		"rune_frost.png",
	Rune.UNHOLY:	"rune_unholy.png"
}

GOLDEN_RUNE_FILENAMES = {
	Rune.NONE:		"",
	Rune.BLOOD:		"rune_blood_golden.png",
	Rune.FROST:		"rune_frost_golden.png",
	Rune.UNHOLY:	"rune_unholy_golden.png"
}

def to_filename(rune: Rune, golden: bool = False) -> str:
	if golden:
		return GOLDEN_RUNE_FILENAMES[rune]
	else:
		return RUNE_FILENAMES[rune]