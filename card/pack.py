from numpy.random import randint

import settings

from structures import expansions
from structures.expansions import Expansion
from structures import rarities
from structures.rarities import Rarity
from structures.classes import Class

from card import card
from card import card_list
from card import collection

import settings

def generate_pack_card(expansion: Expansion, dynamic: bool = True,
	card1: card.Card = None, card2: card.Card = None, card3: card.Card = None,
	card4: card.Card = None, rare: bool = False) -> card.Card:
	"""Generates a random card within a given expansion."""

	dynamic = dynamic and not expansion is Expansion.ALL
	
	number = randint(1,1001)
	
	#Choice of the list from which to draw
	if number <= settings.STAT_COMMON and (not rare):
		rarity = Rarity.COMMON
	elif number <= settings.STAT_RARE:
		rarity = Rarity.RARE
	elif number <= settings.STAT_EPIC:
		rarity = Rarity.EPIC
	else:
		rarity = Rarity.LEGENDARY

	rarity_number = rarities.to_int(rarity)

	if not settings.TAVERN_MODE:
		if expansion is Expansion.ALL:
			chosen_list = card_list.CARD_LIST_RARITY[rarity_number].copy()
		else:
			expansion_number = expansions.to_int(expansion)
			chosen_list = card_list.CARD_LIST_EXPANSION_RARITY[expansion_number][rarity_number].copy()
	else:
		if expansion is Expansion.ALL:
			chosen_list = card_list.CARD_LIST.copy()
		else:
			expansion_number = expansions.to_int(expansion)
			chosen_list = card_list.CARD_LIST_EXPANSION[expansion_number].copy()
	
	if chosen_list == []:
		if rarity_number == 0:
			chosen_card = card.Debug(expansion, "Carte commune", "Carte commune",
				None, None, [], [Class.ANY], Rarity.COMMON, 
				"Vous avez ouvert une carte commune ! Mais il n’y a aucune carte commune dans cette extension...",
				[], False, False, False, [], None)
		elif rarity_number == 1:
			chosen_card = card.Debug(expansion, "Carte rare", "Carte rare",
				None, None, [], [Class.ANY], Rarity.RARE, 
				"Vous avez ouvert une carte rare ! Mais il n’y a aucune carte rare dans cette extension...",
				[], False, False, False, [], None)
		elif rarity_number == 2:
			chosen_card = card.Debug(expansion, "Carte épique", "Carte épique",
				None, None, [], [Class.ANY], Rarity.EPIC, 
				"Vous avez ouvert une carte épique ! Mais il n’y a aucune carte épique dans cette extension...",
				[], False, False, False, [], None)
		else:
			chosen_card = card.Debug(expansion, "Carte légendaire", "Carte légendaire",
				None, None, [], [Class.ANY], Rarity.LEGENDARY, 
				"Vous avez ouvert une carte légendaire ! Mais il n’y a aucune carte légendaire dans cette extension...",
				[], False, False, False, [], None)

	else:

		if dynamic:
			list_copy = chosen_list.copy()

			if rarity_number == 3:
				if card1 is not None and card1 in chosen_list:
					chosen_list.remove(card1)
				if card2 is not None and card2 in chosen_list:
					chosen_list.remove(card2)
				if card3 is not None and card3 in chosen_list:
					chosen_list.remove(card3)
				if card4 is not None and card4 in chosen_list:
					chosen_list.remove(card4)

				#Intermediate step: if there are not enough cards of that rarity left, we stop there.
				if chosen_list != []:
					list_copy = chosen_list.copy()

					#While it's possible, an opened legendary should be one
					#that is not already in the collection.
					for c in list_copy:
						golden_card = c.golden_copy()

						if (collection.COLLECTION.get_how_many(c)
							+ collection.COLLECTION.get_how_many(golden_card) >= 1):
							chosen_list.remove(c)
			else:
				l = [card1, card2]
				if card2 is not None and card2 == card1 and card2 in chosen_list:
					chosen_list.remove(card2)
				if card3 is not None and card3 in l and card3 in chosen_list:
					chosen_list.remove(card3)
				l.append(card3)
				if card4 is not None and card4 in l and card4 in chosen_list:
					chosen_list.remove(card4)

				#Intermediate step: if there are not enough legendary cards left, we stop there.
				if chosen_list != []:
					list_copy = chosen_list.copy()

					#While it's possible, an opened card should be one
					#that is not already twice in the collection.
					for c in list_copy:
						golden_card = c.golden_copy()
						if (collection.COLLECTION.get_how_many(c)
							+ collection.COLLECTION.get_how_many(golden_card)
							+ [i is not None and i in [c, golden_card] for i in l].count(True) >= 2):
							chosen_list.remove(c)


		if len(chosen_list) >= 1:
			chosen_card = chosen_list[randint(len(chosen_list))]
		else:
			chosen_card = list_copy[randint(len(list_copy))]

	return chosen_card

def make_golden_at_random(rarity: Rarity) -> bool:
	"""Give out a random bool that will make a card golden or not."""
	number = randint(1,1001)
	stat = settings.STAT_GOLDEN_DICT[rarity]

	return number > stat

def generate_pack(expansion: Expansion, dynamic: bool = True) -> list:
	"""Give out the cards get when opening a pack of a given expansion.
	The golden properties are taken into account."""
	card1 = generate_pack_card(expansion, dynamic).copy()
	card2 = generate_pack_card(expansion, dynamic, card1).copy()
	card3 = generate_pack_card(expansion, dynamic, card1, card2).copy()
	card4 = generate_pack_card(expansion, dynamic, card1, card2, card3).copy()
	card5 = generate_pack_card(expansion, dynamic, card1, card2, card3, card4, True).copy()

	if settings.TAVERN_MODE:
		if not card1.get_golden_version() is None and make_golden_at_random(card1.get_rarity()):
			card1 = card1.get_golden_version()
		if not card2.get_golden_version() is None and make_golden_at_random(card2.get_rarity()):
			card2 = card2.get_golden_version()
		if not card3.get_golden_version() is None and make_golden_at_random(card3.get_rarity()):
			card3 = card3.get_golden_version()
		if not card4.get_golden_version() is None and make_golden_at_random(card4.get_rarity()):
			card4 = card4.get_golden_version()
		if not card5.get_golden_version() is None and make_golden_at_random(card5.get_rarity()):
			card5 = card5.get_golden_version()
	else:
		card1.set_golden(make_golden_at_random(card1.get_rarity()))
		card2.set_golden(make_golden_at_random(card2.get_rarity()))
		card3.set_golden(make_golden_at_random(card3.get_rarity()))
		card4.set_golden(make_golden_at_random(card4.get_rarity()))
		card5.set_golden(make_golden_at_random(card5.get_rarity()))
		
	ans = [card1, card2, card3, card4]
	ans.insert(randint(0,4), card5)
	collection.add_card_list_to_collection(ans)

	return ans
