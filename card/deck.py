from structures import classes
from structures.classes import Class

from tools import normalization

from card import card

DECK_LIST = []
DEFAULT_CLASS = Class.ANY
DEFAULT_NAME = ""

class Deck_Card(object):
	def __init__(self, c: card.Card, total_number: int, golden_number: int):
		self.card = c.regular_copy()
		self.total_number = total_number
		self.golden_number = golden_number

	def __lt__(self, other):
		return (card.bettercost(self.card.get_cost()), normalization.text_to_id(self.card.get_key())) < (card.bettercost(other.card.get_cost()), normalization.text_to_id(other.card.get_key()))

	def how_golden(self):
		"""
		0: Not at all
		1: Partially
		2: Totally
		"""
		if self.golden_number == 0:
			return 0
		if self.golden_number != self.total_number:
			return 1
		return 2

	def __repr__(self):
		return "({}, {}, {})".format(self.card, self.total_number, self.golden_number)

class Deck(object):
	def __init__(self, name: int, Class: Class, contents: list, ordered: bool = True):
		self._name = name
		self._class = Class
		self._contents = contents
		self._ordered = ordered

	def get_name(self) -> str:
		return self._name

	def change_name(self, new_name: str):
		self._name = new_name

	def get_class(self) -> Class:
		return self._class

	def number_in_deck(self, c: card.Card) -> int:
		c = c.regular_copy()
		for dc in self._contents:
			if c == dc.card:
				return total_number
		return 0

	def is_in_deck(self, c: card.Card) -> bool:
		return self.number_in_deck(c) > 0

	def add_card(self, c: card.Card):
		# print("Adding {}...".format(c.get_name()))
		is_golden = c.is_golden()
		c = c.regular_copy()
		for i in range(len(self._contents)):
			dc = self._contents[i]
			if c == dc.card:
				if is_golden:
					self._contents[i].golden_number += 1
				self._contents[i].total_number += 1
				#print(self._contents)
				#print(self)
				return

		self._contents.append(Deck_Card(c, 1, int(is_golden)))
		if self._ordered:
			self._contents.sort()
		#print(self._contents)
		#print(self)

		return

	def add_card_list(self, l: list):
		for c in l:
			self.add_card(c)

	def remove_card(self, c: card.Card):
		c = c.regular_copy()
		for i in range(len(self._contents)):
			if self._contents[i].card == c:
				if self._contents[i].total_number == 1:
					self._contents.remove(self._contents[i])
				else:
					self._contents[i].total_number -= 1
					self._contents[i].golden_number = min(self._contents[i].golden_number, self._contents[i].total_number)
				break

	def get_contents(self) -> list:
		return self._contents

	def delete(self):
		DECK_LIST.remove(self)

	def __repr__(self):
		ans = self._name + " <{} Deck>\n".format(classes.to_string(self._class)) + '{'

		for dc in self._contents:
			ans += "\n  -{}- {}".format(dc.total_number, dc.card)

		return ans + "\n}"

def new_deck(name: str = DEFAULT_NAME, Class: Class = DEFAULT_CLASS, ordered: bool = True, add_to_deck_list: bool = True) -> Deck:
	d = Deck(name, Class, [], ordered)
	if add_to_deck_list:
		DECK_LIST.append(d)
	return d

def card_list_to_deck(l: list, name: str = DEFAULT_NAME, Class: Class = DEFAULT_CLASS, ordered: bool = True, add_to_deck_list: bool = True) -> Deck:
	d = new_deck(name, Class, ordered, add_to_deck_list)
	d.add_card_list(l)
	return d