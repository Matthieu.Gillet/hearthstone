import settings

from structures.classes import Class
from structures import expansions
from structures.expansions import Expansion
from structures import groups
from structures.groups import Group
from structures import rarities
from structures.rarities import Rarity
from structures.types import Type

from tools import normalization

from card import card
from card import database

CARD_LIST							= []
CARD_LIST_RARITY					= [[] for i in range(rarities.NUMBER_RARITIES)]
CARD_LIST_EXPANSION					= [[] for j in range(expansions.NUMBER_EXPANSIONS)]
CARD_LIST_EXPANSION_RARITY			= [[[] for i in range(rarities.NUMBER_RARITIES)]
	for j in range(expansions.NUMBER_EXPANSIONS)]

COMPLETE_CARD_LIST					= []
COMPLETE_CARD_LIST_RARITY			= [[] for i in range(rarities.NUMBER_RARITIES)]
COMPLETE_CARD_LIST_EXPANSION		= [[] for j in range(expansions.NUMBER_EXPANSIONS)]
COMPLETE_CARD_LIST_EXPANSION_RARITY	= [[[] for i in range(rarities.NUMBER_RARITIES)]
	for j in range(expansions.NUMBER_EXPANSIONS)]

def init_card_list():
	global CARD_LIST
	global CARD_LIST_RARITY
	global CARD_LIST_EXPANSION
	global CARD_LIST_EXPANSION_RARITY
	global COMPLETE_CARD_LIST
	global COMPLETE_CARD_LIST_RARITY
	global COMPLETE_CARD_LIST_EXPANSION
	global COMPLETE_CARD_LIST_EXPANSION_RARITY

	database_list = database.get_database()

	for card in database_list:
		rarity_number = rarities.to_int(card.get_rarity())
		expansion_number = expansions.to_int(card.get_expansion())

		COMPLETE_CARD_LIST.append(card)
		COMPLETE_CARD_LIST_RARITY[rarity_number].append(card)
		COMPLETE_CARD_LIST_EXPANSION[expansion_number].append(card)
		COMPLETE_CARD_LIST_EXPANSION_RARITY[expansion_number][rarity_number].append(card)

		if not card.get_type() is Type.PORTRAIT and ((settings.TAVERN_MODE and card.is_collectable_bg()) or (not settings.TAVERN_MODE and card.is_collectable())):
			CARD_LIST.append(card)
			CARD_LIST_RARITY[rarity_number].append(card)
			CARD_LIST_EXPANSION[expansion_number].append(card)
			CARD_LIST_EXPANSION_RARITY[expansion_number][rarity_number].append(card)

def get_duplicate_keys() -> list:
	ans = []
	n = len(COMPLETE_CARD_LIST)

	for i in range(n):
		a = COMPLETE_CARD_LIST[i]
		for j in range(i+1,n):
			b = COMPLETE_CARD_LIST[j]
			if a.get_key() == b.get_key():
				ans.append([i, j, a, b])

	return ans

def search_with_key(key: str, normalize: bool = True) -> card.Card:
	if normalize:
		key = normalization.normalize_apostrophe(key)

	for card in COMPLETE_CARD_LIST:
		if card.get_key() == key:
			return card
	return None

def search_indix(card: card.Card, list: list) -> int:
	counter = 0

	for other in list:
		if card == other:
			return counter
		
		counter += 1

	raise ValueError

def filter_with_group(group_filter: str, c: card.Card, reverse: bool = False):
	if group_filter is None:
		return True
	if group_filter[0:4] == "NON ":
		return filter_with_group(group_filter[4:], c, not reverse)

	if group_filter == "Collectionnable":
		cond = c.is_collectable()
	elif group_filter[0:4] == "Nom ":
		cond = c.get_name() == group_filter[4:]

	if reverse:
		return not cond
	else:
		return cond


def token_linking():
	for i in range(len(COMPLETE_CARD_LIST)):
		card = COMPLETE_CARD_LIST[i]
		related_cards = card.get_related_cards()
		new_related_cards = []
		special_triple = None
		a = False

		# Liste des cartes liées
		for j in range(len(related_cards)):
			related_card = related_cards[j]

			if j == 0 and "Triple : " in related_card:
				special_triple = related_card.replace("Triple : ", "")
			
			else:
				group_filter = None
				if "#" in related_card:
					related_card_split = related_card.split("#")
					related_card = related_card_split[0]
					group_filter = related_card_split[1]

				try:
					if related_card != "":
						g = groups.to_group(related_card)

					for group_token in COMPLETE_CARD_LIST:
						if g == "" or g in group_token.get_groups():
							b = filter_with_group(group_filter, group_token)

							if g in [Group.EXCAVATE_TREASURE, Group.IMBUED_HERO_POWER]:
								group_token_class = group_token.get_class()[0]

								if not group_token_class is Class.ANY and not Class.ANY in card.get_class():
									b = b and group_token_class in card.get_class_expanded()

							elif g in [Group.STARSHIP]:
								card_class = card.get_class_expanded()
								group_token_class = group_token.get_class()[0]

								if card_class[0] is Class.ANY:
									b = b and Class.ANY is group_token_class
								else:
									b = b and card_class[0] in group_token.get_class_expanded()

							if b:
								new_related_cards.append(group_token)

				except KeyError:
					related_card = search_with_key(related_card, normalize = False)
					if related_card is None:
						print("WARNING [Token linking]: '" + related_cards[j] + "' not found for card", card)
					else:
						new_related_cards.append(related_card)
		
		card._related_cards = new_related_cards

		if settings.TAVERN_MODE:
			if special_triple is None:
				golden_version = search_with_key(card.get_key() + " (Dorée)", normalize = False)
			else:
				golden_version = search_with_key(special_triple + " (Dorée)", normalize = False)

			if not golden_version is None:
				COMPLETE_CARD_LIST[i].set_golden_version(golden_version)

def get_related_cards_recursive(c: card.Card) -> list:
	new_related_cards = c.get_related_cards().copy()
	old_related_cards = [c]
	is_golden = c.is_golden()
	while new_related_cards != []:
		# print(new_related_cards)

		# related_card = new_related_cards.pop()
		related_card = new_related_cards[0]
		new_related_cards.remove(related_card)
		
		if not related_card in old_related_cards:
			if is_golden and not settings.TAVERN_MODE:
				old_related_cards.append(related_card.golden_copy())
			else:
				old_related_cards.append(related_card)

			related_card_related_cards = related_card.get_related_cards()
			if related_card_related_cards != []:
				for related_card_related_card in related_card_related_cards:
					if not related_card_related_card in new_related_cards:
						new_related_cards.append(related_card_related_card)
						# new_related_cards.insert(0, related_card_related_card)
	old_related_cards.remove(c)
	return old_related_cards