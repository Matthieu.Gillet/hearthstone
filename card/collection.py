from tools.text_tools import colorize_text, Color
from tools.normalization import text_to_id

import settings

from structures import rarities
from structures import classes
from structures.rarities import Rarity
from structures.classes import Class

from card import card
from card import card_list
from card.condition import Condition

class Collection(object):
	def __init__(self, opened_packs: int, dust: int, collection: dict):
		self._opened_packs = opened_packs
		self._dust = dust
		self._collection = collection

	def get_opened_packs(self) -> int:
		return self._opened_packs
	def get_dust(self) -> int:
		return self._dust
	def get_how_many(self, c: card.Card) -> int:
		return self._collection[c][0]
	def is_new(self, c: card.Card) -> bool:
		return self._collection[c][1]


	def add_dust(self, arg: int):
		self._dust += arg

	def add_card(self, c: card.Card, set_new: bool = True):
		try:
			x = self._collection[c]
			n, b = x
			
			# Adding a card automatically sets the "New!" value to True if there aren't already
			# 2 copies in the collection (or 1 for a legendary)
			if set_new:
				rarity = c.get_rarity()
				if rarities.group(rarity) is Rarity.LEGENDARY:
					b = b or n < 1
				else:
					b = b or n < 2
			self._collection[c] = (n + 1, b)
		except KeyError:
			print("Couldn't add card {} to collection.".format(c.get_key()))

	def add_card_list(self, arg: list, set_new: bool = True):
		for card in arg:
			self.add_card(card, set_new)

	def set_new(self, c: card.Card, arg: bool):
		self._collection[c] = (self._collection[c][0], arg)

	def disenchant(self, c: card.Card):
		try:
			x = self._collection[c]
			n, b = x
			if n > 0:
				extra_dust = settings.get_disenchant_value(c.get_rarity(), c.is_golden())
				self.add_dust(extra_dust)
				self._collection[c] = (n - 1, b)
		except KeyError:
			print("Couln't remove card {} from collection.".format(c.get_key()))


	def get_owned_cards(self, conds: list) -> list:
		ans = []
		for c in self._collection:
			valid = True
			for cond in conds:
				if not cond.eval(c):
					valid = False
					break
			if valid and self.get_how_many(c) != 0:
				ans.append(c)
		return ans

	def get_duplicates_summary(self) -> (list, list):
		regular_dust_values = [0] * Rarity.NUMBER_RARITIES
		golden_dust_values  = [0] * Rarity.NUMBER_RARITIES
		for c in self._collection:
			rarity = c.get_rarity()
			number_owned = self.get_how_many(c)
			if rarities.group(rarity) is Rarity.LEGENDARY:
				if number_owned > 1:
					if c.is_golden():
						golden_dust_values[Rarity.to_int(rarity)] += (number_owned - 1) * settings.get_disenchant_value(rarity, True)
					else:
						regular_dust_values[Rarity.to_int(rarity)] += (number_owned - 1) * settings.get_disenchant_value(rarity, False)
			else:
				if number_owned > 2:
					if c.is_golden():
						golden_dust_values[Rarity.to_int(rarity)] += (number_owned - 2) * settings.get_disenchant_value(rarity, True)
					else:
						regular_dust_values[Rarity.to_int(rarity)] += (number_owned - 2) * settings.get_disenchant_value(rarity, False)
		return regular_dust_values, golden_dust_values

	def disenchant_duplicates(self):
		for c in self._collection:
			rarity = c.get_rarity()
			number_owned = self.get_how_many(c)
			if rarities.group(rarity) is Rarity.LEGENDARY:
				if number_owned > 1:
					for i in range(number_owned - 1):
						self.disenchant(c)
			else:
				if number_owned > 2:
					for i in range(number_owned - 2):
						self.disenchant(c)

	def disenchant_all(self):
		for c in self._collection:
			rarity = c.get_rarity()
			number_owned = self.get_how_many(c)
			for i in range(number_owned):
				self.disenchant(c)

	def __getitem__(self, arg: card.Card) -> (int, bool):
		return self._collection[card]

	def __repr__(self) -> str:
		ans = "Packs opened: {}\nDust: {}\nCollection:".format(self._opened_packs, self._dust)

		for c in self._collection:
			if self.get_how_many(c) != 0:
				ans += '\n'

				if self.is_new(c):
					ans += colorize_text("NEW!", Color.RED, True)
					ans += ' '

				ans += "{}× {}".format(self.get_how_many(c), c)

		return ans

	def copy(self):
		return Collection(self._opened_packs, self._dust, self._collection)

COLLECTION = Collection(0, 0, {})

def init_collection():
	global COLLECTION
	for card in card_list.COMPLETE_CARD_LIST:
		golden_card = card.golden_copy()
		COLLECTION._collection[card] = (0, False)
		COLLECTION._collection[golden_card] = (0, False)

def add_card_to_collection(arg: card.Card, set_new: bool = True):
	COLLECTION.add_card(arg, set_new)
def add_card_list_to_collection(arg: list, set_new: bool = True):
	COLLECTION.add_card_list(arg, set_new)
def complete_expansion(i: int, set_new: bool = True):
	if i == 0:
		the_card_list = card_list.COMPLETE_CARD_LIST
	else:
		the_card_list = card_list.COMPLETE_CARD_LIST_EXPANSION[i]
		
	for c in the_card_list:
		add_card_to_collection(c, set_new)
		rarity = c.get_rarity()
		if not (rarity is Rarity.LEGENDARY or rarity is Rarity.BASIC_D):
			add_card_to_collection(c, set_new)

#Also gets how many new cards there are per section.
def get_owned_cards_spaced(cards_per_page: int, conds: list) -> (list, list, list):
	owned = COLLECTION.get_owned_cards(conds)
	current_indix = 1
	if owned == []:
		return [], [], []

		
	first_card = owned[0]
	ans = [owned[0]]

	def one_if_new(c: card.Card) -> int:
		return int(COLLECTION.is_new(c))
	
	def complete(l: list, current_indix: int):
		for i in range((cards_per_page - current_indix) % cards_per_page):
			l.append((0, Class.ANY, card.BLANK_CARD))
			current_indix += 1
		return current_indix
	
	if settings.TAVERN_MODE:
		first_class = first_card.get_tavern()
	else:
		first_class = first_card.get_grouped_class()

	quick_access = [(first_class, 0)]
	how_many_new = [one_if_new(owned[0])]

	categories = {}
	if settings.TAVERN_MODE:
		for b in [1,3,0,2]:
			for v in range(8):
				categories[(v, b)] = {"how_many_new": 0, "cards": []}
	else:
		for b in [True, False]:
			for cl in classes.Class:
				categories[(classes.CLASS_REPR[cl], b)] = {"how_many_new": 0, "cards": []}


	last_card = first_card
	
	for c in owned:
		if settings.TAVERN_MODE:
			tav = c.get_tavern()
			if tav is None:
				tav = 0
			else:
				cl_list = [tav]

			b = c.get_collectable_bg()
			entry = categories[(tav, b)]
			entry["how_many_new"] += one_if_new(c)
			entry["cards"].append((b, tav, c))

		else:
			cl_expanded_list = c.get_class_expanded()
			b = c.is_collectable()

			for cl in cl_expanded_list:
				entry = categories[(classes.CLASS_REPR[cl], b)]
				entry["how_many_new"] += one_if_new(c)
				entry["cards"].append((b, cl, c))

	ans = []
	how_many_new = []
	quick_access = []
	quick_access_indix = 0

	for key, item in categories.items():
		cat = categories[key]
		if cat["cards"] != []:

			if settings.TAVERN_MODE:
				cat["cards"].sort(key = lambda info: (info[0], info[1], info[2].extract_comparison_components())) # (0 if c.get_tavern() is None else c.get_tavern(), text_to_id(c.get_name()), text_to_id(c.get_key())))
			else:
				cat["cards"].sort(key = lambda info: (info[0], info[1], info[2].extract_comparison_components_without_class()))

			complete(cat["cards"], len(cat["cards"]))

			ans += cat["cards"]
			how_many_new.append(cat["how_many_new"])
			if settings.TAVERN_MODE:
				tav = 0 if key[0] is None else key[0]
				quick_access.append((tav, quick_access_indix))
			else:
				quick_access.append((classes.CLASS_GROUPING_DICT[classes.CLASS_DICT[key[0]]], quick_access_indix))

			quick_access_indix += len(cat["cards"])



		# if ((settings.TAVERN_MODE and last_card.get_tavern() == c.get_tavern())
		# 		or (not settings.TAVERN_MODE and (last_card.get_grouped_class() is c.get_grouped_class()))):
		# 	ans.append(c)
		# 	how_many_new[-1] += one_if_new(c)
		# else:
		# 	current_indix = complete(ans, current_indix)
		# 	ans.append(c)
		# 	if settings.TAVERN_MODE:
		# 		quick_access.append((c.get_tavern(), current_indix))
		# 	else:
		# 		quick_access.append((c.get_grouped_class()[0], current_indix))
		# 	how_many_new.append(one_if_new(c))
		# current_indix += 1

	# complete(ans, current_indix)
	return ans, quick_access, how_many_new

def get_referred_quick_access(spaced_list: list, pos: int) -> int:
	n = len(spaced_list)
	for i in range(1, n):
		if spaced_list[i][1] > pos:
			return i - 1
	return n - 1