from card import card

class Condition(object):
	def __init__(self, name:str, fun):
		self.name = name
		self._fun = fun

	def eval(self, c: card.Card) -> bool:
		return self._fun(c)

def remove_cond(name: str, cond_list: list):
	for cond in cond_list:
		if name == cond.name:
			cond_list.remove(cond)
			return
	raise ValueError("remove_cond(x, l): no function by name x in l")