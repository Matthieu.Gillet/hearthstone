import settings

from structures.types import Type
from structures.tribes import Tribe
from structures.groups import Group
from structures.runes import to_rune
from tools import normalization
from structures.expansions import CURRENT_CORE_YEAR, to_string

from card import card

def str_to_int_except(s: str) -> int:
	try:
		return int(s)
	except ValueError:
		if s == ".":
			return -1
		return None

def line_to_items(line: str) -> list:
	"""Convert a message in the form "item1;item2;item3" into a list of these
	items.
	
	==========
	line
		Line to be converted."""
	#print(txt)
	l = []
	s = ""
	sc_zone = False
	#Semi-colon zone, to know when to concatenate them
	#(fields with semi-colons in them are surrounded by double-quotes.)

	for i in line:
		if i == ";":
			if sc_zone:
				s += i
				#print("';' auto-replaced on '{}' card.".format(l[1]))
			else:
				l.append(s)
				s = ""

		elif i == '"':
			sc_zone = not sc_zone

		else:
			s+=i

	l.append(s)
	return l

def line_to_card(line: str) -> card.Card:
	l = line_to_items(line)

	if l[0]=="":
		return None

	key         	= l[0]
	expansion		= normalization.normalize_expansion(l[1])
	name			= l[2]
	tavern			= str_to_int_except(l[3])
	cost			= str_to_int_except(l[4])
	attack			= str_to_int_except(l[5])
	health			= str_to_int_except(l[6])
	runes			= [to_rune(i) for i in list(l[7])]
	card_type		= normalization.normalize_type(l[8])
	tribes			= [normalization.normalize_tribe(c) for c in l[9].split(", ")]
	card_class		= [normalization.normalize_class(c) for c in l[10].split(", ")]
	rarity			= normalization.normalize_rarity(l[11])
	description		= l[12]
	golden			= False
	tokens			= line_to_items(l[13])
	collectable		= l[14] == "1"
	collectable_bg	= int(l[15])
	groups			= [normalization.normalize_group(c) for c in l[16].split(", ")]
	more_subtypes	= [normalization.normalize_tribe(c) for c in l[17].split(", ")]
	core_rarity		= l[18]

	if settings.AUTO_LINEBREAK:
		description = description.replace(". ", ".§")
	if settings.REMOVE_LINEBREAK:
		description = description.replace(". ", "._")
		description = description.replace("§", "_")

	try:
		tokens.remove("")
	except ValueError:
		()
	try:
		tribes.remove(Tribe.NONE)
	except ValueError:
		()
	try:
		more_subtypes.remove(Tribe.NONE)
	except ValueError:
		()
	try:
		groups.remove(Group.NONE)
	except ValueError:
		()

	if settings.MORE_SUBTYPES and len(more_subtypes) > 0:
		tribes = more_subtypes

	# print(key, expansion, name, tavern, cost, attack, health, card_type, tribes, groups, card_class, rarity, description, runes, golden, collectable, collectable_bg, tokens)

	if card_type is Type.NONE:
		card_to_be_returned = card.Card(expansion, name, key, tavern, cost, groups, card_class,
			rarity, description, runes, golden, collectable, collectable_bg,
			tokens)
	elif card_type is Type.MINION:
		card_to_be_returned = card.Minion(expansion, name, key, tavern, cost, attack,
			health, tribes, groups, card_class, rarity, description, runes,
			golden, collectable, collectable_bg, tokens)
	elif card_type is Type.SPELL:
		card_to_be_returned = card.Spell(expansion, name, key, tavern, cost, tribes, groups,
			card_class, rarity, description, runes, golden, collectable, collectable_bg,
			tokens)
	elif card_type is Type.WEAPON:
		card_to_be_returned = card.Weapon(expansion, name, key, tavern, cost, attack,
			health, groups, card_class, rarity, description, runes, golden,
			collectable, collectable_bg, tokens)
	elif card_type is Type.HERO_POWER:
		card_to_be_returned = card.HeroPower(expansion, name, key, tavern, cost, groups,
			card_class, rarity, description, runes, golden,
			collectable, collectable_bg, tokens)
	elif card_type is Type.HERO:
		card_to_be_returned = card.Hero(expansion, name, key, tavern, cost, health, groups,
			card_class, rarity, description, runes, golden,
			collectable, collectable_bg, tokens)
	elif card_type is Type.LOCATION:
		card_to_be_returned = card.Location(expansion, name, key, tavern, cost, health, groups,
			card_class, rarity, description, runes, golden,
			collectable, collectable_bg, tokens)
	elif card_type is Type.PORTRAIT:
		card_to_be_returned = card.Portrait(expansion, name, key, tavern, cost, health, groups,
			card_class, rarity, description, runes, golden,
			collectable, collectable_bg, tokens)
	elif card_type is Type.TRINKET:
		card_to_be_returned = card.Trinket(expansion, name, key, tavern, cost, tribes, groups,
			card_class, rarity, description, runes, golden, collectable, collectable_bg,
			tokens)
	elif card_type is Type.GLYPH:
		card_to_be_returned = card.Glyph(expansion, name, key, tavern, cost, groups, card_class,
			rarity, description, runes, golden, collectable, collectable_bg,
			tokens)
	else:
		raise IndexError

	if not core_rarity in [""]:
		ans = [card_to_be_returned]
		
		# Abandoned system of multi-year core
		if core_rarity != "CoT":
			core_rarities = to_string(CURRENT_CORE_YEAR) + ":" + core_rarity
		else:
			core_rarities = core_rarity

		core_rarities_list = core_rarities.split(";")
		for core_info in core_rarities_list:
			core_info_list = core_info.split(":")
			try:
				core_year = core_info_list[0]

				is_core = True

				if core_year == "CoT":
					is_core = False
					core_rarity = card_to_be_returned.get_rarity()
				else:
					core_rarity = normalization.normalize_rarity(core_info_list[1])

				core_card = card_to_be_returned.copy()
				exp = normalization.normalize_expansion(core_year)

				if is_core and not exp is CURRENT_CORE_YEAR:
					core_card.set_collectable(False)
				
				# BG stats
				cb = core_card.get_collectable_bg()
				if cb == 1 or cb == 3:
					core_card.set_collectable_bg(cb - 1)
				core_card.set_tavern(None)

				# To avoid duplicates for only-Core cards
				if is_core and card_to_be_returned.get_expansion() is CURRENT_CORE_YEAR and exp is CURRENT_CORE_YEAR:
					continue

				core_card.set_expansion(exp)
				core_card.set_rarity(core_rarity)
				core_card.set_key(card_to_be_returned.get_key() + " (" + core_year + ")")

				ans.append(core_card)
			except IndexError:
				continue
		return ans
	else:
		return [card_to_be_returned]

def get_csv() -> str:
	file = open("cards.csv", 'r', encoding="utf-8") #latin-1 should work too
	reader = file.read()
	ans = normalization.normalize_csv_file(reader)
	file.close()
	return ans

def get_database() -> list:
	csv_text = get_csv()
	ans = []
	tmp = ""

	for i in csv_text:
		if i == "\n":
			c_list = []
			try:
				c_list = line_to_card(tmp)
			except IndexError:
				print("Error when building card", tmp)

			if not c_list is None:
				for c in c_list:
					if c is not None:
						ans.append(c)
			tmp = ""

		else:
			tmp += i

	ans.sort()
	return ans