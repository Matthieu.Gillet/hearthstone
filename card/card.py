import settings

from structures import expansions
from structures.expansions import Expansion
from structures.rarities import Rarity
from structures import classes
from structures.classes import Class
from structures import groups
from structures.groups import Group
from structures import runes
from structures.runes import Rune
from structures import tribes
from structures.tribes import Tribe
from structures.types import Type

from tools import text_tools
from tools.text_tools import Color
from tools import normalization


def bettercost(n: int) -> int:
	if n is None:
		return -1
	return n

class Card(object):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, 
		cost:int, Groups: list, Class:list, Rarity:Rarity, description:str, Runes: list, golden:bool,
		is_collectable: bool = True, collectable_bg: int = 0, related_cards: list = [], golden_version = None):
		#print("Building '{}' as a {}...".format(name, self.get_type()))
		self._expansion = Expansion
		self._name = name
		self._name_id = normalization.text_to_id(name)
		self._key = key
		self._key_id = normalization.text_to_id(key)
		self._filename = normalization.title_to_filename(key)
		self._tavern = tavern
		self._cost = cost
		self._groups = Groups
		self._class = Class
		self._rarity = Rarity
		self._description = description
		self._runes = Runes
		self._golden = golden
		self._is_collectable = is_collectable
		self._collectable_bg = collectable_bg
		self._related_cards = related_cards
		self._golden_version = golden_version

	def get_type(self) -> Type:
		return Type.NONE
	def get_expansion(self) -> Expansion:
		return self._expansion
	def get_name(self) -> str:
		return self._name
	def get_name_id(self) -> str:
		return self._name_id
	def get_key(self) -> str:
		return self._key
	def get_key_id(self) -> str:
		return self._key_id
	def get_tavern(self) -> int:
		return self._tavern
	def get_cost(self) -> int:
		return self._cost
	def get_class(self) -> list:
		return self._class
	def get_class_expanded(self) -> list:
		ans = []
		for cl in self._class:
			cl_expanded = classes.expand(cl)
			for cl_expanded_cl in cl_expanded:
				if not cl_expanded_cl in ans:
					ans.append(cl_expanded_cl)
		return ans
	def get_grouped_class(self) -> list:
		return [classes.group(c) for c in self._class]
	def get_groups(self) -> list:
		return self._groups
	def get_rarity(self) -> Rarity:
		return self._rarity
	def get_description(self) -> str:
		return self._description
	def get_runes(self) -> list:
		return self._runes
	def is_golden(self) -> bool:
		return self._golden or self._key[-8:] == " (Dorée)"
	def is_collectable(self) -> bool:
		return self._is_collectable
	def get_collectable_bg(self) -> int:
		return self._collectable_bg
	def is_collectable_bg(self) -> bool:
		return self._collectable_bg == 1
	def is_golden_collectable_bg(self) -> bool:
		return self._collectable_bg == 3
	def get_related_cards(self) -> list:
		return self._related_cards
	def get_golden_version(self):
		return self._golden_version

	def set_key(self, new_key: str):
		self._key = new_key
	def set_expansion(self, expansion: Expansion):
		self._expansion = expansion
	def set_tavern(self, tavern: int):
		self._tavern = tavern
	def set_rarity(self, rarity: Rarity):
		self._rarity = rarity
	def set_collectable(self, is_collectable: bool):
		self._is_collectable = is_collectable
	def set_collectable_bg(self, collectable_bg: int):
		self._collectable_bg = collectable_bg
	def set_golden(self, arg: bool):
		self._golden = arg

	def add_related_card(self, card):
		self._related_cards.append(card)
	def set_golden_version(self, card):
		self._golden_version = card

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		if self.get_runes() != []:
			runes_string = "["
			first_rune = True
			for rune in self.get_runes():
				if first_rune:
					first_rune = False
				else:
					runes_string += ", "
				runes_string += runes.to_string(rune)
			s += " " + runes_string + "]"

		return s + " — " + normalization.normalize_description(self.get_description())

	def extract_comparison_components(self):
		if settings.TAVERN_MODE:
			coin_cost = 0
			if not self.get_type() is Type.MINION:
				coin_cost = bettercost(self._cost)
			return (not self._collectable_bg, bettercost(self._tavern), coin_cost, normalization.text_to_id(self._name), normalization.text_to_id(self._key))
		else:
			try:
				return (not self._is_collectable, self._class[0], bettercost(self._cost), normalization.text_to_id(self._name), normalization.text_to_id(self._key), self._golden)
			except TypeError:
				print(self.__dict__)

	def extract_comparison_components_without_class(self):
		if settings.TAVERN_MODE:
			coin_cost = 0
			if not self.get_type() is Type.MINION:
				coin_cost = bettercost(self._cost)
			return (not self._collectable_bg, coin_cost, normalization.text_to_id(self._name), normalization.text_to_id(self._key))
		else:
			try:
				return (not self._is_collectable, bettercost(self._cost), normalization.text_to_id(self._name), normalization.text_to_id(self._key), self._golden)
			except TypeError:
				print(self.__dict__)

	def __eq__(self,other):
		return self.extract_comparison_components() == other.extract_comparison_components()
	def __ne__(self,other):
		return self.extract_comparison_components() != other.extract_comparison_components()
	def __lt__(self, other):
		return self.extract_comparison_components() < other.extract_comparison_components()
	def __le__(self, other):
		return self.extract_comparison_components() <= other.extract_comparison_components()
	def __gt__(self, other):
		return self.extract_comparison_components() > other.extract_comparison_components()
	def __ge__(self, other):
		return self.extract_comparison_components() >= other.extract_comparison_components()
	def __hash__(self):
		return hash(self.extract_comparison_components())


	def copy(self):
		return Card(self._expansion, self._name, self._key, self._tavern,
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Card(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes, False,
			self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Card(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes, True,
			self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)


class Debug(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int, Groups: list,
		Class:Class, Rarity:Rarity, description:str, Runes: list, golden:bool,
		collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

	def get_type(self) -> Type:
		return Type.DEBUG

	def copy(self):
		return Debug(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Debug(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Debug(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Minion(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int,
		attack:int, health: int, Tribes: list, Groups: list, Class:list, Rarity:Rarity,
		description:str, Runes: list, golden:bool, collectable: bool = True,
		collectable_bg: bool = False, related_cards: list = [], golden_version = None):

		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

		self.force_show_attack = attack == -1
		if self.force_show_attack:
			self._attack = None
		else:
			self._attack = attack

		self.force_show_health = health == -1
		if self.force_show_health:
			self._health = None
		else:
			self._health = health

		self._tribes = Tribes

	def get_type(self) -> Type:
		return Type.MINION
	def get_attack(self) -> int:
		return self._attack
	def get_health(self) -> int:
		return self._health
	def get_tribes(self) -> list:
		return self._tribes
	def get_groups(self) -> list:
		return self._groups

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		subtypes = self.get_tribes()
		for i in range(len(subtypes)):
			if i == 0:
				s += " " + tribes.to_string(subtypes[i])
			else:
				s += ", " + tribes.to_string(subtypes[i])
		s += " {}/{}".format(self.get_attack(), self.get_health())

		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		attack = self._attack
		if self.force_show_attack:
			attack = -1
		health = self._health
		if self.force_show_health:
			health = -1

		return Minion(self._expansion, self._name, self._key, self._tavern, self._cost,
			attack, health, self._tribes, self._groups, self._class, self._rarity,
			self._description, self._runes, self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		attack = self._attack
		if self.force_show_attack:
			attack = -1
		health = self._health
		if self.force_show_health:
			health = -1

		return Minion(self._expansion, self._name, self._key, self._tavern, self._cost,
			attack, health, self._tribes, self._groups, self._class, self._rarity,
			self._description, self._runes, False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		attack = self._attack
		if self.force_show_attack:
			attack = -1
		health = self._health
		if self.force_show_health:
			health = -1

		return Minion(self._expansion, self._name, self._key, self._tavern, self._cost,
			attack, health, self._tribes, self._groups, self._class, self._rarity,
			self._description, self._runes, True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)


class Spell(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int, Tribes: list,
		Groups: list, Class:Class, Rarity:Rarity, description:str, Runes: list, golden:bool,
		collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)
		self._tribes = Tribes

	def get_type(self) -> Type:
		return Type.SPELL
	def get_tribes(self) -> list:
		return self._tribes

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		subtypes = self.get_tribes()
		for i in range(len(subtypes)):
			if i == 0:
				s += " " + tribes.to_string(subtypes[i])
			else:
				s += ", " + tribes.to_string(subtypes[i])

		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		return Spell(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Spell(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Spell(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Weapon(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int,
		attack:int, durability: int, Groups: list, Class:Class, Rarity:Rarity, description:str, Runes: list,
		golden:bool, collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)
		self._attack = attack
		self._durability = durability
	
	def get_type(self):
		return Type.WEAPON
	def get_attack(self):
		return self._attack
	def get_durability(self):
		return self._durability

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		s += " {}/{}".format(self.get_attack(), self.get_durability())
		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		return Weapon(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._attack, self._durability, self._groups, self._class, self._rarity,
			self._description, self._runes, self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Weapon(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._attack, self._durability, self._groups, self._class, self._rarity,
			self._description, self._runes, False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Weapon(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._attack, self._durability, self._groups, self._class, self._rarity,
			self._description, self._runes, True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class HeroPower(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int, Groups:list,
		Class:Class, Rarity:Rarity, description:str, Runes: list, golden:bool,
		collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

	def get_type(self) -> Type:
		return Type.HERO_POWER

	def copy(self):
		return HeroPower(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return HeroPower(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return HeroPower(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Hero(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int,
		armor: int, Groups:list, Class:Class, Rarity:Rarity, description:str, Runes: list,
		golden:bool, collectable: bool = True, collectable_bg: bool	= False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)
		self._armor = armor
	
	def get_type(self):
		return Type.HERO
	def get_armor(self):
		return self._armor

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		s += " -/{}".format(self.get_armor())
		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		return Hero(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._armor, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Hero(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._armor, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Hero(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._armor, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)


class Location(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int,
		durability: int, Groups:list, Class:Class, Rarity:Rarity, description:str, Runes: list,
		golden:bool, collectable: bool = True, collectable_bg: bool	= False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)
		self._durability = durability
	
	def get_type(self):
		return Type.LOCATION
	def get_durability(self):
		return self._durability

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		s += " -/{}".format(self.get_durability())
		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		return Location(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._durability, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Location(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._durability, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Location(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._durability, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Portrait(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int,
		health: int, Groups: list, Class:Class, Rarity:Rarity, description:str, Runes: list,
		golden:bool, collectable: bool = True, collectable_bg: bool	= False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

		self.force_show_health = health == -1
		if self.force_show_health:
			self._health = None
		else:
			self._health = health
	
	def get_type(self):
		return Type.PORTRAIT
	def get_health(self):
		return self._health

	def __repr__(self):
		s = expansions.to_symbol(self.get_expansion()) + " "

		if self._cost is None:
			s += "() "
		else:
			s += "({}) ".format(self.get_cost())

		s += text_tools.colorize_rarity(self.get_name(), self.get_rarity())

		if self.is_golden():
			s += text_tools.colorize_text(" DORÉE !", Color.YELLOW, True)

		s += " -/{}".format(self.get_health())
		return s + " — " + normalization.normalize_description(self.get_description())

	def copy(self):
		return Portrait(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._health, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Portrait(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._health, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Portrait(self._expansion, self._name, self._key, self._tavern, self._cost,
			self._health, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Trinket(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int, Tribes: list, Groups:list,
		Class:Class, Rarity:Rarity, description:str, Runes: list, golden:bool,
		collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

		self._tribes = Tribes

	def get_type(self) -> Type:
		return Type.TRINKET
	def get_tribes(self) -> list:
		return self._tribes

	def copy(self):
		return Trinket(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Trinket(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Trinket(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._tribes, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)



class Glyph(Card):
	def __init__(self, Expansion:Expansion, name:str, key:str, tavern:int, cost:int, Groups:list,
		Class:Class, Rarity:Rarity, description:str, Runes: list, golden:bool,
		collectable: bool = True, collectable_bg: bool = False, related_cards: list = [], golden_version = None):
		Card.__init__(self, Expansion, name, key, tavern, cost, Groups, Class, Rarity,
			description, Runes, golden, collectable, collectable_bg, related_cards, golden_version)

	def get_type(self) -> Type:
		return Type.GLYPH

	def copy(self):
		return Glyph(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			self._golden, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def regular_copy(self):
		return Glyph(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			False, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

	def golden_copy(self):
		return Glyph(self._expansion, self._name, self._key, self._tavern, 
			self._cost, self._groups, self._class, self._rarity, self._description, self._runes,
			True, self._is_collectable, self._collectable_bg, self._related_cards.copy(), self._golden_version)

BLANK_CARD = Card(Expansion.ALL, "", "", None, None, [Group.NONE], [Class.ANY], Rarity.BASIC, "", [], False, False, False, [])