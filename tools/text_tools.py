import os
from enum import Enum

from structures.rarities import group, Rarity

class Color(Enum):
    BLACK   = 30
    RED     = 31
    GREEN   = 32
    YELLOW  = 33
    BLUE    = 34
    MAGENTA = 35
    CYAN    = 36
    WHITE   = 37

def colorize_text(text: str, color: Color, bold: bool = False) -> str:
    """Displays a colorized text when printed.
    
    Settings
    ----------
    text: str
        Text to be colorized.
    color: Color
        The color to be colorized in.
    bold: bool, default: False
        Should the text be embolden?"""
    
    if os.getenv('ANSI_COLORS_DISABLED') is None:
        if bold:
            return '\033[1m\033[%dm%s\033[0m' % (color.value, text)
        else:
            return '\033[%dm%s\033[0m' % (color.value, text)
    else:
        return text

def embolden_text(text: str) -> str:
    """Displays an emboldened text when printed.

    Settings
    ----------
    text: str
        Text to be emboldened."""
    return colorize_text(text, Color.WHITE, True)

def italicize_text(text: str) -> str:
    """Displays an italicized text when printed.

    Settings
    ----------
    text: str
        Text to be italicized."""
    return '\033[3m%s\033[0m' % (text)

def colorize_rarity(text: str, rarity: Rarity) -> str:
    """Displays a colorized text when printed, depending on a certain rarity.
    
    Settings
    ----------
    text: str
        Text to be colorized.
    rarity: rarity.Rarity
        The rarity of the attached text."""
    if rarity is Rarity.RARE:
        return colorize_text(text,Color.BLUE,True)
    if rarity is Rarity.EPIC:
        return colorize_text(text,Color.MAGENTA, True)
    if group(rarity) is Rarity.LEGENDARY:
        return colorize_text(text,Color.YELLOW, True)
    return text