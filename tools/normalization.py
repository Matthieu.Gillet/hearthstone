import re
import unicodedata

from tools import text_tools

from structures import expansions, classes, tribes, groups, rarities, types

def normalize_apostrophe(s: str) -> str:
	"""Replace apostrophes with normalized ones in a text message."""
	ans = ""

	for i in s:
		if i == "'":
			ans += "’"

		else:
			ans += i

	return ans

def normalize_expansion(expansion:str) -> expansions.Expansion:
	return expansions.EXPANSION_DICT[expansion]

def normalize_class(c: str) -> classes.Class:
	return classes.CLASS_DICT[c]

def normalize_group(c: str) -> groups.Group:
	return groups.GROUP_DICT[c]

def normalize_tribe(tribe: str) -> tribes.Tribe:
	return tribes.TRIBE_DICT[tribe]

def normalize_type(t: str) -> types.Type:
	return types.TYPE_DICT[t]

def normalize_rarity(rarity: str) -> rarities.Rarity:
	return rarities.RARITY_DICT[rarity]

def normalize_description(d: str) -> str:
	"""By inserting '$' stamps into a text, a text can be embolden
	in a much easier way."""

	# if d == "—":
	#     return ""
	ans = ""
	tmp_italic = ""
	tmp_bold = ""
	in_bold_field = False
	in_italic_field = False

	d = d.replace("_", " ")
	d = d.replace("§", " ")

	for i in d:
		if i == '$':
			in_bold_field = not in_bold_field


			if tmp_bold != "":
				len_b = len(tmp_bold)
				len_i = len(tmp_italic)
				if len_b < len_i:
					ans += text_tools.italicize_text(tmp_italic[:-len_b])
					ans += text_tools.embolden_text(text_tools.italicize_text(tmp_bold))
					tmp_italic = ""

				else:
					ans += text_tools.embolden_text(tmp_bold)

			tmp_bold = ""

		elif i == '^':
			in_italic_field = not in_italic_field

			if tmp_italic != "":
				len_b = len(tmp_bold)
				len_i = len(tmp_italic)
				if len_b > len_i:
					ans += text_tools.embolden_text(tmp_bold[:-len_i])
					ans += text_tools.embolden_text(text_tools.italicize_text(tmp_italic))
					tmp_bold = ""

				else:
					ans += text_tools.italicize_text(tmp_italic)
				
			tmp_italic = ""

		else:
			if in_bold_field:
				tmp_bold += i
			if in_italic_field:
				tmp_italic += i
			if not (in_bold_field or in_italic_field):
				ans += i

	return ans

def title_to_filename(title: str) -> str:
	"""Convert a title into a proper filename.
	
	==========
	title
		Title to be converted."""
	ans = ""
	for i in title:
		if i == "’":
			ans += "'"

		elif i in [":", "?"]:
			ans += "-"

		else:
			ans += i

	ans = ans.replace("…", "...")
	return ans

def title_to_tag(title: str) -> str:
	ans = ""
	for i in title:
		if i == ' ':
			ans += '_'

		elif i == '.':
			ans += '_'

		elif i in ['(', ')', ':']:
			ans += '-'

		elif i == '!':
			()

		else:
			ans += i

	return ans

def normalize_csv_file(text: str) -> str:
    i=0

    while text[i] != "\n": #Getting rid of the header
        i+=1

    return text[i+1:]
    # i+=1

    # rep=""

    # try:
    #     while True:
    #         a=txt[i]

    #         if a == ";":
    #             rep += "_"
    #         else:
    #             rep += a
            
    #         i+=1
    # except IndexError:
    #     return rep



#Getting rid of accents and capital letters, to sort strings in a better way.
def strip_accents(text):
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = re.sub(r'(Œ|œ)', 'oe', text)
    text = re.sub(r'(Æ|æ)', 'ae', text)
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError): # unicode is a default on python 3 
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)

def text_to_id(text):
    """
    Convert input text to id.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    text = strip_accents(text.lower())
    text = re.sub(r'[ ]+', '_', text)
    text = re.sub(r'[^0-9a-zA-Z_-]', '', text)
    return text