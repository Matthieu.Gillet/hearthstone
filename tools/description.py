from structures.types import Type

def letter_weight(i: str) -> int:
	"""Give a weight to a letter depending on the place it takes once written."""
	if i in ['$', '^']:
		return 0
	elif i in ['i', 'î', 'I', 'Î', 'j', 'J', 'l', '’', ',', ' ', '-', ':', ';', '(', ')', '.', '!', '_', ' ']:
		return 1
	else:
		o = ord(i)
		if i in ['m', '/'] or i in [str(n) for n in range(10)] or (o >= 65 and o <= 90):
			return 3
		else:
			return 2

def text_weight(text: str) -> int:
	"""Return the length of a text where the place the letter take matters."""
	ans = 0
	for i in text:
		ans += letter_weight(i)
	return ans

def letters_word(text: str, a: int) -> str:
	"""In a text, return the word containing the letter in position a."""
	#Complete the word to the left
	try:
		left_part = ""
		i = a - 1   #The letter at position a is not wanted twice.
		while text[i] != ' ':
			left_part = text[i] + left_part
			i -= 1
	except IndexError:
		left_part = text[:a+1]

	#Complete the word to the right
	try:
		right_part = ""
		i = a
		while text[i] != ' ':
			right_part += text[i]
			i += 1
	except IndexError:
		right_part = text[a:]

	return left_part + right_part
	
def stick_to_left(description: str, pos: int, tmp_word: str, in_a_keyword: bool) -> bool:
	"""Determine if the following word must be attached to the next one.
	This includes:
	- Statistics, i.e. words with a '/' in it: (example: 5/5)
	- Ponctuation, among ':', ';', '»', '!', '?'."""
	if in_a_keyword and tmp_word != "" and tmp_word[0] != "+":
		return True

	try:
		next_character = description[pos+1]

		if next_character in [':', ';', '»', '!', '?']:
			return True

		# if in_a_keyword and next_character in ['(']:
		# 	return True

		w = letters_word(description, pos+1)

		if tmp_word in ["Coûte", "niveau", "niveaux"]:
			return True
		if not '/' in w:
			return False
		if not '+' in w:
			return True

		return description[pos-1] != ','

	except IndexError:
		return False

def stick_to_right(last_character: str, description: str, pos: int) -> bool:
	"""Determine if the previous word must be attached to the next one.
	This includes:
	- Mere numbers (example: 3);
	- Mere numbers between parentheses (example: (1));
	- Punctuation sign '«';
	- The letter 'à', to avoid line breaks in '1 à 3 dégâts' for instance."""
	o = ord(last_character)

	w = letters_word(description, pos+1)
	w_strip = w.replace(".", "")
	w_strip = w_strip.replace(",", "")
	# print(w)
	return ((last_character in ['«', ')', 'à', 'À'])
		or ((o>=48 and o<=57) and not '/' in letters_word(description, pos-1))
		or w_strip in ["ATQ", "Attaque", "PV", "Vie"])

def get_local_cap(cap: int, number_of_lines: int, card_type: Type):
	if card_type is Type.SPELL or number_of_lines < 4:
		return cap
	elif not card_type is Type.WEAPON and number_of_lines == 4:
		return 0.82 * cap
	else:
		return 0.7 * cap

def description(d: str, card_type: Type = Type.NONE, font_size: int = 10) -> str:
	"""Take a text and return it with regular line breaks, without cutting
	words and when certain formulations are used."""
	if d[0] == "—":   #For cards with blank text
		return {"font_size": font_size, "description": ""}

	font_size_to_cap = {
		10: 52,
		9: 57,
		8: 63,
		7: 79
	}

	try:
		cap				= font_size_to_cap[font_size]
	except IndexError:
		cap				= font_size_to_cap[10]

	len_d				= len(d)
	ans					= ""
	tmp_line			= ""
	tmp_word			= ""
	current_keyword		= ""
	weight_line			= 0
	weight_word			= 0
	pos					= 0
	number_of_lines		= 1
	last_character		= ' '
	new_line			= True
	in_a_keyword		= False
	keywords_phase		= True								#Is it still the keywords phase at the beginning?
	keywords_separators	= ['.']
	soft_keywords_sep	= [',']
	semantic_symbols	= ['$', '^']
	spaces				= [' ']
	linebreakable_chars	= ['-']								#Characters that can be line-breaked right after
	keyword_symbol		= '$'
	italic_symbol		= '^'
	fcinlb				= [':']								#Finishing characters that imply no line break ("Battlecry:")
	keywords_end_text	= [":"]
	end_sentence_chars	= [".", "»", "!"]
	force_linebreaks	= ["§"]
	fake_keywords		= ["Dragage"]

	debug_ans = ""

	try:
		while True:
			i = d[pos]
			if number_of_lines >= 5 and font_size > 8:
				return description(d, card_type, font_size - 1)
			if number_of_lines > 5 and font_size > 7:
				return description(d, card_type, font_size - 1)

			if not i in semantic_symbols:
				if in_a_keyword:
					current_keyword += i
				else:
					keywords_phase = False
				
				in_a_new_keyword = new_line and in_a_keyword
				# print(in_a_keyword, in_a_new_keyword)

				before_an_italicised_portion = d[pos-1:pos+2] == ". ^"

				if not before_an_italicised_portion and not i in force_linebreaks and (in_a_new_keyword or (not i in spaces and not i in linebreakable_chars) or stick_to_left(d, pos, tmp_word, in_a_keyword) or stick_to_right(last_character, d, pos)):
					#There are too many letters on the line, so a line break is done.
					if not in_a_new_keyword and weight_line >= get_local_cap(cap, number_of_lines, card_type):
						weight_line = weight_word
						ans += tmp_line + '\n'
						debug_ans += "A"
						number_of_lines += 1
						tmp_line = ""
						new_line = True

					if not i in spaces:
						last_character = i

					# print('BB')
					i_weight = letter_weight(i)
					tmp_word += i
					weight_line += i_weight
					weight_word += i_weight
					
				else:
					# print("CC")
					local_cap = get_local_cap(cap, number_of_lines, card_type)

					if i in force_linebreaks or before_an_italicised_portion or weight_line >= local_cap:
						if new_line:
							if i in force_linebreaks:
								ans += tmp_word + '\n'
							else:
								ans += tmp_word + i + '\n'
						else:
							if tmp_line[-1] in linebreakable_chars:
								ans += tmp_line + tmp_word + '\n'
							elif i in linebreakable_chars:
								ans += tmp_line + ' ' + tmp_word + i + '\n'
							else:
								ans += tmp_line + ' ' + tmp_word + '\n'
						weight_line = 0
						weight_word = 0
						debug_ans += "B"
						number_of_lines += 1
						tmp_line = ""
						tmp_word = ""
						new_line = True

					else:
						weight_word += letter_weight(i)
						if new_line:
							tmp_line = tmp_word
							if i in linebreakable_chars:
								tmp_line += i
								weight_line += letter_weight(i)
							tmp_word = ""
							weight_word = 0
							new_line = False
						else:
							if not tmp_line[-1] in linebreakable_chars:
								tmp_line += ' '
								weight_line += letter_weight(' ')

							tmp_line += tmp_word

							if i in linebreakable_chars:
								tmp_line += i
								weight_line += letter_weight(i)

							tmp_word = ""
							weight_word = 0
					last_character = i

			elif i == keyword_symbol:
				# if in_a_keyword:
				# 	tmp_word+="</b>"
				# else:
				# 	tmp_word+="<b>"
				if in_a_keyword:
					# print(ans + "|" + "\n" + tmp_line + "|" + "\n" + "-----")
					# print("TMP LINE IS", tmp_line)
					# print("LAST IS <<" + tmp_line[-1] if tmp_line != "" else "" + ">>")
					last_character_is_a_dot = (tmp_line != "" and tmp_line[-1] in end_sentence_chars) or (tmp_line == "" and ans != "" and ans[-1] == end_sentence_chars)

					if keywords_phase:
						try:
							if d[pos+1] in keywords_separators and d[pos+2] in spaces and not current_keyword in fake_keywords:
								new_line = True
								endline = '\n'
								if len_d == pos + 2:
									endline = '' # no linebreak if it's the end of the word
								else:
									debug_ans += "D"
									number_of_lines += 1

								if tmp_line == "":
									ans += tmp_word + endline
								else:
									ans += tmp_line + ' ' + tmp_word + endline

								tmp_line = ""
								tmp_word = ""
								weight_line = 0
								weight_word = 0
								pos += 2	#Neither the dot nor the comma nor the following space are taken into account, hence the 2.
							
							elif d[pos+1] in soft_keywords_sep and d[pos+2] in spaces and not current_keyword in fake_keywords:
								
								next_letter = d[pos+1]
								# print("AAA: '{}', '{}', '{}'".format(i, next_letter, d[pos+2]))
								if tmp_line == "":
									tmp_line += tmp_word + next_letter
								else:
									tmp_line += ' ' + tmp_word + next_letter
									weight_line += letter_weight(' ')

								tmp_word = ""
								weight_line += letter_weight(next_letter) + letter_weight(d[pos+2])
								weight_word = 0
								pos += 2
								new_line = False
							else:
								keywords_phase = False
						except IndexError:
							()

					else:
						is_tmp_word_in_keywords_end_text = False
						for kw in keywords_end_text:
							# print(kw, tmp_word)
							if kw in tmp_word:
								is_tmp_word_in_keywords_end_text = True
								break

						if is_tmp_word_in_keywords_end_text:
							if last_character_is_a_dot:
								# print("PING")
								ans += tmp_line + '\n'
								number_of_lines += 1
								weight_line = weight_word
								tmp_line = tmp_word
								# new_line = True

								tmp_word = ""
								weight_word = 0
					current_keyword = ""
				in_a_keyword = not in_a_keyword
			# print(weight_word,weight_line,last_character,stick_to_left(d,pos,tmp_word,in_a_keyword),stick_to_right(last_character,d,pos),"///" ,tmp_word, "///", tmp_line, "|\n|",d[:pos+1],"|\n|",ans,"|\n")
			pos += 1

	except IndexError:
		if number_of_lines >= 5 and font_size > 8:
			return description(d, card_type, font_size - 1)
		if number_of_lines > 5 and font_size > 7:
			return description(d, card_type, font_size - 1)


		debug_beginning = ""
		debug_end = ""
		# debug_beginning = str(number_of_lines) + "L" + " • " + str(font_size) + "F" + " • " + str(len_d) + " • " + debug_ans + "R" + "\n"
		# debug_end = "\n"
		if new_line:
			ans = debug_beginning + ans + tmp_word + debug_end
			ans = ans.replace("_", " ")
			return {"font_size": font_size, "description": ans}
		elif tmp_line[-1] in linebreakable_chars:
			ans = debug_beginning + ans + tmp_line + tmp_word + debug_end
			ans = ans.replace("_", " ")
			return {"font_size": font_size, "description": ans}
		else:
			ans = debug_beginning + ans + tmp_line + ' ' + tmp_word + debug_end
			ans = ans.replace("_", " ")
			return {"font_size": font_size, "description": ans}