import re

file = open("cards_tmp.csv", 'r', encoding="utf-8")
reader = file.read()
file.close()

reader = reader.replace("\t", ";")
reader = re.sub(";[^;\n]*;[^;\n]*;[^;\n]*;[^;\n]*;[^;\n]*;[^;\n]*\n", "\n", reader)
reader = re.sub("\n;[^\n]*", "", reader)
reader = re.sub("UPCOMING", "PiP", reader)

file2 = open("cards.csv", 'w+', encoding="utf-8")
file2.write(reader)
print("Chopped!")