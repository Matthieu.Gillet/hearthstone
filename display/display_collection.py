from user_settings import big_card_back_path
import settings

from structures import classes
from structures.classes import Class
from structures.items import Item
from structures.events import Event

from card import card
from card import card_list
from card.card import BLANK_CARD
from card import deck
from card.deck import DECK_LIST
from card import collection
from card import condition
from card.condition import Condition

from display import window
from display import display_card
from display import display_deck

ASSETS_PATH = "assets/"
CARD_ASSETS_PATH = "assets/card/"
COLLECTION_ASSETS_PATH = "assets/collection/"

cards_per_page = 8
current_cost_filter = None
global current_page_class
current_page_class = [None]

def x_class_access(i: int) -> int:
	return 40 + 37 * i
def x_cost(i: int) -> int:
	return 950 + 35 * i
def y_top(i: int) -> int:
	return 40

def x_card(i: int) -> int:
	return 100 + int(275 * (i % 4))
def y_card(i: int) -> int:
	return 75 + 310 * (i // 4)

def get_cost_eq_cond(n: str) -> Condition:
	def cond_fun(c: card.Card) -> bool:
		return c.get_cost() == n
	return Condition("cost", cond_fun)

def get_cost_geq_cond(n: str) -> Condition:
	def cond_fun(c: card.Card) -> bool:
		return card.bettercost(c.get_cost()) >= n
	return Condition("cost", cond_fun)

def get_golden_cond() -> Condition:
	def cond_fun(c: card.Card) -> bool:
		return c.is_golden()
	return Condition("golden", cond_fun)

def get_new_cond() -> Condition:
	def cond_fun(c: card.Card) -> bool:
		return collection.COLLECTION.is_new(c)
	return Condition("new", cond_fun)

def display_collection(w: window.Window, page, building_deck: deck.Deck, cache, conds: list, back):
	#page can be an int, or a card.
	w.clear()

	def cond_fun(c: card.Card) -> bool:
		if building_deck is None:
			return True
		else:
			for i in c.get_class():
				if i in [building_deck.get_class(), Class.ANY]:
					return True
			return False
	building_deck_class_cond = Condition("building_deck_class", cond_fun)

	cache_was_none = cache is None
	if cache_was_none:
		cache = collection.get_owned_cards_spaced(
			cards_per_page, conds + [building_deck_class_cond])
		current_triple_cards_dict_undefined = True

	owned, quick_access, how_many_new = cache
	len_owned = len(owned)

	if (cache_was_none or current_page_class[0] is None) and type(page) is int:
		try:
			current_page_class[0] = quick_access[0][0]

			for access in quick_access:
				if access[1] <= page * cards_per_page:
					current_page_class[0] = access[0]
				else:
					break
		except IndexError:
			current_page_class[0] = None

	w.add_image((0, Item.BACKGROUND), COLLECTION_ASSETS_PATH + "background.png", 0, 0, "nw")
	w.add_text("Collection", 620, 40, ("Belwe Bd BT", 35), "black", "center", "center", "collection-headline")
	
	w.add_image((0, Item.BACK_BUTTON), ASSETS_PATH + "back.png", 0, 701, "sw", "back_button")
	w.add_tag("back_button", Event.CLICK, back)

	def jump_to_page(page: int):
		# Changing the page
		if not settings.TAVERN_MODE:
			current_card_number = page * cards_per_page

			for cl, card_number in quick_access:
				if card_number > current_card_number:
					break
				current_page_class[0] = cl

		display_collection(w, page, building_deck, cache, conds, back)
	
	if owned == []:
		w.add_text("Aucun résultat.", 620, 300, ("Belwe Bd BT", 25), "#635645", "center", "center", "no-cards")
		current_cards_dict = {0: 0}
		page = 0

	else:
		if type(page) is not int:
			reference_card = page
			page = 0
			i = 0
			found = False
			reference_items = (reference_card.is_collectable(), current_page_class[0], reference_card.extract_comparison_components_without_class())

			while i < len_owned and reference_items >= (owned[i][0], owned[i][1], owned[i][2].extract_comparison_components_without_class()):
			
				j = i + cards_per_page - 1
				while owned[j][2] is BLANK_CARD:
					j -= 1

				if reference_items <= (owned[j][0], owned[j][1], owned[j][2].extract_comparison_components_without_class()):
					break

				page += 1
				i += cards_per_page
			
			if i > len_owned:
				page = 0
				i = 0

			current_page_class[0] = owned[i][1]

		elif len_owned <= cards_per_page * page:
			#In case all cards from the last page have been disenchanted, for instance.
			jump_to_page(page - 1)
			return


		w.add_text("Page {}".format(page + 1), 620, 681, ("Belwe Bd BT", 20), "#635645", "center", "center", "page-number")

		##Card management
		first_card_indix = page * cards_per_page
		last_page = False
		current_cards_dict = {}
		current_triple_cards_dict = {}
		related_decks_dict = {}

		x_triple = 915
		y_triple = 300

		def disenchant(i: int):
			def f(event):
				c = current_cards_dict[i]
				collection.COLLECTION.disenchant(c)
				how_many = collection.COLLECTION.get_how_many(c)

				if how_many > 1:
					w.delete_text("number_possesser-{}".format(i))
					w.add_text("x{}".format(how_many), x_card(1.5) + 102,
						0.5 * y_card(0) + 0.5 * y_card(cards_per_page - 1) + 280, ("Belwe Bd BT", 22),
						"#635645", "center", "center", "number_possesser-{}".format(i))
				elif how_many == 1:
					w.delete_text("number_possesser-{}".format(i))
					w.delete_image((i, Item.CARD_BACK))
					w.delete_image((i, Item.NUMBER_POSSESSER))
				else:
					display_collection(w, page, building_deck, None, conds, back)
			return f

		def display_related_cards(i: int, triple: bool = False):
			x_pos = x_card(1.5)
			y_pos = 0.5 * y_card(0) + 0.5 * y_card(cards_per_page - 1)

			# Removing previous images when toggling triple
			if settings.TAVERN_MODE:
				other_c = current_triple_cards_dict[i]
				if triple:
					other_c = current_cards_dict[i]

				if not other_c is None:
					related_cards_to_delete = card_list.get_related_cards_recursive(other_c)
					if related_cards_to_delete != []:
						try:
							w.delete_card(related_cards_to_delete[0], cards_per_page + 1)
						except KeyError:
							()

				if related_decks_dict != {}:
					related_deck = related_decks_dict.pop(0)
					display_deck.delete_deck(w, related_deck)


			c = current_cards_dict[i]
			if triple:
				c = current_triple_cards_dict[i]

			related_cards = card_list.get_related_cards_recursive(c)
			if related_cards != []:
				token_tag = "inspected_card-token"
				if len(related_cards) == 1:
					()
					display_card.add_card_to_window(w, related_cards[0], x_card(0.6), y_pos, cards_per_page + 1,
						token_tag, settings.TAVERN_MODE, current_page_class[0])
					# print(w.__dict__)
				else:
					if current_page_class[0] is None:
						related_deck = deck.card_list_to_deck(related_cards, ordered = False, add_to_deck_list = False)
					else:
						related_deck = deck.card_list_to_deck(related_cards, Class = current_page_class[0], ordered = False, add_to_deck_list = False)
					related_decks_dict[0] = related_deck
					display_deck.display_hitboxes_in(w, related_deck, 450, 350 - 10 * len(related_cards))

		def toggle_card_version(i: int, base_to_triple: bool = True):
			x_pos = x_card(1.5)
			y_pos = 0.5 * y_card(0) + 0.5 * y_card(cards_per_page - 1)

			tag = "inspected_card"
			triple_tag = "inspected_card-triple"
			if base_to_triple:
				try:
					w.delete_card_text(current_cards_dict[i], i)
				except KeyError:
					()
				display_card.add_card_to_window(w, current_triple_cards_dict[i], x_pos, y_pos, i, triple_tag, settings.TAVERN_MODE, current_page_class[0])
				display_related_cards(i, True)
			else:
				try:
					w.delete_card_text(current_triple_cards_dict[i], i)
				except KeyError:
					()
				display_card.add_card_to_window(w, current_cards_dict[i], x_pos, y_pos, i, tag, settings.TAVERN_MODE, current_page_class[0])
				display_related_cards(i, False)

		def trigger_triple(i: int):
			def f(event):
				w.add_image((0, Item.TRIPLE), COLLECTION_ASSETS_PATH + "triple.png", x_triple, y_triple, "center", "untrigger-triple")
				toggle_card_version(i, True)
			return f

		def untrigger_triple(i: int):
			def f(event):
				w.add_image((0, Item.TRIPLE), COLLECTION_ASSETS_PATH + "triple.png", x_triple, y_triple, "center", "trigger-triple")
				toggle_card_version(i, False)
			return f

		def click_on_card(i: int):
			def f(event):
				if not building_deck is None:
					building_deck.add_card(current_cards_dict[i])
					display_deck.display_full(w, building_deck, 1364, 0, 0)
			return f

		def right_click_on_card(i: int):
			def f(event):
				c = current_cards_dict[i]
				how_many = collection.COLLECTION.get_how_many(c)
				x_pos = x_card(1.5)
				y_pos = 0.5 * y_card(0) + 0.5 * y_card(cards_per_page - 1)

				w.delete_card_text(c, i)
				if how_many > 1:
					w.delete_text("number_possesser-{}".format(i))

				path_blur = COLLECTION_ASSETS_PATH + "blur.png"
				w.add_image((0, Item.BLUR), path_blur, 0, 0, "nw", "blur")
				w.add_tag("blur", Event.CLICK, back_to_collection)
				
				if settings.TAVERN_MODE:
					triple_version = c.get_golden_version()
					current_triple_cards_dict[i] = triple_version
					if not triple_version is None:
						w.add_image((0, Item.TRIPLE), COLLECTION_ASSETS_PATH + "triple.png", x_triple, y_triple, "center", "trigger-triple")
						w.add_tag("trigger-triple", Event.CLICK, trigger_triple(i))
						w.add_tag("untrigger-triple", Event.CLICK, untrigger_triple(i))

				if how_many > 1:
					w.add_image((i, Item.CARD_BACK), big_card_back_path, x_pos - 20, y_pos - 20, "nw")
					w.add_image((i, Item.NUMBER_POSSESSER), COLLECTION_ASSETS_PATH + "number_possesser.png",
							x_pos + 102, y_pos + 280, "center", "np-{}".format(i))
					w.add_text("x{}".format(how_many), x_pos + 102, y_pos + 280, ("Belwe Bd BT", 22),
						"#635645", "center", "center", "number_possesser-{}".format(i))
				
				tag = "inspected_card"
				display_card.add_card_to_window(w, c, x_pos, y_pos, i, tag, settings.TAVERN_MODE, current_page_class[0])

				w.add_image((i, Item.DISENCHANTMENT_BUTTON), COLLECTION_ASSETS_PATH + "disenchant.png", 
					x_pos + 400, y_pos + 140, "center", "disenchant")
				w.add_tag("disenchant", Event.CLICK, disenchant(i))

				display_related_cards(i)
			return f

		def hover_over_card(i: int):
			def f(event):
				c = current_cards_dict[i]
				tag = "hovered-collection_card-{}".format(i)
				quick_access_number = collection.get_referred_quick_access(quick_access, i + cards_per_page * page)

				w.delete_card(c, i)

				if collection.COLLECTION.is_new(c):
					collection.COLLECTION.set_new(c, False)
					classes = c.get_class_expanded().copy()
					if len(classes) > 1:
						qa_index = 0
						for cl, quick_access_cl in quick_access:
							if cl in classes:
								how_many_new[qa_index] -= 1
								w.delete_text("text-class_access-{}".format(qa_index))

								if how_many_new[qa_index] != 0:
									w.add_outline(how_many_new[qa_index],
										x_class_access(qa_index) + 10,
										y_top(qa_index) + 10, ("Belwe Bd BT", 10), "red", "white", "center", "center",
										"text-class_access-{}".format(qa_index))

								classes.remove(cl)
								if classes == []:
									break

							qa_index += 1
					else:
						how_many_new[quick_access_number] -= 1
						w.delete_text("text-class_access-{}".format(quick_access_number))

						if how_many_new[quick_access_number] != 0:
							w.add_outline(how_many_new[quick_access_number],
								x_class_access(quick_access_number) + 10,
								y_top(quick_access_number) + 10, ("Belwe Bd BT", 10), "red", "white", "center", "center",
								"text-class_access-{}".format(quick_access_number))

					w.delete_text("new-{}".format(i))
					w.delete_image((i, Item.HOVER_LIGHT))
				
				display_card.add_card_to_window(w, c, x_card(i), y_card(i), i, tag, settings.TAVERN_MODE, current_page_class[0])
				w.add_tag(tag, Event.CLICK, click_on_card(i))
				w.add_tag(window.get_card_text_tag(c, i), Event.CLICK, click_on_card(i))
				w.add_tag(tag, Event.RIGHT_CLICK, right_click_on_card(i))
				w.add_tag(window.get_card_text_tag(c, i), Event.RIGHT_CLICK, right_click_on_card(i))
			return f


		for i in range(cards_per_page):
			try:
				c = owned[first_card_indix + i][2]
				if c is BLANK_CARD:
					raise IndexError
			except IndexError:
				break
			how_many = collection.COLLECTION.get_how_many(c)
			current_cards_dict[i] = c
			current_triple_cards_dict[i] = None

			def back_to_collection(event):
				jump_to_page_event(page)(event)

			if how_many > 1:
				w.add_image((i, Item.NUMBER_POSSESSER), COLLECTION_ASSETS_PATH + "number_possesser.png",
						x_card(i) + 102, y_card(i) + 280, "center", "np-{}".format(i))
				w.add_text("x{}".format(how_many), x_card(i) + 102, y_card(i) + 280, ("Belwe Bd BT", 22),
					"#635645", "center", "center", "number_possesser-{}".format(i))
			
			if collection.COLLECTION.is_new(c):
				w.add_image((i, Item.HOVER_LIGHT), COLLECTION_ASSETS_PATH + "hover-New.png", 
					x_card(i) + 102, y_card(i) + 140, "center")
				w.add_outline("NOUVEAU !", x_card(i) + 102, y_card(i) + 285, ("Belwe Bd BT", 22),
					"white", "#37D3CB", "center", "center", "new-{}".format(i))

			tag = "collection_card-{}".format(i)
			display_card.add_card_to_window(w, c, x_card(i), y_card(i), i, tag, settings.TAVERN_MODE, current_page_class[0])
			w.add_tag(tag, Event.HOVER, hover_over_card(i))

		##Little icons to jump from a class to another
		def jump_to_page_event(page: int):
			def f(event):
				jump_to_page(page)
			return f

		for i in range(len(quick_access)):
			c, number = quick_access[i]

			if settings.TAVERN_MODE:
				path = CARD_ASSETS_PATH + "tavern-small-{}.png".format(c)
			else:
				path = CARD_ASSETS_PATH + "class-{}.png".format(classes.to_string(classes.group(c)))

			tag = "class_access-" + str(i)
			text_tag = "text-" + tag
			w.add_image((i, Item.CLASS_ACCESS), path, x_class_access(i), y_top(i), "center", tag)
			if how_many_new[i] != 0:
				w.add_outline(how_many_new[i], x_class_access(i) + 10, y_top(i) + 10,
					("Belwe Bd BT", 10), "red", "white", "center", "center", text_tag)

			w.add_tag(tag, Event.CLICK, jump_to_page_event(number // cards_per_page))
			w.add_tag(text_tag, Event.CLICK, jump_to_page_event(number // cards_per_page))

		##Arrows
		try:
			owned[first_card_indix + cards_per_page]
		except IndexError:
			last_page = True
		
		def move_left(event):
			jump_to_page_event(page - 1)(event)
		
		def move_right(event):
			jump_to_page_event(page + 1)(event)

		if first_card_indix != 0:
			w.add_image((0, Item.LEFT_ARROW), COLLECTION_ASSETS_PATH + "left_arrow.png", 
				0, 350, "w", "left_arrow")
			w.add_tag("left_arrow", Event.LEFT_ARROW, move_left)
			w.add_tag("left_arrow", Event.CLICK, move_left)
		
		if not last_page:
			w.add_image((0, Item.RIGHT_ARROW), COLLECTION_ASSETS_PATH + "right_arrow.png", 
				1225, 350, "e", "right_arrow")
			w.add_tag("right_arrow", Event.CLICK, move_right)
			w.add_tag("right_arrow", Event.RIGHT_ARROW, move_right)

	##Little mana icons to filter by cost
	def add_cost_filter(i: int, fun):
		def f(event):
			try:
				condition.remove_cond("cost", conds)
			except ValueError:
				()
			global current_cost_filter
			if current_cost_filter == i:
				current_cost_filter = None
			else:
				current_cost_filter = i
				conds.append(fun)

			display_collection(w, current_cards_dict[0], building_deck, None, conds, back)
		return f

	if not settings.TAVERN_MODE:
		for i in range(8):
			tag = "cost-filter-" + str(i)
			w.add_image((i, Item.COST_FILTER), COLLECTION_ASSETS_PATH + "cost-filter.png",
				x_cost(i), y_top(i), "center", tag)
			if i == 7:
				w.add_outline("7+", x_cost(i), y_top(i) - 3, ("Belwe Bd BT", 22), "white", "black",
					"center", "center", tag)
				w.add_tag(tag, Event.CLICK, add_cost_filter(i, get_cost_geq_cond(i)))
			else:
				w.add_outline(i, x_cost(i), y_top(i) - 3, ("Belwe Bd BT", 25), "white", "black",
					"center", "center", tag)
				w.add_tag(tag, Event.CLICK, add_cost_filter(i, get_cost_eq_cond(i)))

	def toggle_golden_only(event):
		try:
			condition.remove_cond("golden", conds)
		except ValueError:
			conds.append(get_golden_cond())
		display_collection(w, current_cards_dict[0], building_deck, None, conds, back)

	icon_shift = 0
	if settings.TAVERN_MODE:
		icon_shift = 8
	tag = "toggle-golden-only"
	w.add_image((0, Item.GOLDEN_FILTER), CARD_ASSETS_PATH + "golden_star.png", x_cost(-1 + icon_shift), y_top(-1 + icon_shift), "center", tag)
	w.add_tag(tag, Event.CLICK, toggle_golden_only)

	def toggle_new_only(event):
		try:
			condition.remove_cond("new", conds)
		except ValueError:
			conds.append(get_new_cond())
		display_collection(w, current_cards_dict[0], building_deck, None, conds, back)

	tag = "toggle-new-only"
	w.add_outline('!', x_cost(-2 + icon_shift), y_top(-2 + icon_shift), ("Belwe Bd BT", 25), "red", "white", "center", "center", tag)
	w.add_tag(tag, Event.CLICK, toggle_new_only)

	def view_mass_disenchant_summary(event):
		collection.COLLECTION.disenchant_duplicates()
		display_collection(w, page, building_deck, None, conds, back)

	def view_disenchant_all_summary(event):
		collection.COLLECTION.disenchant_all()
		display_collection(w, page, building_deck, None, conds, back)

	w.add_text("Dust: {}".format(collection.COLLECTION.get_dust()),
		1200, 670, ("Belwe Bd BT", 15), "#635645", "e", "e", "dust-counter")
	w.add_text("Désenchantement de masse", 1200, 690, ("Belwe Bd BT", 15), "#635645", "e", "e", "mass_disenchant_button")
	w.add_tag("mass_disenchant_button", Event.CLICK, view_mass_disenchant_summary)
	w.add_tag("mass_disenchant_button", Event.RIGHT_CLICK, view_disenchant_all_summary)

	##Deck management
	def click_on_deck(d: deck.Deck):
		def f(event):
			display_collection(w, current_cards_dict[0], d, None, conds, back)
		return f

	def click_on_building_deck(event):
		display_collection(w, current_cards_dict[0], None, None, conds, back)

	def newdeck(event):
		()

	if building_deck is None:
		i=0
		try:
			while True:
				y_deck = 78 * i
				display_deck.display_miniature(w, DECK_LIST[i], 1364, y_deck, i, "deck-{}".format(i))
				w.add_tag("deck-{}".format(i), Event.CLICK, click_on_deck(DECK_LIST[i]))
				i+=1
		except IndexError:
			w.add_image((i, Item.DECK_HOLDER), CARD_ASSETS_PATH + "deck_holder.png", 1364, y_deck + 2, "ne", "newdeck")
			w.add_text("Nouveau\ndeck", 1295, 41 + y_deck, ("Belwe Bd BT", 18), 
				"#635645", "center", "center", "newdeck")
			w.add_tag("newdeck", Event.CLICK, newdeck)
	else:
		display_deck.display_full(w, building_deck, 1364, 0, 0)
		w.add_tag("deck", Event.CLICK, click_on_building_deck)