import settings

from structures import classes
from structures import expansions
from structures import rarities
from structures.groups import Group
from structures.rarities import Rarity
from structures import tribes
from structures.tribes import Tribe
from structures import types
from structures.types import Type
from structures import runes
from structures.items import Item
from structures.classes import Class

from tools import normalization
from tools import description

from card import card

from display import window

CARD_ASSETS_PATH = "assets/card/"
CARD_ART_PATH = "assets/card/art/"

def add_background(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	if c.is_golden():
		path = CARD_ASSETS_PATH + "background_golden.png"
	else:
		path = CARD_ASSETS_PATH + "background.png"
	w.add_image((number, Item.CARD_BACKGROUND), path, dx, dy, "nw", tag)

def add_carpet(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str, class_context: Class = None):
	cl = c.get_class()
	number_classes = len(cl)

	if number_classes == 1:
		classes_expanded = classes.expand(cl[0])
	else:
		classes_expanded = cl

	if len(classes_expanded) >= 3:
		if not class_context is None:
			if class_context in classes_expanded:
				cl = [class_context]
				number_classes = 1

	y_top_carpet = 15
	y_bottom_carpet = 260

	if c.get_type() in [Type.MINION, Type.SPELL] and len(c.get_tribes()) >= 2:
		y_bottom_carpet = 269

	if len(cl) > 1:
		first_class_grouped = classes.group(cl[0])
		second_class_grouped = classes.group(cl[1])
		path = CARD_ASSETS_PATH + "carpet/carpet-{}-up-left.png".format(classes.to_string(first_class_grouped))
		w.add_image((number, Item.CARPET, "left"), path, dx + 89, dy + y_top_carpet, "center", tag)
		path = CARD_ASSETS_PATH + "carpet/carpet-{}-up-right.png".format(classes.to_string(second_class_grouped))
		w.add_image((number, Item.CARPET, "right"), path, dx + 116, dy + y_top_carpet, "center", tag)
		path = CARD_ASSETS_PATH + "carpet/carpet-middle-up.png"
		w.add_image((number, Item.CARPET, "middle"), path, dx + 102, dy + y_top_carpet, "center", tag)

		path = CARD_ASSETS_PATH + "carpet/carpet-{}-down-left.png".format(classes.to_string(first_class_grouped))
		w.add_image((number, Item.CARPET2, "left"), path, dx + 89, dy + y_bottom_carpet, "center", tag)
		path = CARD_ASSETS_PATH + "carpet/carpet-{}-down-right.png".format(classes.to_string(second_class_grouped))
		w.add_image((number, Item.CARPET2, "right"), path, dx + 116, dy + y_bottom_carpet, "center", tag)
		path = CARD_ASSETS_PATH + "carpet/carpet-middle-down.png"
		w.add_image((number, Item.CARPET2, "middle"), path, dx + 102, dy + y_bottom_carpet, "center", tag)
	else:
		class_grouped = classes.group(cl[0])
		path = CARD_ASSETS_PATH + "carpet/carpet-{}-up.png".format(classes.to_string(class_grouped))
		w.add_image((number, Item.CARPET), path, dx + 102, dy + y_top_carpet, "center", tag)

		path = CARD_ASSETS_PATH + "carpet/carpet-{}-down.png".format(classes.to_string(class_grouped))
		w.add_image((number, Item.CARPET2), path, dx + 102, dy + y_bottom_carpet, "center", tag)

def add_artwork(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	path = CARD_ART_PATH + "{}.png".format(normalization.title_to_filename(c.get_key()))
	parenthese_pos = path.find("(")
	general_path = None

	image_found = False

	w.add_image((number, Item.ARTWORK), CARD_ART_PATH + "_default.png", dx + 102, dy + 60, "center", tag)

	while parenthese_pos != -1:
		general_path = path[:parenthese_pos - 1] + ".png"
		if not general_path is None:
			image_found = w.add_image((number, Item.ARTWORK), general_path, dx + 102, dy + 60, "center", tag) or image_found

		parenthese_pos = path.find("(", parenthese_pos + 1)

	return w.add_image((number, Item.ARTWORK), path, dx + 102, dy + 60, "center", tag) or image_found

def add_frame(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	path = CARD_ASSETS_PATH + "frame.png"
	w.add_image((number, Item.FRAME), path, dx + 102, dy + 60, "center", tag)

def add_class(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str, class_context: Class = None):
	cl = c.get_class()
	number_classes = len(cl)

	if number_classes == 1:
		classes_expanded = classes.expand(cl[0])
	else:
		classes_expanded = cl

	if len(classes_expanded) >= 3:
		path = CARD_ASSETS_PATH + "sidebanner_multiclass.png"
		w.add_image((number, Item.SIDE_BANNER), path, dx + 165, dy + 75, "nw", tag)
		if not class_context is None:
			if class_context in classes_expanded:
				cl = [class_context]
				number_classes = 1

	for i in range(number_classes):
		path = CARD_ASSETS_PATH + "class-{}.png".format(classes.to_string(classes.group(cl[i])))
		w.add_image((number, Item.CLASS, i), path, dx + 164, dy + 5 + 35*i, "nw", tag)


def add_minion_type_banner(w: window.Window, c: card.Minion, dx: int, dy: int, number: int, tag: str):
	#Subtypes
	subtypes = c.get_tribes()
	if subtypes != []:
		number_subtypes = len(subtypes)
		path = CARD_ASSETS_PATH + "minion_type_banner"
		
		if number_subtypes != 1:
			path += str(number_subtypes)

		if c.is_golden():
			path += "_golden"

		w.add_image((number, Item.TRIBE_BANNER), path + ".png", dx + 102, dy + 251, "center", tag)

def add_spell_school_banner(w: window.Window, c: card.Minion, dx: int, dy: int, number: int, tag: str):
	#Subtypes
	subtypes = c.get_tribes()
	if subtypes != []:
		number_subtypes = len(subtypes)
		path = CARD_ASSETS_PATH + "spell_school_banner"
		
		if number_subtypes != 1:
			path += str(number_subtypes)

		if c.is_golden():
			path += "_golden"

		w.add_image((number, Item.TRIBE_BANNER), path + ".png", dx + 102, dy + 251, "center", tag)

force_tiny_tribes = [Tribe.TITAN_FORGED]

def add_subtype_text(w: window.Window, c: card.Minion, dx: int, dy: int, text_tag: str):
	subtypes = c.get_tribes()
	if subtypes != []:
		number_subtypes = len(subtypes)
		interline = 12
		
		force_tiny = False
		for t in force_tiny_tribes:
			if t in subtypes:
				force_tiny = True
				break
		
		if not force_tiny and number_subtypes <= 1:
			font_size = 10
		elif not force_tiny and number_subtypes == 2:
			font_size = 8
		else:
			font_size = 7
			interline = 9

		subtypes_strings = [tribes.to_string(i) for i in subtypes]
		for i in range(number_subtypes):
			subtypes_string = subtypes_strings[i]
			w.add_outline(subtypes_string, 102 + dx, 251 + dy - (number_subtypes - 1) * interline // 2 + interline * i,
			("Belwe Bd BT", font_size), "white", "black", "center", "center", text_tag)

def add_description_background(w: window.Window, c: card.Card, dx: int, dy: int, number:int, tag: str):
	if not c.is_golden():
		path = CARD_ASSETS_PATH + "description_background.png"
	else:
		path = CARD_ASSETS_PATH + "description_background_golden.png"
	w.add_image((number, Item.DESCRIPTION_BACKGROUND), path, dx + 102, dy + 206, "center", tag)

def add_description(w: window.Window, c: card.Card, dx: int, dy: int, text_tag: str):
	t = c.get_type()
	x, y = dx + 102, dy + 205
	if t in [Type.SPELL, Type.HERO_POWER, Type.GLYPH]:
		y = dy + 209
	if t in [Type.MINION, Type.SPELL]:
		number_subtypes = len(c.get_tribes())
		if number_subtypes == 1:
			y = dy + 205
		elif number_subtypes >= 2:
			y = dy + 200
	
	desc_result = description.description(c.get_description(), c.get_type())
	text = desc_result["description"]
	font_size = desc_result["font_size"]
	if c.is_golden():
		color = "white"
	else:
		color = "black"
	
	w.add_text(text, x, y, ("Franklin Gothic Medium Cond", font_size), color, "center", "center", text_tag)

def add_watermark(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	# Guide for golden watermarks: 110 Saturation, -50 Intensity; then 25 Constrast
	# Guide for recoloring from hearthcards: 95 Saturation, 10 Intensity
	if c.is_golden():
		path = CARD_ASSETS_PATH + "watermark/watermark-{}-golden.png".format(expansions.to_string(c.get_expansion()))
	else:
		path = CARD_ASSETS_PATH + "watermark/watermark-{}.png".format(expansions.to_string(c.get_expansion()))
	y = dy + 208
	if c.get_type() in [Type.MINION, Type.SPELL]:
		number_subtypes = len(c.get_tribes())
		if number_subtypes == 1:
			y -= 4
		elif number_subtypes >= 2:
			y -= 8

	w.add_image((number, Item.WATERMARK), path, dx + 102, y, "center", tag)

def add_expansion(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	# path = CARD_ASSETS_PATH + "expansion-{}.png".format(expansions.to_string(c.get_expansion()))
	# w.add_image((number, Item.EXPANSION), path, dx + 102, dy + 153, "center", tag)
	()

def add_golden(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	if c.is_golden():
		# path_star = CARD_ASSETS_PATH + "golden_star.png"
		path_leaves_left = CARD_ASSETS_PATH + "golden_leaves_left.png"
		path_leaves_right = CARD_ASSETS_PATH + "golden_leaves_right.png"
		# w.add_image((number, Item.GOLDEN), path_star, dx + 170, dy + 50, "n", tag)
		w.add_image((number, Item.LEAVES_LEFT), path_leaves_left, dx + 58, dy + 92, "center", tag)
		w.add_image((number, Item.LEAVES_RIGHT), path_leaves_right, dx + 142, dy + 92, "center", tag)

def add_name_banner(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	if c.is_golden():
		path = CARD_ASSETS_PATH + "name_banner_golden.png"
	else:
		path = CARD_ASSETS_PATH + "name_banner.png"
	w.add_image((number, Item.NAME_BANNER), path, dx + 102, dy + 136, "center", tag)

def add_name(w: window.Window, c: card.Card, dx: int, dy: int, text_tag: str):
	name = c.get_name()
	if name[0] == "—":
		return

	name_weight = description.text_weight(c.get_name())

	#The font size is different depending on the length of the name.
	font_size = 11
	if name_weight > 33:
		font_size -= 1
		if name_weight > 39:
			font_size -= 1
			if name_weight > 45:
				font_size -= 1
				if name_weight > 53:
					font_size -= 1
	
	rarity = c.get_rarity()
	x, y = dx + 102, dy + 136

	primary_color = "white"
	# if rarities.group(rarity) is Rarity.COMMON:
	# 	primary_color = "white"
	# elif rarity is Rarity.RARE:
	# 	primary_color = "#3a8bff"
	# elif rarity is Rarity.EPIC:
	# 	primary_color = "#a82ec9"
	# elif rarities.group(rarity) is Rarity.LEGENDARY:
	# 	primary_color = "#ffae0c"

	w.add_outline(c.get_name(), x, y,
		("Belwe Bd BT", font_size), primary_color, "black", "center", "center", text_tag)

def add_gem(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	rarity = c.get_rarity()

	if not (rarity is Rarity.BASIC or rarity is Rarity.BASIC_D):
		# if c.is_golden():
		# 	path = CARD_ASSETS_PATH + "gem-{}-golden.png".format(rarities.to_string(rarity))
		# else:
		path = CARD_ASSETS_PATH + "gem-{}.png".format(rarities.to_string(rarity))
		w.add_image((number, Item.GEM), path, dx + 102, dy + 95, "n", tag)

def add_dragon(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	rarity = c.get_rarity()

	if rarities.group(rarity) is Rarity.LEGENDARY:
		if c.is_golden():
			path = CARD_ASSETS_PATH + "dragon_golden.png"
		else:
			path = CARD_ASSETS_PATH + "dragon.png"
		w.add_image((number, Item.DRAGON), path, dx + 62, dy + 3, "nw", tag)

def add_type(w: window.Window, c: card.Card, dx: int, dy: int, text_tag: str):
	y = 266

	if c.get_type() in [Type.MINION, Type.SPELL] and len(c.get_tribes()) >= 2:
		y = 275

	# Card type
	w.add_outline(types.to_string(c.get_type()), dx + 102, dy + y,
		("Belwe Bd BT", 8), "white", "black", "center", "center", text_tag)

def add_undermana(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	# Banner
	x_banner, y_banner = 27, 48

	class_values = [cl.value for cl in c.get_class()]
	groups_values = [g.value for g in c.get_groups()]
	desc = c.get_description()
	if Class.KABAL.value in class_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_kabal.png", dx + x_banner, dy + y_banner, "center", tag)
	elif Class.GOONS.value in class_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_goons.png", dx + x_banner, dy + y_banner, "center", tag)
	elif Class.JADE.value in class_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_jade.png", dx + x_banner, dy + y_banner, "center", tag)
	
	elif Group.PROTOSS.value in groups_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_protoss.png", dx + x_banner, dy + y_banner, "center", tag)
	elif Group.TERRAN.value in groups_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_terran.png", dx + x_banner, dy + y_banner, "center", tag)
	elif Group.ZERG.value in groups_values:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_zerg.png", dx + x_banner, dy + y_banner, "center", tag)

	elif desc[:13] == "$Échangeable$":
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_tradeable.png", dx + x_banner, dy + y_banner, "center", tag)
	elif "$Forge :$" in desc:
		w.add_image((number, Item.UNDERMANA), CARD_ASSETS_PATH + "banner_forge.png", dx + x_banner, dy + y_banner, "center", tag)


	# Death Knight Runes
	card_runes = c.get_runes()
	if card_runes != []:
		w.add_image((number, Item.RUNE_HOLDER), CARD_ASSETS_PATH + "rune_holder.png", dx + 24, dy + 45, "center", tag)

		is_golden = c.is_golden()

		for i in range(len(card_runes)):
			rune_i_filename = runes.to_filename(card_runes[i], is_golden)
			# print(rune_i_filename)
			if i == 0:
				w.add_image((number, Item.RUNE1), CARD_ASSETS_PATH + rune_i_filename, dx + 10, dy + 52, "center", tag)
			elif i == 1:
				w.add_image((number, Item.RUNE2), CARD_ASSETS_PATH + rune_i_filename, dx + 24, dy + 57, "center", tag)
			elif i == 2:
				w.add_image((number, Item.RUNE3), CARD_ASSETS_PATH + rune_i_filename, dx + 38, dy + 52, "center", tag)
			else:
				break

def add_cost(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	path = CARD_ASSETS_PATH + "cost.png"
	w.add_image((number, Item.COST), path, dx + 24, dy + 23, "center", tag)

def add_coin(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str, second_emplacement: bool = False):
	path = CARD_ASSETS_PATH + "coin.png"
	item = Item.COST
	if second_emplacement:
		dy += 57
		item = Item.COST2
	w.add_image((number, item), path, dx + 24, dy + 23, "center", tag)

def add_cost_text(w: window.Window, c: card.Card, dx: int, dy: int, text_tag: str, second_emplacement: bool = False):
	cost = c.get_cost()
	if second_emplacement:
		dy += 57
	if cost is not None:
		w.add_outline(cost, dx + 24, dy + 19, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_tavern(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str):
	tavern = c.get_tavern()
	path = CARD_ASSETS_PATH + "tavern-{}.png".format(c.get_tavern())
	w.add_image((number, Item.COST), path, dx + 24, dy + 27, "center", tag)

def add_attack_minion(w: window.Window, c: card.Minion, dx: int, dy: int, number: int, tag: str):
	attack = c.get_attack()
	if attack is not None or c.force_show_attack:
		path = CARD_ASSETS_PATH + "attack_minion.png"
		w.add_image((number, Item.ATTACK), path, dx + 21, dy + 244, "center", tag)

def add_attack_minion_text(w: window.Window, c: card.Minion, dx: int, dy: int, text_tag: str):
	attack = c.get_attack()
	if attack is not None:
		w.add_outline(attack, dx + 24, dy + 246, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_health(w: window.Window, c: card.Minion, dx: int, dy: int, number: int, tag: str):
	health = c.get_health()
	if health is not None or c.force_show_health:
		path = CARD_ASSETS_PATH + "health.png"
		w.add_image((number, Item.HEALTH), path, dx + 184, dy + 245, "center", tag)

def add_health_text(w: window.Window, c: card.Minion, dx: int, dy: int, text_tag: str):
	health = c.get_health()
	if health is not None:
		w.add_outline(health, dx + 183, dy + 246, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_attack_weapon(w: window.Window, c: card.Weapon, dx: int, dy: int, number: int, tag: str):
	path = CARD_ASSETS_PATH + "attack_weapon.png"
	w.add_image((number, Item.ATTACK), path, dx + 1, dy + 227, "nw", tag)

def add_attack_weapon_text(w: window.Window, c: card.Weapon, dx: int, dy: int, text_tag: str):
	attack = c.get_attack()
	if attack is not None:
		w.add_outline(attack, dx + 24, dy + 246, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_durability(w: window.Window, c: card.Weapon, dx: int, dy: int, number: int, tag: str):
	path = CARD_ASSETS_PATH + "durability.png"
	w.add_image((number, Item.DURABILITY), path, dx + 162, dy + 225, "nw", tag)

def add_durability_text(w: window.Window, c: card.Weapon, dx: int, dy: int, text_tag: str):
	durability = c.get_durability()
	if durability is not None:
		w.add_outline(durability, dx + 183, dy + 246, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_armor(w: window.Window, c: card.Hero, dx: int, dy: int, number: int, tag: str):
	path = CARD_ASSETS_PATH + "armor.png"
	w.add_image((number, Item.ARMOR), path, dx + 163, dy + 226, "nw", tag)

def add_armor_text(w: window.Window, c: card.Hero, dx: int, dy: int, text_tag: str):
	armor = c.get_armor()
	if armor is not None:
		w.add_outline(armor, dx + 183, dy + 246, ("Belwe Bd BT", 33), "white", "black", "center", "center", text_tag)

def add_minion_to_window(w: window.Window, c: card.Minion, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_minion_type_banner(w, c, dx, dy, number, tag)
	add_subtype_text(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	if tavern_mode or (not tavern_mode and c.get_cost() is None and not c.get_tavern() is None):
		add_tavern(w, c, dx, dy, number, tag)
	else:
		add_cost(w, c, dx, dy, number, tag)
		add_cost_text(w, c, dx, dy, text_tag)
	add_attack_minion(w, c, dx, dy, number, tag)
	add_attack_minion_text(w, c, dx, dy, text_tag)
	add_health(w, c, dx, dy, number, tag)
	add_health_text(w, c, dx, dy, text_tag)

def add_spell_to_window(w: window.Window, c: card.Spell, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_spell_school_banner(w, c, dx, dy, number, tag)
	add_subtype_text(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	if tavern_mode:
		if c.get_tavern() is None:
			add_coin(w, c, dx, dy, number, tag)
			add_cost_text(w, c, dx, dy, text_tag)
		else:
			add_tavern(w, c, dx, dy, number, tag)
			add_coin(w, c, dx, dy, number, tag, second_emplacement = True)
			add_cost_text(w, c, dx, dy, text_tag, second_emplacement = True)
	else:
		add_cost(w, c, dx, dy, number, tag)
		add_cost_text(w, c, dx, dy, text_tag)

def add_weapon_to_window(w: window.Window, c: card.Weapon, dx: int, dy: int, number: int, tag: str, text_tag: str, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)
	add_attack_weapon(w, c, dx, dy, number, tag)
	add_attack_weapon_text(w, c, dx, dy, text_tag)
	add_durability(w, c, dx, dy, number, tag)
	add_durability_text(w, c, dx, dy, text_tag)

def add_hero_to_window(w: window.Window, c: card.Hero, dx: int, dy: int, number: int, tag: str, text_tag: str, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)
	add_armor(w, c, dx, dy, number, tag)
	add_armor_text(w, c, dx, dy, text_tag)

def add_hero_power_to_window(w: window.Window, c: card.HeroPower, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	if tavern_mode:
		add_coin(w, c, dx, dy, number, tag)
	else:
		add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)

def add_trinket_to_window(w: window.Window, c: card.Trinket, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_minion_type_banner(w, c, dx, dy, number, tag)
	add_subtype_text(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	if tavern_mode:
		add_coin(w, c, dx, dy, number, tag)
	else:
		add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)

def add_location_to_window(w: window.Window, c: card.Spell, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)
	add_durability(w, c, dx, dy, number, tag)
	add_durability_text(w, c, dx, dy, text_tag)

def add_portrait_to_window(w: window.Window, c: card.Hero, dx: int, dy: int, number: int, tag: str, text_tag: str, tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	if tavern_mode:
		add_coin(w, c, dx, dy, number, tag)
	else:
		add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)
	add_health(w, c, dx, dy, number, tag)
	add_health_text(w, c, dx, dy, text_tag)

def add_glyph_to_window(w: window.Window, c: card.Glyph, dx: int, dy: int, number: int, tag: str, text_tag: str, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)

def add_default_card_to_window(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str, text_tag: str, class_context: Class = None):
	add_background(w, c, dx, dy, number, tag)
	add_carpet(w, c, dx, dy, number, tag, class_context)
	add_name_banner(w, c, dx, dy, number, tag)
	add_name(w, c, dx, dy, text_tag)
	add_artwork(w, c, dx, dy, number, tag)
	add_frame(w, c, dx, dy, number, tag)
	add_gem(w, c, dx, dy, number, tag)
	add_class(w, c, dx, dy, number, tag, class_context)
	add_description_background(w, c, dx, dy, number, tag)
	add_watermark(w, c, dx, dy, number, tag)
	add_description(w, c, dx, dy, text_tag)
	add_golden(w, c, dx, dy, number, tag)
	add_expansion(w, c, dx, dy, number, tag)
	add_dragon(w, c, dx, dy, number, tag)
	add_type(w, c, dx, dy, text_tag)
	add_undermana(w, c, dx, dy, number, tag)
	add_cost(w, c, dx, dy, number, tag)
	add_cost_text(w, c, dx, dy, text_tag)


def add_card_to_window(w: window.Window, c: card.Card, dx: int, dy: int, number: int, tag: str = "", tavern_mode: bool = settings.TAVERN_MODE, class_context: Class = None):
	t = c.get_type()
	text_tag = window.get_card_text_tag(c, number)

	if t is Type.MINION:
		add_minion_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.SPELL:
		add_spell_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.WEAPON:
		add_weapon_to_window(w, c, dx, dy, number, tag, text_tag, class_context)
	elif t is Type.HERO_POWER:
		add_hero_power_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.HERO:
		add_hero_to_window(w, c, dx, dy, number, tag, text_tag, class_context)
	elif t is Type.PORTRAIT:
		add_portrait_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.LOCATION:
		add_location_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.TRINKET:
		add_trinket_to_window(w, c, dx, dy, number, tag, text_tag, tavern_mode, class_context)
	elif t is Type.GLYPH:
		add_glyph_to_window(w, c, dx, dy, number, tag, text_tag, class_context)
	else:
		add_default_card_to_window(w, c, dx, dy, number, tag, text_tag, class_context)