#Every tkinter implementation should be hidden here, and invisible to the other modules.
#The module Image also knows tkinter, but it's the only other one.

from tkinter import *

from tools import normalization

from structures import expansions
from structures import items
from structures.items import Item
from structures import events
from structures.events import Event
from card import card

def get_card_text_tag(c: card.Card, number: int) -> str:
	return normalization.title_to_tag("{}-{}".format(number, c.get_key()))

class Window(Frame):
	def __init__(self, window: Tk, canvas: Canvas):

		# Frame.__init__(self, canvas)
		# self.master.minsize(width=100, height=100)
		# self.master.config()

		# self.master.bind('<Left>', self.left_key)
		# self.master.bind('<Right>', self.right_key)

		self._window	= window
		self._canvas	= canvas
		
		self._images	= {}
		self._text_tags	= {}

	def add_image(self, key: (int, Item), path: str, x: int, y: int, anchor: str = "nw", tag: str = None):
		try:
			i = PhotoImage(file = path)
			if tag is None:
				self._canvas.create_image(x, y, image = i, anchor = anchor)
			else:
				self._canvas.create_image(x, y, image = i, tags = tag, anchor = anchor)
			self._canvas.pack()
			self._images[key] = i
			return True
		except TclError:
			()
			# print("Couldn't find image located at", path)
			return False

	def add_text(self, text: str, x: int, y: int, font: (str, int), fill = "black",
		anchor: str = "center", justify: str = "center", tag: str = None):
		if tag is None:
			self._canvas.create_text(x, y,
				text = text, font = font, fill = fill, anchor = anchor, justify = "center")
		else:
			norm_tag = normalization.title_to_tag(tag)
			self._canvas.create_text(x, y,
				text = text, font = font, fill = fill, anchor = anchor, justify = "center", tag = norm_tag)
			self._text_tags[norm_tag] = 0

	def add_outline(self, text: str, x: int, y: int, font: (str, int), primary_color: str,
		secondary_color: str, anchor: str = "center", justify: str = "center", tag: str = None):
		for i in range(-1, 2):
			for j in range(-1, 2):
				if (i,j) != (0,0):
					self.add_text(text, x+i, y+j, font=font, fill = secondary_color, anchor = anchor, justify = justify, tag = tag)
		self.add_text(text, x, y, font = font, fill = primary_color, anchor = anchor, justify = justify, tag = tag)


	def add_tag(self, name: str, event: Event, function):
		tag = events.to_tag(event)
		self._canvas.tag_bind(name, tag, function)

	def add_global_bind(self, event: Event, function):
		tag = events.to_tag(event)
		self._canvas.bind_class(tag, func = function)


	def delete_image(self, key: (int, Item)):
		image = self._images.pop(key, None)
		if image is not None:
			self._canvas.delete(image)

	def delete_text(self, tag: str):
		norm_tag = normalization.title_to_tag(tag)
		self._canvas.delete(tag)
		self._text_tags.pop(norm_tag)

	def delete_card_images(self, number: int):
		for item in Item:
			if items.is_in_card_display(item):
				self.delete_image((number, item))
		for position in ["left", "middle", "right"]:
			self.delete_image((number, Item.CARPET, position))
			self.delete_image((number, Item.CARPET2, position))

	def delete_classes(self, number: int, how_many_classes: int):
		for i in range(how_many_classes):
			self.delete_image((number, Item.CLASS, i))

	def delete_card_text(self, c: card.Card, number: int):
		tag = get_card_text_tag(c, number)
		self.delete_text(tag)

	def delete_card(self, c: card.Card, number: int):
		self.delete_card_text(c, number)
		self.delete_card_images(number)
		self.delete_classes(number, len(c.get_class()))

	def delete_tag(self, name: str, event: Event):
		tag = events.to_tag(event)
		self._canvas.unbind(name, tag)
		

	def clear(self):
		self._canvas.delete("all")
		self._text_tags = {}
		self._images = {}


	def display(self):
		self._window.mainloop()

	def get_window(self) -> Tk:
		return self._window
	def get_canvas(self) -> Canvas:
		return self._canvas

def init_window(x: int, y: int) -> Window:
	window = Tk()
	canvas = Canvas(window, width = x, height = y)
	return Window(window, canvas)

def reset_windows():
	mainloop()