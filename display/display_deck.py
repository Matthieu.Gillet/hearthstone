import settings

from structures import classes
from structures.items import Item
from structures.rarities import group, Rarity
from structures.events import Event
from structures.types import Type

from tools import description

from card import card
from card import deck
from card.deck import Deck

from display import window
from display import display_card

DECK_ASSETS_PATH = "assets/deck/"
HERO_ART_PATH = "assets/deck/art/Heroes/"

def display_miniature(w: window.Window, d: Deck, dx: int, dy: int, number: int, tag: str):
	portait_path = HERO_ART_PATH + "{}.png".format(classes.to_string(d.get_class()))
	w.add_image((number, Item.PORTRAIT), portait_path, dx-12, dy + 9, "ne", tag)
	w.add_image((number, Item.BLUR), DECK_ASSETS_PATH + "blur_deck_name.png", dx-9, dy + 35, "ne", tag)
	w.add_outline(d.get_name(), dx-117, dy + 60, ("Belwe Bd BT", 11),
		"white", "black", "w", "w", tag)
	w.add_image((number, Item.DECK_HOLDER), DECK_ASSETS_PATH + "deck_holder.png", dx, dy + 2 , "ne", tag)

def delete_miniature(w: window, number: int, tag: str):
	w.delete_image((number, Item.PORTRAIT))
	w.delete_image((number, Item.BLUR))
	w.delete_image((number, Item.DECK_HOLDER))
	w.delete_text(tag)


def display_deck_card(w: window.Window, d: deck.Deck, dx: int, dy: int, number: int):
	x, y = dx-122, dy+20*number
	tag = "deck_card-{}".format(number)
	try:
		w.delete_image((number, Item.MINIATURE_BACKGROUND))
		w.delete_image((number, Item.MINIATURE_COST))
		w.delete_text(tag)
	except KeyError:
		()

	dc = d.get_contents()[number]
	c = dc.card

	def hover_out_deck_card(dc: deck.Deck_Card, number: int):
		def f(event):
			try:
				w.delete_card(dc.card, 9)
			except KeyError:
				()
			w.delete_image((number, Item.HITBOX_OUT))
			display_hitboxes_in(w, d, dx, dy)
		return f

	def click_on_deck_card(dc: deck.Deck_Card, number: int):
		def f(event):
			d.remove_card(dc.card)
			w.delete_card(dc.card, 9)
			display_hitboxes_in(w, d, dx, dy)
		return f

	def hover_over_deck_card(dc: deck.Deck_Card, number: int):
		def f(event):
			if dc.golden_number == 0:
				c = dc.card
			else:
				c = dc.card.golden_copy()
			display_card.add_card_to_window(w, c, x-218, max(10,min(420, y-90)), 9, tavern_mode = settings.TAVERN_MODE, class_context = d.get_class())
			w.add_image((number, Item.HITBOX_OUT), DECK_ASSETS_PATH + "hitbox_out_deck_card.png",
				x+55, y, "center", "hitbox_out_deck_card")
			w.add_image((number, Item.HITBOX_IN), DECK_ASSETS_PATH + "hitbox_in_deck_card.png",
				x+55, y, "center", "hitbox_in_deck_card-hovered")
			w.add_tag("hitbox_out_deck_card", Event.HOVER, hover_out_deck_card(dc, number))
			w.add_tag("hitbox_in_deck_card-hovered", Event.CLICK, click_on_deck_card(dc, number))
		return f
	
	rarity = c.get_rarity()

	path_minibg = DECK_ASSETS_PATH + "deck_card_box{}.png".format(dc.how_golden())
	w.add_image((number, Item.MINIATURE_BACKGROUND), path_minibg, x+55, y, "center")

	#The font size is different depending on the length of the name.
	font_size = 8
	card_name = c.get_name()
	if card_name[0] != "—":
		name_weight = description.text_weight(card_name)

		if name_weight > 33:
			font_size -= 1
			if name_weight > 39:
				font_size -= 1
				if name_weight > 45:
					font_size -= 1
					if name_weight > 53:
						font_size -= 1
		w.add_outline(c.get_name(), x + 12, y,
			("Belwe Bd BT", font_size), "white", "black", "w", "w", tag)

	if settings.TAVERN_MODE:
		if not c.get_tavern() is None:
			w.add_image((number, Item.MINIATURE_COST), DECK_ASSETS_PATH + "tavern-{}.png".format(c.get_tavern()), x, y, "center")
		else:
			w.add_image((number, Item.MINIATURE_COST), DECK_ASSETS_PATH + "coin.png", x, y, "center")
			w.add_outline(c.get_cost(), x, y-1, ("Belwe Bd BT", 12), "white", "black", "center", "center", tag)
	else:
		if c.get_cost() is None and not c.get_tavern() is None:
			w.add_image((number, Item.MINIATURE_COST), DECK_ASSETS_PATH + "tavern-{}.png".format(c.get_tavern()), x, y, "center")
		else:
			w.add_image((number, Item.MINIATURE_COST), DECK_ASSETS_PATH + "cost.png", x, y, "center")
		w.add_outline(c.get_cost(), x, y-1, ("Belwe Bd BT", 12), "white", "black", "center", "center", tag)

	if dc.total_number > 1:
		w.add_outline(dc.total_number, x+113, y-1, ("Belwe Bd BT", 10), "#FF8121", "black", "center", "center", tag)
	elif group(rarity) is Rarity.LEGENDARY:
		w.add_outline("★", x+113, y-1, ("Belwe Bd BT", 10), "#FF8121", "black", "center", "center", tag)

	w.add_image((number, Item.HITBOX_IN), DECK_ASSETS_PATH + "hitbox_in_deck_card.png",
		x+55, y, "center", "hitbox_in_deck_card-{}".format(number))
	w.add_tag("hitbox_in_deck_card-{}".format(number), Event.HOVER, hover_over_deck_card(dc, number))

def display_hitboxes_in(w: window.Window, d: Deck, dx: int, dy: int):
	card_list = d.get_contents()
	i=0
	try:
		while True:
			display_deck_card(w, d, dx, dy, i)
			i+=1
	except IndexError:
		()

def display_full(w: window.Window, d: Deck, dx: int, dy: int, number: int):
	display_miniature(w, d, dx, dy, number, "deck")
	display_hitboxes_in(w, d, dx, dy+90)

def delete_deck(w: window.Window, d: Deck):
	card_list = d.get_contents()
	i = 0
	try:
		while True:
			tag = "deck_card-{}".format(i)

			w.delete_image((i, Item.MINIATURE_BACKGROUND))
			w.delete_image((i, Item.MINIATURE_COST))
			w.delete_text(tag)

			i = i + 1
	except KeyError:
		()

