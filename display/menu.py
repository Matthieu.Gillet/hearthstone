from structures.items import Item
from structures.events import Event

from display import window
from display import pack_opening
from display import display_collection

#Files are dumb for now.
MENU_ASSETS_PATH = "assets/menu/"
OPENING_ASSETS_PATH = "assets/opening/"

def menu(w: window.Window):
	def back(event):
		w.clear()
		menu(w)

	def opening(event):
		w.clear()
		pack_opening.expansion_hub(w, True, back)


	def add_background_opening(w: window.Window):
		w.add_image((0, Item.BACKGROUND), MENU_ASSETS_PATH + "background.png", 0, 0, "nw")

	add_background_opening(w)
	
	w.add_image((0, Item.LOGO), OPENING_ASSETS_PATH + "pack.png", 400, 384, "center", "opening")
	w.add_tag("opening", Event.CLICK, opening)

	
	def my_collection(event):
		w.clear()
		display_collection.display_collection(w, 0, None, None, [], back)
	
	w.add_image((1, Item.LOGO), MENU_ASSETS_PATH + "collection.png", 900, 384, "center", "my_collection")
	w.add_tag("my_collection", Event.CLICK, my_collection)