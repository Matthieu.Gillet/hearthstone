from user_settings import card_back_path, big_card_back_path
import settings

from structures import expansions
from structures import rarities
from structures.items import Item
from structures.events import Event

from card import card
from card import collection
from card import pack

from display import window
from display import display_card

ASSETS_PATH = "assets/"
OPENING_ASSETS_PATH = "assets/opening/"

NUMBER_EXPANSIONS_PER_ROW = 7

def add_background_opening(w: window.Window):
	w.add_image((0, Item.BACKGROUND), OPENING_ASSETS_PATH + "background.png", 0, 0, "nw")


def display_pack(w: window.Window, pack: list, expansion: expansions.Expansion, dynamic: bool, back):
	c1, c2, c3, c4, c5 = pack
	
	is_card_revealed = [False for i in range(5)]

	x,y=[680,450,910,550,810], [30,105,105,415,415]     #Coordonnées des cartes à afficher
	xh,yh=[i+102 for i in x], [j+140 for j in y] 					     #Coordonnées des lumières hover à afficher
	xc,yc=[50]*4+[175]*4,[22,57,92,127]*2          		 #Coordonnées des compteurs de rareté
	
	add_background_opening(w)

	# #Compteur de paquets ouverts
	# global photopack
	# global cptg
	# photopack=PhotoImage(file=pathassets+"Paquets/_pack.png")
	# canvas.create_image(90,380,image=photopack,anchor="n")
	# txt_compteurpacks = canvas.create_text(90,500, text=cptg, font=("Belwe Bd BT", 50), fill="white",anchor="n")
	
	# #Compteur de cartes selon la rareté
	# global gemme
	# gemme[0]=PhotoImage(file=pathassets+"midcommon.png")
	# gemme[1]=PhotoImage(file=pathassets+"midrare.png")
	# gemme[2]=PhotoImage(file=pathassets+"midepic.png")
	# gemme[3]=PhotoImage(file=pathassets+"midlegendary.png")
	
	# for i in range(4):
	# 	canvas.create_image(xc[i]-30,yc[i]+3,image=gemme[i],anchor="w")
	# 	gemme[i+4]=PhotoImage(file=pathassets+"ministar.png")
	# 	canvas.create_image(xc[i+4]-33,yc[i+4]+2,image=gemme[i+4],anchor="w")
	
	# def remplacercompteurs():
	# 	"""Une fois qu'une carte est révélée, on met à jour les compteurs de rareté."""
	# 	for i in range(8):
	# 		canvas.delete(txtcompteursrarete[i])
	# 		txtcompteursrarete[i]=canvas.create_text(xc[i],yc[i], text=compteursrarete[i], font=("Belwe Bd BT", 25), fill="white",anchor="w")
	
	# remplacercompteurs()
		
	# def ajoutercompteurs(c):
	# 	"""Met à jour les compteurs à l'aide de la carte donnée en entrée.
		
	# 	==========
	# 	c
	# 		Carte dont la rareté doit être ajoutée aux compteurs."""
	# 	r=rarete2num(c.rarete)
	# 	if r is not None:
	# 		compteursrarete[r]+=1
	# 		if c.doree:
	# 			compteursrarete[r+4]+=1
	
	def reveal(i: int):
		def f(event):
			w.delete_image((i, Item.CARD_BACK))
			w.delete_image((i, Item.HOVER_LIGHT))

			display_card.add_card_to_window(w, pack[i], x[i], y[i], i, tavern_mode = settings.TAVERN_MODE)
			is_card_revealed[i]=True
			
			#ajoutercompteurs(c[i])
			#remplacercompteurs()
			check_if_end()
		return f

	def hover_over_card(i: int):
		def f(event):
			w.delete_image((i, Item.CARD_BACK))

			hover_path = OPENING_ASSETS_PATH + "hover-{}.png".format(rarities.to_string(pack[i].get_rarity()))

			w.add_image((i, Item.HOVER_LIGHT), hover_path, xh[i], yh[i], "center", "hover-{}".format(i))
			w.add_image((i, Item.CARD_BACK), big_card_back_path, xh[i], yh[i], "center", "clickable_card-{}".format(i))
		return f
	
	
	def rehide(i: int):
		def f(event):
			if not is_card_revealed[i]:
				w.delete_image((i, Item.CARD_BACK))
				w.delete_image((i, Item.HOVER_LIGHT))
				
				w.add_image((i, Item.CARD_BACK), card_back_path, xh[i], yh[i], "center", "card-{}".format(i))
		return f
	
	for i in range(5):
		w.add_image((i, Item.CARD_BACK), card_back_path, xh[i], yh[i], "center", "card-{}".format(i))

		w.add_tag("card-{}".format(i), Event.HOVER, hover_over_card(i))
		w.add_tag("clickable_card-{}".format(i), Event.CLICK, reveal(i))
		w.add_tag("hover-{}".format(i), Event.HOVER, rehide(i))

	def check_if_end():
		if not False in is_card_revealed:
			end_of_pack_display(w, expansion, dynamic, back)


def end_of_pack_display(w: window.Window, expansion: expansions.Expansion, dynamic: bool, back):
	def open_another_pack(event):
		w.clear()
		display_opening(w, expansion, dynamic, back)
	
	def changepack(event):
		w.clear()
		expansion_hub(w, dynamic, back)

	path_changepack = OPENING_ASSETS_PATH + "changepack.png"
	w.add_image((0, Item.CHANGEPACK_BUTTON), path_changepack, 1200, 600, "center", "changepack-button")
	w.add_tag("changepack-button", Event.CLICK, changepack)

	path_continue = OPENING_ASSETS_PATH + "continue.png"
	w.add_image((0, Item.CONTINUE_BUTTON), path_continue, 780, 370, "center", "continue-button")
	w.add_tag("continue-button", Event.CLICK, open_another_pack)

	w.add_image((0, Item.BACK_BUTTON), ASSETS_PATH + "back.png", 0, 701, "sw", "back_button")
	w.add_tag("back_button", Event.CLICK, back)


def display_opening(w: window.Window, expansion: expansions.Expansion, dynamic: bool, back):
	p = pack.generate_pack(expansion, dynamic)
	display_pack(w, p, expansion, dynamic, back)


def expansion_hub(w: window.Window, dynamic: bool, back):
	add_background_opening(w)
	w.add_outline("Choisissez une extension", 731, 50, ("Belwe Bd Bt", 25),
		"white", "black", "center", "center", "expansion-hub_headline")
	
	def open_pack(i):
		def f(event):
			w.clear()

			expansion = expansions.EXPANSION_DICT[i]
			display_opening(w, expansion, dynamic, back)
		return f

	def complete_expansion(i):
		def f(event):
			collection.complete_expansion(i, False)
		return f
	
	def x(i: int) -> int:
		return 810 + (80 * (2 * i - NUMBER_EXPANSIONS_PER_ROW))
	def y(i: int) -> int:
		return 120 + 105 * i

	skipped = 0
	for i in range(expansions.NUMBER_EXPANSIONS):
		if expansions.EXPANSION_DICT[i] == expansions.Expansion.BATTLEGROUNDS:
			skipped += 1
			continue

		path = OPENING_ASSETS_PATH + "expansion-{}.png".format(i)
		w.add_image((i, Item.LOGO), path, x((i-skipped)%NUMBER_EXPANSIONS_PER_ROW), y((i-skipped)//NUMBER_EXPANSIONS_PER_ROW), "center", "expansion-{}".format(i))
		w.add_tag("expansion-{}".format(i), Event.CLICK, open_pack(i))
		w.add_tag("expansion-{}".format(i), Event.RIGHT_CLICK, complete_expansion(i))

	w.add_image((0, Item.BACK_BUTTON), ASSETS_PATH + "back.png", 0, 701, "sw", "back_button")
	w.add_tag("back_button", Event.CLICK, back)